
-- CREATE TABLE
CREATE TABLE kd_search_history
(
  user_id INT,
  keyword VARCHAR,
  count_keyword   INT default 1 CHECK (count_keyword > 0),
  status BOOLEAN DEFAULT TRUE,
  PRIMARY KEY (user_id, keyword),
  FOREIGN KEY (user_id) REFERENCES kd_user (id)
);

-- CREATE FUNCTION
CREATE OR REPLACE FUNCTION func_add_search_history (u_id INT, k_word VARCHAR)
RETURNS BOOLEAN AS $$
DECLARE
  d_count_keyword INT;
  d_status BOOLEAN;
  BEGIN
    -- GET count from table first.
    d_count_keyword = (SELECT count_keyword FROM kd_search_history WHERE user_id=u_id AND keyword=k_word);
    d_status = (SELECT status FROM kd_search_history WHERE user_id=u_id AND keyword=k_word);

    -- check if d_status is TRUE or status is null which is mean that there is no previous record
    -- of user_id and keyword of provided
    IF d_status IS TRUE OR d_status IS NULL THEN
      INSERT INTO kd_search_history (user_id, keyword) VALUES (u_id, k_word)
      ON CONFLICT (user_id, keyword) DO UPDATE  SET count_keyword = d_count_keyword + 1;
      RETURN TRUE;
    -- if status is false, update status back to TRUE and increase count
    ELSE
      INSERT INTO kd_search_history (user_id, keyword) VALUES (u_id, k_word)
      ON CONFLICT (user_id, keyword) DO UPDATE
        SET status = TRUE, count_keyword = d_count_keyword + 1;
      RETURN TRUE;
    END IF;
  RETURN FALSE;
  END
  $$
LANGUAGE 'plpgsql';

-- call this function in Spring in Repository

SELECT * FROM func_add_search_history(1, 'sala');


SELECT * FROM kd_search_history;
