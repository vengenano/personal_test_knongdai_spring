CREATE TABLE kd_notification(
  id serial PRIMARY KEY ,
  request_date timestamp,
  sent_date timestamp,
  url_id int,
  user_id int,
  send_status char(1) default '1',
  message varchar(200),
  foreign key (user_id) references kd_user on UPDATE cascade on delete cascade ,
  foreign key (url_id) references kd_url on update cascade on delete cascade
);

create table kd_search_history
(
  user_id integer not null constraint kd_search_history_user_id_fkey references kd_user,
  keyword varchar not null,
  count_keyword integer default 1  constraint kd_search_history_count_keyword_check    check (count_keyword > 0),
  status  boolean default true,
  constraint kd_search_history_pkey
  primary key (user_id, keyword)
);


CREATE TABLE kd_notification(
  id serial PRIMARY KEY ,
  request_date timestamp,
  sent_date timestamp default now(),
  url_id int unique ,
  user_id int,
  send_status char(1) default '1',
  message varchar(200),
  foreign key (user_id) references kd_user on UPDATE cascade on delete cascade ,
  foreign key (url_id) references kd_url on update cascade on delete cascade
);
