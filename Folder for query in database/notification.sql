CREATE OR REPLACE FUNCTION "public"."get_user_for_notification"(p_url_id int)
RETURNS TABLE("r_user_id" int4, "r_user_player_id" varchar, "r_url_id" int4, "r_title" varchar,
"r_link" varchar, "r_is_approved" bool,r_created_date timestamp) AS $$
BEGIN
RETURN QUERY
SELECT user_id,
user_player_id,
url.id,
url.title,
url.link,
url.is_approved,
url.created_date
FROM kd_url url LEFT JOIN kd_user us
ON url.user_id=us.id
WHERE user_id IS NOT NULL
AND user_player_id IS NOT NULL
AND url.id=$1
AND role_id=0;
END;
$$ language plpgsql;


CREATE OR REPLACE FUNCTION get_notification(user_id int)
RETURNS TABLE(r_notification_id int,r_request_date timestamp,r_sent_date timestamp,r_url_id int,r_user_id int,r_title varchar,r_link varchar,r_message varchar) AS $$
BEGIN
RETURN QUERY
select notification.id,
       request_date,
       sent_date,
       url_id,
       notification.user_id,
       url.title,
       url.link,
       notification.message
from kd_notification notification left join kd_url url on notification.url_id = url.id
where notification.user_id=$1;
END;
$$ language plpgsql;