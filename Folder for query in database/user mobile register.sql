create function get_user_mobile_register(p_id integer) returns TABLE(r_user_name character varying, r_facebook_id character varying, r_phone_number character varying, r_image character varying, r_gender character varying)
  language plpgsql
as
$$
BEGIN
  RETURN QUERY
  SELECT user_name,facebook_id,phone_number,image,gender
  FROM kd_user where id=$1;
END
$$;

create function insert_user_mobile_register(p_user_name character varying, p_facebook_id character varying, p_phone_number character varying, p_image character varying, p_gender character varying) returns void
  language plpgsql
as
$$
begin
    insert into kd_user( user_name, email, password, facebook_id, phone_number, image, gender,role_id)
    values ($1,'','',$2,$3,$4,$5,0);
  end
$$;

create function update_user_mobile_register(p_id integer, p_user_name character varying, p_gender character varying) returns void
  language plpgsql
as
$$
BEGIN
    UPDATE kd_user
    SET user_name=$2,
        gender=$3
    WHERE id=$1;
  END;
$$;