package com.kshrd.knongdai.scrape;

import com.kshrd.knongdai.domain.Url;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by phen on 27/07/17.
 */
public class ScrapeYellowPage {
    public static List<Url> extractData(String mainUrl, String url){

        String setTitle = "";
        String setPhone = "";
        String setEmail = "";
        String setAddress ="";
        String setUrl ="";
        String pic_url = "";

        List<Url> myurl = new ArrayList<>();
//        Url recordData = new Url();
        try{
            Document document = JsoupConnection.getConnection(url).timeout(10000).get();

            Elements container = document.getElementsByClass("grid-container").select("div.row-fluid");

            for(Element box : container){
                Elements scan = box.select("div.grid_element");
                //Title
                Elements title = scan.select("div.mid_section").select("a").get(0).select("h2").select("b");
                setTitle = title.text();
//                System.out.println("TITLE : "+title.text());
                //Link
                for(Element linkUrl: scan){
                    String suburl = mainUrl + linkUrl.select("div.mid_section").select("a").get(0).attr("href");
                     setUrl = exstractDetail(suburl);
                }
                //Phone
                for(Element phone: scan){
                    String suburl = mainUrl + phone.select("div.mid_section").select("a").get(0).attr("href");
                    setPhone = getPhoneNumber(suburl);
//                    System.out.println("URL : "+ setUrl);

                }
                //Address
                Elements address = scan.select("div.mid_section");
                setAddress =address.select("span").get(2).text();
//                System.out.println("ADDRESS :" + phone.select("span").get(2).text());

                Elements image = scan.select("div.img_section").select("a").get(0).select("img");
                for (Element img : image) {
                    pic_url = img.attr("src");
                    if(!pic_url.contains("https://")){
                        pic_url = "http://www.yp.com.kh" + pic_url;
                    }
                }

                System.out.println(setTitle);
                System.out.println(setUrl);
                System.out.println(setPhone);
                System.out.println(setEmail);
                System.out.println(setAddress);
                System.out.println(pic_url);

//                url.setTitle(title);
//                url.setLink(link);
//                url.setUrlType("w");
//                url.setPhone(phone);
//                url.setEmail(email);
//                url.setAddress(address);
//                url.setDescription(description);
//                url.setMainCateId(1);
//                url.setSubCateId(1);
//                url.setPicUrl(pic_url);

                Url record = new Url();
                record.setTitle(setTitle);
                record.setLink(setUrl);
                record.setUrlType("w");
                record.setPhone(setPhone);
                record.setEmail(setEmail);
                record.setAddress(setAddress);
                record.setDescription("");
                record.setMainCateId(1);
                record.setSubCateId(1);
                record.setPicUrl(pic_url);
                myurl.add(record);
//                myurl.add(new Url(setTitle,setUrl,"w",setPhone,setEmail,setAddress,"",1,1,pic_url));
                //geturl = new Url(title, link,"w", phone, email, address, description,1,1, pic_url);
            }

        }catch (Exception e){
            e.printStackTrace();
            System.out.println("Can not add record to list. ");
        }
        return myurl;
    }

    //If we need jum to de detail page
    public static String exstractDetail(String suburl){
        String link="";
        try {

            Document document = JsoupConnection.getConnection(suburl).timeout(5000).get();


            Element idcode =document.getElementById("1");

            Elements descrip = idcode.getElementsByTag("ul").select("div.table-view-group");

            Elements image = document.getElementsByClass("container").select("div.content_w_sidebar").select("div.col-md-8").select("div.nopad");

            link = descrip.get(1).select("li").get(1).select("a").attr("href");
            if(!link.startsWith("http")){
                link = "http://" + link;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return link;

    }


    //If we need jum to de detail page
    public static String getPhoneNumber(String suburl){
//        String link="";
        String phone = "";
        try {

            Document document = JsoupConnection.getConnection(suburl).timeout(5000).get();

            Element idcode =document.getElementById("1");
            //scrape url,phonenumber,email
            Elements descrip = idcode.getElementsByTag("ul").select("div.table-view-group");

            Elements getPhone = document.getElementsByClass("xs-text-center").select("div.row").get(0).select("div.row").get(0).select("div.col-sm-6").get(1).select("h4");
            phone = getPhone.text();
//            System.out.println(phone);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return phone;

    }


}
