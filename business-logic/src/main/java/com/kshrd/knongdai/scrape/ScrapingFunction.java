package com.kshrd.knongdai.scrape;

import com.kshrd.knongdai.domain.Url;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Phen on 8/17/2017.
 */
public class ScrapingFunction {

    // *** Convert keyword from text search box  to google keyword ***
    public static String convertSpaceToSlush(String text){

        String[] st = text.split(" ");
        String re = st[0].toString();
        for(int i=1; i < st.length ; i++){
            re = re +"+"+st[i];
        }
        return re;
    }

    // *** find doublicate in list from webpage by it self  ***
    public static List<Url> deleteDuplicationUrl(List<Url> datas){
        List<Url> afterFindDuplicateLink = new ArrayList<>();
        Set<String> test = new HashSet<>();
        System.out.println("FIND DUPLICATE IN LIST FROM WEB PAGE BY IT SELF");
        for(Url afterdeleteDuplicateLink : datas){
            if(test.add(afterdeleteDuplicateLink.getLink())){
                afterFindDuplicateLink.add(afterdeleteDuplicateLink);
            }else{
                System.err.println("Duplicate in one page : " + afterdeleteDuplicateLink.getLink());
            }
        }
        return afterFindDuplicateLink;
    }

    // *** VALIDATE URL FROM WEB SITE TO URL FROM DATABASE ***
    public static List<Url> validateWithDatabase(List<Url> RecordFromWebPage, List<Url> RecordFromDb){
        List<Url> filterUrl = new ArrayList<>();
        System.out.println("VALIDATE URL FROM WEB SITE TO URL FROM DATABASE");
        for(Url sh : RecordFromWebPage){
            boolean status = true;
            try{
                if(sh.getLink() != ""){
                    if(sh.getLink().contains("www")){
                        for(Url getFrommdb : RecordFromDb){
                            String db = getFrommdb.getLink().toString();
                            String getFromWeb = sh.getLink().toString();
                            if (getFromWeb.equals(db) ){
                                status = false;
                                System.err.println("old  link " + sh.getLink());
                            }
                        }

                        if(status == true){
                            filterUrl.add(sh);
                            System.out.println("New Link " + sh.getLink());
                        }
                    }
                }

            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return filterUrl;
    }
}
