package com.kshrd.knongdai.service;

import com.kshrd.knongdai.domain.BaseCategory;
import com.kshrd.knongdai.domain.Category;
import com.kshrd.knongdai.domain.MainCategory;
import com.kshrd.knongdai.domain.SubCategoryWithTotalUrl;
import com.kshrd.knongdai.domain.form.CategoryAddForm;
import com.kshrd.knongdai.domain.form.SubCategoryAddForm;

import java.util.List;

public interface CategoryService {

    List<MainCategory> getAllMainCategories();

    Category getCategoryById(int id);

    List<BaseCategory> getAllSubCategories();

    List<SubCategoryWithTotalUrl> getSubCategoriesByMainCateId(int id);

    boolean insertSubCategory(SubCategoryAddForm category);

    boolean insertMainCategory(CategoryAddForm category);

    boolean updateSubCategory(SubCategoryAddForm category);

    boolean updateMainCategory(CategoryAddForm category);

    boolean deleteCategoryById(int id);

    int getMainCateTotalUrl(int mainCateId);

    int getSubCateTotalUrl(int subCateId);

    int countTotalMainCate();

    int countTotalSubCate();

    int getTotalCountUrlByMainId(int id);

}
