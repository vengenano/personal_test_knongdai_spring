package com.kshrd.knongdai.service;

import java.util.List;

/**
 * Created by Mr.Pheng on 7/26/2017.
 */
public interface KeywordService {

    public List<String> getkeywordByName(String name);

}
