package com.kshrd.knongdai.service;

import com.kshrd.knongdai.domain.ListNewKeywordWithTotalRecord;
import com.kshrd.knongdai.domain.form.NewKeywordCategoryMap;
import com.kshrd.knongdai.domain.form.NewKeywordUrlMap;

import java.util.List;

//import com.sun.org.apache.xpath.internal.operations.Bool;

/**
 * Created by Theara on 02-Aug-17.
 */
public interface NewKeywordService {

    List<ListNewKeywordWithTotalRecord> getKeywordsByPage(String sortBy, String order, Boolean mapped, int numRow, int page);

    boolean deleteKeyword(int id);

    boolean mapKeywordToCategory(NewKeywordCategoryMap newKeywordCategoryMap);

    boolean mapKeywordToUrl(NewKeywordUrlMap newKeywordUrlMap);

    int countTotalSearch();

    int countTotalSearchToday();

}
