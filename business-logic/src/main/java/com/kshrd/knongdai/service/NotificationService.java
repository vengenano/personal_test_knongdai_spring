package com.kshrd.knongdai.service;

import com.kshrd.knongdai.domain.Notification;

import java.util.Date;
import java.util.List;

public interface NotificationService {
    Notification getUserForNotification(int urlId);
    boolean insertNotification(Date requestDate,int urlId,int userId,String message);
    Notification getNotification(int userId);
}
