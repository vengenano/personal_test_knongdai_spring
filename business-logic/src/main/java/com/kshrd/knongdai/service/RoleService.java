package com.kshrd.knongdai.service;

import com.kshrd.knongdai.domain.Role;

import java.util.List;

public interface RoleService {

	List<Role> getAllRoles();

	boolean updateRole(Role role);

	boolean deleteRole(int id);

	boolean insertRole(Role role);
}
