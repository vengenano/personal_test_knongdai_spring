package com.kshrd.knongdai.service;

import com.kshrd.knongdai.domain.Url;

import java.util.List;

public interface ScrapeService {

    List<Url> scrapeUrls(String webSite, String keyword, String type);

}
