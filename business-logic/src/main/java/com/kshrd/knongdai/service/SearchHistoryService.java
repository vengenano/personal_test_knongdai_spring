package com.kshrd.knongdai.service;

import com.kshrd.knongdai.domain.SearchHistory;

import java.util.List;

public interface SearchHistoryService {

     boolean insertSearchHistory(SearchHistory searchHistory);
     boolean deletedSearchHistoryStatus(SearchHistory searchHistory);
    List<SearchHistory> getKeywordByUserId(int userId);
}
