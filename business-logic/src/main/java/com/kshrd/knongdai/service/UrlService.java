package com.kshrd.knongdai.service;

import com.kshrd.knongdai.domain.*;

import java.util.List;

public interface UrlService {

    boolean insertUrl(Url url);

    Url getUrlById(int id);

    List<ListUrl> getUrlsByPage(
            int numRow,
            int page,
            String title,
            Integer mainCateId,
            Integer subCateId,
            Boolean approved
    );

    boolean updateUrl(Url url);

    boolean deleteUrlById(int id);

    boolean updateApproved(int urlId, boolean approved);

    List<String> getSearchSuggestedKeywords(String keyword);

    List<String> getUrlSuggestedKeywords(String keyword);

    List<ResultUrlWithTotalRecord> queryUrlsByKeyword(String keyword, int limit, int page);

    List<ResultUrlWithTotalRecord> queryUrlsBySubCateId(int cateId, int limit, int page);

    boolean incrementUrlView(int id);

    boolean isLinkExist(String link);

    int countActiveUrl();

    List<Url> getAllUrls();

    List<UrlGetPopular> selectTopTen();

//    boolean getURLlStatus(int id);

}
