package com.kshrd.knongdai.service;

import com.kshrd.knongdai.domain.UserMobileRegister;

public interface UserMobileRegisterService {
    public boolean insertUserMobileRegister(UserMobileRegister userMobileRegister);

    public UserMobileRegister getUserMobileRegister(int id);

    public boolean updateUserMobileRegister(UserMobileRegister userMobileRegister);
}
