package com.kshrd.knongdai.service;

import com.kshrd.knongdai.domain.User;
import com.kshrd.knongdai.domain.form.UserAddForm;
import com.kshrd.knongdai.domain.form.UserUpdateForm;

import java.util.List;

public interface UserService {

	User findUserByEmail(String email);

	List<User> getAllUsers();

	boolean updateUser(UserUpdateForm user);

	boolean deleteUser(int userId);

	boolean createUser(UserAddForm user);

	User findUserById(int id);
	
}
