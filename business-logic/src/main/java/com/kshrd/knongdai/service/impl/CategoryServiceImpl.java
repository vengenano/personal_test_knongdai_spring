package com.kshrd.knongdai.service.impl;

import com.kshrd.knongdai.domain.BaseCategory;
import com.kshrd.knongdai.domain.Category;
import com.kshrd.knongdai.domain.MainCategory;
import com.kshrd.knongdai.domain.SubCategoryWithTotalUrl;
import com.kshrd.knongdai.domain.form.CategoryAddForm;
import com.kshrd.knongdai.domain.form.SubCategoryAddForm;
import com.kshrd.knongdai.persistence.CategoryRepository;
import com.kshrd.knongdai.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService{

    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryRepository categoryRepository){
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<MainCategory> getAllMainCategories(){
        return categoryRepository.getAllMainCategories();
    }

    @Override
    public Category getCategoryById(int id){
        return categoryRepository.getCategoryById(id);
    }

    @Override
    public List<BaseCategory> getAllSubCategories(){
        return categoryRepository.getAllSubCategories();
    }

    @Override
    public List<SubCategoryWithTotalUrl> getSubCategoriesByMainCateId(int id){
        return categoryRepository.getSubCategoriesByMainCateId(id);
    }

    @Override
    public boolean insertSubCategory(SubCategoryAddForm category){
        if(categoryRepository.insertSubCategory(category)){
            if(!isKeywordEmpty(category.getKeywords())){
                return categoryRepository.insertCategoryKeywords(category.getKeywords(), category.getId());
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean insertMainCategory(CategoryAddForm category){
        if(categoryRepository.insertMainCategory(category)){
            if(!isKeywordEmpty(category.getKeywords())){
                return categoryRepository.insertCategoryKeywords(category.getKeywords(), category.getId());
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean updateSubCategory(SubCategoryAddForm category){
        if(categoryRepository.getCategoryById(category.getId()).getKeywords().size() == 0
                || categoryRepository.removeAllKeywordsByCategoryId(category.getId())){
            if(categoryRepository.updateSubCategory(category)){
                if(!isKeywordEmpty(category.getKeywords())) {
                    return categoryRepository.insertCategoryKeywords(category.getKeywords(), category.getId());
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean updateMainCategory(CategoryAddForm category){
        if(categoryRepository.getCategoryById(category.getId()).getKeywords().size() == 0
                || categoryRepository.removeAllKeywordsByCategoryId(category.getId())){
            if(categoryRepository.updateMainCategory(category)){
                if(!isKeywordEmpty(category.getKeywords())) {
                    return categoryRepository.insertCategoryKeywords(category.getKeywords(), category.getId());
                }
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean deleteCategoryById(int id){
        return categoryRepository.deleteCategoryById(id);
    }

    @Override
    public int getMainCateTotalUrl(int mainCateId){
        return categoryRepository.getMainCateTotalUrl(mainCateId);
    }

    @Override
    public int getSubCateTotalUrl(int subCateId){
        return categoryRepository.getSubCateTotalUrl(subCateId);
    }

    @Override
    public int countTotalMainCate(){
        return categoryRepository.countTotalMainCate();
    }

    @Override
    public int countTotalSubCate(){
        return categoryRepository.countTotalSubCate();
    }



    private boolean isKeywordEmpty(List<String> keywords){
        return keywords == null || keywords.size() == 0;
    }


    //venge implement on int getTotalCountUrlByMainId
    @Override
    public int getTotalCountUrlByMainId(int id) {
        return categoryRepository.getTotalCountUrlByMainId(id);
    }

}
