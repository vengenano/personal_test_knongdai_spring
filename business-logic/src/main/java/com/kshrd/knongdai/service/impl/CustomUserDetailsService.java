package com.kshrd.knongdai.service.impl;

import com.kshrd.knongdai.domain.User;
import com.kshrd.knongdai.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService{

	private UserService userService;
	
	@Autowired
	public CustomUserDetailsService(UserService userService) {
		this.userService = userService;
	}
	
	@Override
	public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

		User user = userService.findUserByEmail(email);
		if(user == null){
			System.out.println("User not found!");
			throw new UsernameNotFoundException("User not found!");
		}
		System.out.println("User email: " + user.getEmail());

		return user;
	}

}
