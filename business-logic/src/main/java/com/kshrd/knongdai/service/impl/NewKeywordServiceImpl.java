package com.kshrd.knongdai.service.impl;

import com.kshrd.knongdai.domain.ListNewKeywordWithTotalRecord;
import com.kshrd.knongdai.domain.form.NewKeywordCategoryMap;
import com.kshrd.knongdai.domain.form.NewKeywordUrlMap;
import com.kshrd.knongdai.persistence.NewKeywordRepository;
import com.kshrd.knongdai.service.NewKeywordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by User on 02-Aug-17.
 */
@Service
public class NewKeywordServiceImpl implements NewKeywordService {

    private NewKeywordRepository newKeywordRepository;

    @Autowired
    public NewKeywordServiceImpl(NewKeywordRepository newKeywordRepository){
        this.newKeywordRepository = newKeywordRepository;
    }

    @Override
    public List<ListNewKeywordWithTotalRecord> getKeywordsByPage(String sortBy, String order, Boolean mapped, int numRow, int page){
        return newKeywordRepository.getKeywordsByPage(sortBy, order, mapped, numRow, page);
    }

    @Override
    public boolean deleteKeyword(int id){
        return newKeywordRepository.deleteKeyword(id);
    }

    @Override
    public boolean mapKeywordToCategory(NewKeywordCategoryMap newKeywordCategoryMap){
        return newKeywordRepository.mapKeywordToCategory(newKeywordCategoryMap);
    }

    @Override
    public boolean mapKeywordToUrl(NewKeywordUrlMap newKeywordUrlMap){
        return newKeywordRepository.mapKeywordToUrl(newKeywordUrlMap);
    }

    @Override
    public int countTotalSearch(){
        return newKeywordRepository.countTotalSearch();
    }

    @Override
    public int countTotalSearchToday(){
        return newKeywordRepository.countTotalSearchToday();
    }
    
}
