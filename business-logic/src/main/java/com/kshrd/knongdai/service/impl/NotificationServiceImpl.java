package com.kshrd.knongdai.service.impl;


import com.kshrd.knongdai.domain.Notification;
import com.kshrd.knongdai.persistence.NotificationRepository;
import com.kshrd.knongdai.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class NotificationServiceImpl implements NotificationService {
    private NotificationRepository notificationRepository;

    @Autowired
    public NotificationServiceImpl(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    @Override
    public Notification getUserForNotification(int urlId) {
        return notificationRepository.getUserForNotification(urlId);
    }

    @Override
    public boolean insertNotification(Date requestDate, int urlId, int userId,String message) {
        return notificationRepository.insertNotification(requestDate,urlId,userId,message);
    }

    @Override
    public Notification getNotification(int userId) {
        return notificationRepository.getNotification(userId);
    }


}
