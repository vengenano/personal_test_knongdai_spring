package com.kshrd.knongdai.service.impl;

import com.kshrd.knongdai.domain.Role;
import com.kshrd.knongdai.persistence.RoleRepository;
import com.kshrd.knongdai.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by User on 22-Jul-17.
 */

@Service
public class RoleServiceImpl implements RoleService {

    private RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository){
        this.roleRepository = roleRepository;
    }

    @Override
    public List<Role> getAllRoles(){
        return roleRepository.getAllRoles();
    }

    @Override
    public boolean updateRole(Role role){
        return roleRepository.updateRole(role);
    }

    @Override
    public boolean deleteRole(int id){
        return roleRepository.deleteRole(id);
    }

    @Override
    public boolean insertRole(Role role){
        return roleRepository.insertRole(role);
    }

}
