package com.kshrd.knongdai.service.impl;

import com.kshrd.knongdai.domain.Url;
import com.kshrd.knongdai.persistence.UrlRepository;
import com.kshrd.knongdai.scrape.ScrapeAtoZpage;
import com.kshrd.knongdai.service.ScrapeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class ScrapeServiceImpl implements ScrapeService {

    private UrlRepository urlRepository;

    @Autowired
    public ScrapeServiceImpl(UrlRepository urlRepository){
        this.urlRepository = urlRepository;
    }

    @Override
    public List<Url> scrapeUrls(String webSite, String searchKeyword, String type){

//        List<Url> temp = new ArrayList<>();
//        Url u = new Url();
//        u.setTitle("Title");
//        u.setLink("www.facebook.com");
//        u.setEmail("facebook@gmail.com");
//        u.setPhone("0709498595");
//        u.setAddress("Address");
//        u.setDescription("Description");
//        u.setUrlType("w");
//        u.setPicUrl("asfasdfa90rq034rijdzsdf");
//        Url u1 = new Url();
//        u1.setTitle("Title");
//        u1.setLink("www.facebook.com");
//        u1.setEmail("facebook@gmail.com");
//        u1.setPhone("0709498595");
//        u1.setAddress("Address");
//        u1.setDescription("Description");
//        u1.setUrlType("w");
//        u1.setPicUrl("asfasdfa90rq034rijdzsdf");
//        temp.add(u);
//        temp.add(u1);
//
//        return temp;

        String url_type = type;
        int user_id = 1;
        int cate_id = 1;
        int start_page = 1;
        int until_page = 1;

        String mainUrl = webSite;

        List<Url> getAllPageUrl = new ArrayList<>();
        ArrayList<Url> filterUrl = new ArrayList<>();
        Set<Url> afterFindDuplicateLink = new HashSet<>();
        Set<String> test = new HashSet<>();

        try{
            if(mainUrl.equals("http://atozcambodia.com/")){
                System.out.println("Now scrape "+ mainUrl);
                for(int i = start_page ; i<= until_page; i++) {
//                    getAllPageUrl.addAll(ScrapeAtoZpage.extractData(mainUrl,"http://atozcambodia.com/maintence.php?cat="+i));
                    getAllPageUrl.addAll(ScrapeAtoZpage.extractData(mainUrl,"http://atozcambodia.com/searchres.php?local=&&key=" + searchKeyword));

                }
            }else if(mainUrl.equals("http://yp.com.kh/")){
                System.out.println("Now scrape "+ mainUrl);
                for(int i = start_page ; i<= until_page; i++) {
//                    getAllPageUrl.addAll(ScrapeYellowPage.extractData(mainUrl,"https://www.yp.com.kh/search_results?page="+i+"&q=ture&location_value="));
                }
            }else if(mainUrl.equals("https://www.cccdirectory.org")){
                System.out.println("Now scrape "+ mainUrl);
                for(int i = start_page ; i<= until_page; i++) {
//                    getAllPageUrl.addAll(ScrapeCccdirectory.extractData(mainUrl,"https://www.cccdirectory.org/locations/id1/listings?page="+i));
//                    getAllPageUrl.addAll(ScrapeCccdirectory.extractData(mainUrl,"https://www.cccdirectory.org/search?q=University&page="+i));
//                    getAllPageUrl.addAll(ScrapeCccdirectory.extractData(mainUrl,"https://www.cccdirectory.org/categories/id186/listings"));
                }
            }else if(mainUrl.equals("https://cambodia-pages.com")){
                System.out.println("Now scrape "+ mainUrl);
                for(int i = start_page; i<= until_page; i++){
                    //PLEASE CLICK ON MAIN CATEGORY AND THEN COMPY URL TO PAGE BELOW  LIKE BELOW
//                    getAllPageUrl.addAll(ScrapeCambodiaPage.extractData(mainUrl,"https://cambodia-pages.com/categories/khmer184-association/listings?page="+i));
//                    getAllPageUrl.addAll(ScrapeCambodiaPage.extractData(mainUrl,"https://cambodia-pages.com/categories/khmer256-universities/listings?page="+i));
//                    getAllPageUrl.addAll(ScrapeCambodiaPage.extractData(mainUrl,"https://cambodia-pages.com/search?page="+i+"&q=hotel"));
                }
            }else{
                System.out.println("No Url to scan");
            }
        }catch (Exception e){
            System.err.println("Adding data was error.");
        }

        //TEST SHOW ALL RECORD FROM WEB PAGES
        System.out.println("TEST SHOW ALL RECORD FROM WEB PAGES");
        for(Url show: getAllPageUrl ){
            System.out.println(show.getLink());
        }

        //find doublicate in list from webpage by it self
        System.out.println("FIND DUPLICATE IN LIST FROM WEB PAGE BY IT SELF");
        for(Url afterdeleteDuplicateLink : getAllPageUrl){
            if(test.add(afterdeleteDuplicateLink.getLink())){
                afterFindDuplicateLink.add(afterdeleteDuplicateLink);
            }else{
                System.err.println("Duplicate in one page : " + afterdeleteDuplicateLink.getLink());
            }
        }
//        Validation Url  From Web site and Url in our Database
        System.out.println("VALIDATE URL FROM WEB SITE TO URL FROM DATABASE");
        for(Url sh : afterFindDuplicateLink){
            try{
                if(sh.getLink() != ""){
                    if(sh.getLink().contains("www")){
                        if(urlRepository.isLinkExist(sh.getLink())){
                            //pic_with_domain = mainUrl + sh.getPic_url().toString();   // this is for AtoZCambodia page
                            filterUrl.add(sh);
                            System.out.println("New Link " + sh.getLink());
                        }
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return filterUrl;
    }

}
