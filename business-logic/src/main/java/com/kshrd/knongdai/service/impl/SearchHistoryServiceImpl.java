package com.kshrd.knongdai.service.impl;


import com.kshrd.knongdai.domain.SearchHistory;
import com.kshrd.knongdai.persistence.SearchHistoryRepository;
import com.kshrd.knongdai.service.SearchHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchHistoryServiceImpl implements SearchHistoryService {

     SearchHistoryRepository searchHistoryRepository;
    @Autowired
    public SearchHistoryServiceImpl(SearchHistoryRepository searchHistoryRepository) {
        this.searchHistoryRepository = searchHistoryRepository;
    }

    @Override
    public boolean insertSearchHistory(SearchHistory searchHistory) {
        return searchHistoryRepository.insertSearchHistory(searchHistory);
    }

    @Override
    public boolean deletedSearchHistoryStatus(SearchHistory searchHistory) {
        return searchHistoryRepository.deletedSearchHistoryStatus(searchHistory);
    }

    @Override
    public List<SearchHistory> getKeywordByUserId(int userId) {
        return searchHistoryRepository.getKeywordByUserId(userId);
    }


}
