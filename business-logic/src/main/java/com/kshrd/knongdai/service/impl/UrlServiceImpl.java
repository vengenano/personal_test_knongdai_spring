package com.kshrd.knongdai.service.impl;

import com.kshrd.knongdai.domain.*;
import com.kshrd.knongdai.persistence.NewKeywordRepository;
import com.kshrd.knongdai.persistence.UrlRepository;
import com.kshrd.knongdai.service.UrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UrlServiceImpl implements UrlService {

    private UrlRepository urlRepository;
    private NewKeywordRepository newKeywordRepository;

    @Autowired
    public UrlServiceImpl(UrlRepository urlRepository, NewKeywordRepository newKeywordRepository){
        this.urlRepository = urlRepository;
        this.newKeywordRepository = newKeywordRepository;
    }

    @Override
    public boolean insertUrl(Url url){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        boolean status = false;
        if(!auth.getPrincipal().equals("anonymousUser")){
            url.setUserId(((User)auth.getPrincipal()).getId());
            status = true;
        }else{
            url.setUserId(null);
        }
        url.setApproved(status);
        if(urlRepository.insertUrl(url)){
            return this.insertUrlKeywords(url);
        }
        return false;
    }

    @Override
    public Url getUrlById(int id){
        return urlRepository.getUrlById(id);
    }

    @Override
    public List<ListUrl> getUrlsByPage(
            int numRow,
            int page,
            String title,
            Integer mainCateId,
            Integer subCateId,
            Boolean approved
    ){

        return urlRepository.getUrlsByPage(numRow, page, (title == null) ? title : "%" + title + "%", mainCateId, subCateId, approved);
    }

    @Override
    public boolean updateUrl(Url url){
        System.out.println(url.getUrlType());
        return
                urlRepository.updateUrl(url) &&
                this.removeAllKeywordsByUrlId(url.getId()) &&
                this.insertUrlKeywords(url);
    }

    @Override
    public boolean deleteUrlById(int id){
        return urlRepository.deleteUrlById(id);
    }

    @Override
    public boolean updateApproved(int urlId, boolean approved){
        boolean result = false;
        if(urlRepository.updateApproved(urlId, approved)){
            result = urlRepository.updateKeywordsStatus(urlId, approved);
        }
        return result;
    }

    @Override
    public List<String> getSearchSuggestedKeywords(String keyword){
        return urlRepository.getSearchSuggestedKeywords(keyword + "%");
    }

    @Override
    public List<String> getUrlSuggestedKeywords(String keyword){
        return urlRepository.getUrlSuggestedKeywords(keyword + "%");
    }

    @Override
    public List<ResultUrlWithTotalRecord> queryUrlsByKeyword(String keyword, int limit, int page){
        List<ResultUrlWithTotalRecord> resultUrls = urlRepository.queryUrlsByKeyword("%" + keyword + "%", limit, page);
        if(page == 1){
            NewKeyword newKeyword = new NewKeyword();
            newKeyword.setKeywordName(keyword);
            newKeyword.setTotalResult((resultUrls.size() == 0) ? 0 : resultUrls.get(0).getTotalRecord());
            newKeywordRepository.insertNewKeyword(newKeyword);
        }
        return resultUrls;
    }

    @Override
    public List<ResultUrlWithTotalRecord> queryUrlsBySubCateId(int cateId, int limit, int page){
        return urlRepository.queryUrlsBySubCateId(cateId, limit, page);
    }

    @Override
    public boolean incrementUrlView(int id){
        return urlRepository.incrementUrlView(id);
    }

    private boolean removeAllKeywordsByUrlId(int urlId){
        if(urlRepository.getUrlKeywordsByUrlId(urlId).size() == 0){
            return true;
        }
        return urlRepository.removeAllKeywordsByUrlId(urlId);
    }

    private boolean insertUrlKeywords(Url url){
        if(isKeywordEmpty(url.getKeywords())){
            return true;
        }
        return urlRepository.insertUrlKeywords(url.getKeywords(), url.getId(), (url.isApproved() && url.isStatus()) );
    }

    private boolean isKeywordEmpty(List<String> keywords){
        return keywords == null || keywords.size() == 0;
    }

    @Override
    public boolean isLinkExist(String link){
        return urlRepository.isLinkExist(link);
    }

    @Override
    public int countActiveUrl(){
        return urlRepository.countActiveUrl();
    }

    @Override
    public List<Url> getAllUrls(){
        return urlRepository.getAllUrls();
    }

    @Override
    public List<UrlGetPopular> selectTopTen() {
        return urlRepository.selectTopTen();
    }


}
