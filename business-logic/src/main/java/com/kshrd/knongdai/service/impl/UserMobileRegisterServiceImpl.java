package com.kshrd.knongdai.service.impl;
import com.kshrd.knongdai.domain.UserMobileRegister;
import com.kshrd.knongdai.persistence.UserMobileRegisterRepository;
import com.kshrd.knongdai.service.UserMobileRegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserMobileRegisterServiceImpl implements UserMobileRegisterService {

    private UserMobileRegisterRepository userMobileRegisterRepository;

    @Autowired
    public UserMobileRegisterServiceImpl(UserMobileRegisterRepository userMobileRegisterRepository) {
        this.userMobileRegisterRepository = userMobileRegisterRepository;
    }

    @Override
    public boolean insertUserMobileRegister(UserMobileRegister userMobileRegister) {
        return userMobileRegisterRepository.insertUserMobileRegister(userMobileRegister);
    }

    @Override
    public UserMobileRegister getUserMobileRegister(int id) {
        return userMobileRegisterRepository.getUserMobileRegister(id);
    }

    @Override
    public boolean updateUserMobileRegister(UserMobileRegister userMobileRegister) {
        return userMobileRegisterRepository.updateUserMobileRegister(userMobileRegister);
    }
}
