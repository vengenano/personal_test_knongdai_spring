package com.kshrd.knongdai.service.impl;

import com.kshrd.knongdai.domain.form.UserAddForm;
import com.kshrd.knongdai.domain.form.UserUpdateForm;
import com.kshrd.knongdai.service.UserService;
import com.kshrd.knongdai.persistence.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.kshrd.knongdai.domain.User;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

	private UserRepository userRepository;
	private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
	
	@Autowired
	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}
	
	@Override
	public User findUserByEmail(String email) {
		return userRepository.findUserByEmail(email);
	}

	@Override
	public List<User> getAllUsers(){
		return userRepository.getAllUsers();
	}

	@Override
	public boolean updateUser(UserUpdateForm user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		return userRepository.updateUser(user);
	}

	@Override
	public boolean deleteUser(int userId){
		return userRepository.deleteUser(userId);
	}

	@Override
	public boolean createUser(UserAddForm user){
		String encryptPassword = bCryptPasswordEncoder.encode(user.getFirstPassword());
		user.setFirstPassword(encryptPassword);
		user.setSecondPassword(encryptPassword);
		return userRepository.createUser(user);
	}

	@Override
	public User findUserById(int id){
		return userRepository.findUserById(id);
	}
}
