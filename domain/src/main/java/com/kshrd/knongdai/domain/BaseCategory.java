package com.kshrd.knongdai.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**********************************************************************
 *
 * Author: Theara
 * Created Date: 26/July/2017
 * Development Group: PP_G3
 * Description: BaseCategory class is used for the Base Class for Category
 *              and CategoryAddForm. It consists of common fields for Category Table
 *
 **********************************************************************/

public class BaseCategory extends BaseEntity {

    @JsonProperty("cate_name")
    private String categoryName;
    @JsonProperty("des")
    private String description;
    @JsonProperty("icon_name")
    private String iconName;
    @JsonProperty("total_url")
    private int totalUrl;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description.trim();
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName.trim();
    }

    public int getTotalUrl() {
        return totalUrl;
    }

    public void setTotalUrl(int totalUrl) {
        this.totalUrl = totalUrl;
    }
}
