package com.kshrd.knongdai.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.sql.Date;

/**********************************************************************
 *
 * Author: Theara
 * Created Date: 21/July/2017
 * Development Group: PP_G3
 * Description: BaseEntity class is used for the Base Class of all Modal Class.
 *              It consists common two common field id and status fields.
 *
 **********************************************************************/

public class BaseEntity {

    private int id;
    private boolean status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
