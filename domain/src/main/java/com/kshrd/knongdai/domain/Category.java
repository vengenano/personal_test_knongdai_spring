package com.kshrd.knongdai.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**********************************************************************
 *
 * Author: Theara
 * Created Date: 26/July/2017
 * Development Group: PP_G3
 * Description: Category class is used to present as a response fields for Category
 *
 **********************************************************************/

public class Category extends BaseCategory {

    @JsonProperty("main_cate")
    private BaseCategory mainCategory;
    private List<Keyword> keywords;

    public BaseCategory getMainCategory() {
        return mainCategory;
    }

    public void setMainCategory(BaseCategory mainCategory) {
        this.mainCategory = mainCategory;
    }

    public List<Keyword> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<Keyword> keywords) {
        this.keywords = keywords;
    }
}
