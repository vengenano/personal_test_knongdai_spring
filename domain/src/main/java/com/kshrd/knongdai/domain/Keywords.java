package com.kshrd.knongdai.domain;

/**********************************************************************
 *
 * Author: Chanpheng
 * Created Date: 7/July/2017
 * Development Group: PP_G3
 * Description: Keyword class is used as a keyword field of Url Model
 *
 **********************************************************************/

public class Keywords {

    private int id;
    private int cate_id;
    private String keyword_name;

    public Keywords() {
    }

    public Keywords(int id, int cate_id, String keyword_name) {
        this.id = id;
        this.cate_id = cate_id;
        this.keyword_name = keyword_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCate_id() {
        return cate_id;
    }

    public void setCate_id(int cate_id) {
        this.cate_id = cate_id;
    }

    public String getKeyword_name() {
        return keyword_name;
    }

    public void setKeyword_name(String keyword_name) {
        this.keyword_name = keyword_name;
    }
}
