package com.kshrd.knongdai.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**********************************************************************
 *
 * Author: Theara
 * Created Date: 7/Aug/2017
 * Development Group: PP_G3
 * Description: ListNewKeyWord class is used
 *              as an overview of the statistic
 *              of the new keyword that users have searched
 *              from our System.
 *
 **********************************************************************/

public class ListNewKeyword extends NewKeyword {

    @JsonProperty("total_traffic")
    private int totalTraffic;

    private boolean mapped;

    public int getTotalTraffic() {
        return totalTraffic;
    }

    public void setTotalTraffic(int totalTraffic) {
        this.totalTraffic = totalTraffic;
    }

    public boolean isMapped() {
        return mapped;
    }

    public void setMapped(boolean mapped) {
        this.mapped = mapped;
    }
}
