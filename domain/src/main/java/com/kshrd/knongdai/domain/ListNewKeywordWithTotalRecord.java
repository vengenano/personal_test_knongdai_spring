package com.kshrd.knongdai.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**********************************************************************
 *
 * Author: Theara
 * Created Date: 7/Aug/2017
 * Development Group: PP_G3
 * Description: ListNewKeyWordWithTotalRecord class is an extension of
 *              the ListNewKeyWord with additional totalRecord field
 *
 **********************************************************************/

public class ListNewKeywordWithTotalRecord extends ListNewKeyword {

    @JsonIgnore
    private int totalRecord;

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }
}
