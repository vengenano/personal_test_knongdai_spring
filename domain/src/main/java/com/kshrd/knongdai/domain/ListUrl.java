package com.kshrd.knongdai.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**********************************************************************
 *
 * Author: Sokheang
 * Created Date: 2/Aug/2017
 * Development Group: PP_G3
 * Description: ListUrl class is used for representation the response of
 *              Url result to the client side
 *
 **********************************************************************/

public class ListUrl extends ResultUrl {

    @JsonProperty("sub_cate")
    private String subCategory;

    @JsonProperty("main_cate")
    private String mainCategory;

    @JsonIgnore
    private int totalRecord;

    @JsonProperty("approved")
    private boolean approved;

    public String getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(String subCategory) {
        this.subCategory = subCategory;
    }

    public String getMainCategory() {
        return mainCategory;
    }

    public void setMainCategory(String mainCategory) {
        this.mainCategory = mainCategory;
    }

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }
}
