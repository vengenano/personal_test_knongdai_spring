package com.kshrd.knongdai.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**********************************************************************
 *
 * Author: Theara
 * Created Date: 26/July/2017
 * Development Group: PP_G3
 * Description: MainCategory is an extension of BaseCategory class
 *              with an additional SubCate field.
 *              It's used to present as Main Category
 *
 **********************************************************************/

public class MainCategory extends BaseCategory {

    @JsonProperty("sub_cate")
    private List<BaseCategory> subCategories;

    public List<BaseCategory> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<BaseCategory> subCategories) {
        this.subCategories = subCategories;
    }

}
