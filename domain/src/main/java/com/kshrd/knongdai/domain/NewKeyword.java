package com.kshrd.knongdai.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**********************************************************************
 *
 * Author: Sokheang
 * Created Date: 2/Aug/2017
 * Development Group: PP_G3
 * Description: NewKeyword class is used for represent the statistic
 *              of the keyword that users have search in our System
 *
 **********************************************************************/

public class NewKeyword {

    @JsonProperty("keyword_name")
    private String keywordName;

    @JsonProperty("total_result")
    private int totalResult;

    public String getKeywordName() {
        return keywordName;
    }

    public void setKeywordName(String keywordName) {
        this.keywordName = keywordName;
    }

    public int getTotalResult() {
        return totalResult;
    }

    public void setTotalResult(int totalResult) {
        this.totalResult = totalResult;
    }
}
