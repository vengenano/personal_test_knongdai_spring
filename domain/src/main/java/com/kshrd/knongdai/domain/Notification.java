package com.kshrd.knongdai.domain;

import java.util.Date;
import java.util.StringJoiner;

public class Notification {
    private int id;
    private int userId;
    private int urlId;
    private String userPlayerId;
    private String title;
    private String link;
    private boolean isApproval;
    private Date requestDate;
    private String requestedDate;
    private String sendDate;
    private String message;

    public Notification() {
    }

    public Notification(int id, int userId, int urlId, String userPlayerId, String title, String link, boolean isApproval, Date requestDate, String message, String sendDate,String requestedDate) {
        this.id=id;
        this.userId = userId;
        this.urlId = urlId;
        this.userPlayerId = userPlayerId;
        this.title = title;
        this.link = link;
        this.isApproval=isApproval;
        this.requestDate=requestDate;
        this.message=message;
        this.sendDate=sendDate;
        this.requestedDate=requestedDate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getUrlId() {
        return urlId;
    }

    public void setUrlId(int urlId) {
        this.urlId = urlId;
    }

    public String getUserPlayerId() {
        return userPlayerId;
    }

    public void setUserPlayerId(String userPlayerId) {
        this.userPlayerId = userPlayerId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public boolean isApproval() {
        return isApproval;
    }

    public void setApproval(boolean approval) {
        isApproval = approval;
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(Date requestDate) {
        this.requestDate = requestDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSendDate() {
        return sendDate;
    }

    public void setSendDate(String sendDate) {
        this.sendDate = sendDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRequestedDate() {
        return requestedDate;
    }

    public void setRequestedDate(String requestedDate) {
        this.requestedDate = requestedDate;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "id=" + id +
                ", userId=" + userId +
                ", urlId=" + urlId +
                ", userPlayerId='" + userPlayerId + '\'' +
                ", title='" + title + '\'' +
                ", link='" + link + '\'' +
                ", isApproval=" + isApproval +
                ", requestDate=" + requestDate +
                ", requestedDate='" + requestedDate + '\'' +
                ", sendDate='" + sendDate + '\'' +
                ", message='" + message + '\'' +
                '}';
    }
}
