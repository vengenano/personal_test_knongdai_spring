package com.kshrd.knongdai.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**********************************************************************
 *
 * Author: Theara
 * Created Date: 2/Aug/2017
 * Development Group: PP_G3
 * Description: ResultUrlWithTotalRecord is an extension of ResultUrl class
 *              with additional totalRecord field
 *
 **********************************************************************/

public class ResultUrlWithTotalRecord extends ResultUrl {

    @JsonIgnore
    int totalRecord;

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }
}
