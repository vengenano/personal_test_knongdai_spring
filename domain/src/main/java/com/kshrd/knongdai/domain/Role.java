package com.kshrd.knongdai.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.security.core.GrantedAuthority;

/**********************************************************************
 *
 * Author: Chanpheng
 * Created Date: 23/July/2017
 * Development Group: PP_G3
 * Description: Role class is used for authorize
 *              the role of every requests to see whether the login user
 *              has right access to the specified page
 *
 **********************************************************************/

public class Role extends BaseEntity implements GrantedAuthority {

    @JsonProperty("role_name")
    private String roleName;
    private static final long serialVersionUID = 1L;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName =  roleName.trim();
    }

    @Override
    @JsonIgnore
    public String getAuthority() {
        return "ROLE_" + roleName;
    }

    public static long getSerialversionuid() {
        return serialVersionUID;
    }

}
