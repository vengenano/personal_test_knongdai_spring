package com.kshrd.knongdai.domain;

public class SearchHistory {
    private  int userId;
    private String keyword;
    private int count_keyword;
    private boolean status;

    public SearchHistory(){

    }

    public SearchHistory(int userId, String keyword, int count_keyword, boolean status) {
        this.userId = userId;
        this.keyword = keyword;
        this.count_keyword = count_keyword;
        this.status = status;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int getCount_keyword() {
        return count_keyword;
    }

    public void setCount_keyword(int count_keyword) {
        this.count_keyword = count_keyword;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
