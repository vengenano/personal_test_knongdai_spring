package com.kshrd.knongdai.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**********************************************************************
 *
 * Author: Theara
 * Created Date: 13/Aug/2017
 * Development Group: PP_G3
 * Description: SubCategoryWithTotalUrl is an extension of BaseCategory
 *              with additional totalUrl fields
 *
 **********************************************************************/
public class SubCategoryWithTotalUrl extends BaseCategory {

    @JsonProperty("total_url")
    private int totalUrl;

    public int getTotalUrl() {
        return totalUrl;
    }

    @JsonIgnore
    public void setTotalUrl(int totalUrl) {
        this.totalUrl = totalUrl;
    }
}


