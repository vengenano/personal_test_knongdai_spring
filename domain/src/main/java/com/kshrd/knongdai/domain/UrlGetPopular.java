package com.kshrd.knongdai.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class UrlGetPopular {
   private int id;
   private String title;
   private String link;
   private int cateId;
   private String urlType;
   private int userId;
   private String address;
   private String description;
   private String picUrl;
   private String phone;
   private String email;
   private boolean isApproved;
   private int view;
   private boolean status;
   private String keywords;


    public UrlGetPopular() {
    }

    public UrlGetPopular(int id, String title, String link, int cateId, String urlType, int userId, String address, String description, String picUrl, String phone, String email, boolean isApproved, int view, boolean status, String keywords) {
        this.id = id;
        this.title = title;
        this.link = link;
        this.cateId = cateId;
        this.urlType = urlType;
        this.userId = userId;
        this.address = address;
        this.description = description;
        this.picUrl = picUrl;
        this.phone = phone;
        this.email = email;
        this.isApproved = isApproved;
        this.view = view;
        this.status = status;
        this.keywords = keywords;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getCateId() {
        return cateId;
    }

    public void setCateId(int cateId) {
        this.cateId = cateId;
    }

    public String getUrlType() {
        return urlType;
    }

    public void setUrlType(String urlType) {
        this.urlType = urlType;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isApproved() {
        return isApproved;
    }

    public void setApproved(boolean approved) {
        isApproved = approved;
    }

    public int getView() {
        return view;
    }

    public void setView(int view) {
        this.view = view;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    @Override
    public String toString() {
        return "UrlGetPopular{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", link='" + link + '\'' +
                ", cateId=" + cateId +
                ", urlType='" + urlType + '\'' +
                ", userId=" + userId +
                ", address='" + address + '\'' +
                ", description='" + description + '\'' +
                ", picUrl='" + picUrl + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", isApproved=" + isApproved +
                ", view=" + view +
                ", status=" + status +
                ", keywords='" + keywords + '\'' +
                '}';
    }
}
