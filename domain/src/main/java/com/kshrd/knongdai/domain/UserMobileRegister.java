package com.kshrd.knongdai.domain;

public class UserMobileRegister  {
    private int id;
    private String userName;
    private String facebookId;
    private String phoneNumber;
    private String image;
    private String gender;
    private String userPlayerId;


    public UserMobileRegister(){}

    public UserMobileRegister(int id,String userName, String facebookId, String phoneNumber, String image, String gender,String userPlayerId) {
        this.id=id;
        this.userName = userName;
        this.facebookId = facebookId;
        this.phoneNumber = phoneNumber;
        this.image = image;
        this.gender = gender;
        this.userPlayerId=userPlayerId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserPlayerId() {
        return userPlayerId;
    }

    public void setUserPlayerId(String userPlayerId) {
        this.userPlayerId = userPlayerId;
    }

    @Override
    public String toString() {
        return "UserMobileRegister{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", facebookId='" + facebookId + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", image='" + image + '\'' +
                ", gender='" + gender + '\'' +
                ", userPlayerId='" + userPlayerId + '\'' +
                '}';
    }
}
