package com.kshrd.knongdai.domain;

import com.kshrd.knongdai.domain.form.UserAddForm;

public class UserMoblie extends UserAddForm {
    private String facebookId;
    private String phoneNumber;
    private String image;
    private String gender;

    public UserMoblie(){

    }

    public UserMoblie(String facebookId, String phoneNumber, String image, String gender) {
        this.facebookId = facebookId;
        this.phoneNumber = phoneNumber;
        this.image = image;
        this.gender = gender;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
