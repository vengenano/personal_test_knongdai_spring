package com.kshrd.knongdai.domain.form;

import com.fasterxml.jackson.annotation.JsonProperty;

/**********************************************************************
 *
 * Author: MouyKea
 * Created Date: 8/Aug/2017
 * Development Group: PP_G3
 * Description: NewKeywordCategoryMap class is used for mapping new keyword
 *              to Category
 *
 **********************************************************************/

public class NewKeywordCategoryMap {

    private String keyword;

    @JsonProperty("cate_id")
    private int categoryId;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}
