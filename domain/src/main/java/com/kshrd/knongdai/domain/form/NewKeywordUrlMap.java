package com.kshrd.knongdai.domain.form;

import com.fasterxml.jackson.annotation.JsonProperty;

/**********************************************************************
 *
 * Author: Phen
 * Created Date: 8/Aug/2017
 * Development Group: PP_G3
 * Description: NewKeywordUrlMap class is used for mapping new keyword
 *              to Url
 *
 **********************************************************************/

public class NewKeywordUrlMap {

    private String keyword;

    @JsonProperty("url_id")
    private int urlId;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public int getUrlId() {
        return urlId;
    }

    public void setUrlId(int urlId) {
        this.urlId = urlId;
    }
}
