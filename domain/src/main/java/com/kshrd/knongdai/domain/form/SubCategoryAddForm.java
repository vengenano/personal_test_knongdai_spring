package com.kshrd.knongdai.domain.form;

import com.fasterxml.jackson.annotation.JsonProperty;

/**********************************************************************
 *
 * Author: Lyhout
 * Created Date: 24/July/2017
 * Development Group: PP_G3
 * Description: SubCategoryAddForm class is used for SubCategory adding operation.
 *
 **********************************************************************/

public class SubCategoryAddForm extends CategoryAddForm {

    @JsonProperty("main_cate_id")
    int mainCategoryId;

    public int getMainCategoryId() {
        return mainCategoryId;
    }

    public void setMainCategoryId(int mainCategoryId) {
        this.mainCategoryId = mainCategoryId;
    }
}
