package com.kshrd.knongdai.domain.form;

import com.fasterxml.jackson.annotation.JsonProperty;

/**********************************************************************
 *
 * Author: Vannak
 * Created Date: 26/July/2017
 * Development Group: PP_G3
 * Description: UserUpdateForm class is used for User update operation.
 *
 **********************************************************************/

public class UserUpdateForm {

    private int id;
    @JsonProperty("user_name")
    private String userName;
    private String email;
    private String password;
    @JsonProperty("role_id")
    private int roleId;
    private boolean status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
