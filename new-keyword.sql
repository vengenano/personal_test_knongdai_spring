CREATE or REPLACE function pr_new_keyword(sort_by VARCHAR(15), order_by INT, num_row INT, page INT)
returns TABLE(keyword_name TEXT, total_traffic BIGINT, total_result BIGINT, mapped BOOLEAN, total_record BIGINT)
LANGUAGE plpgsql
AS $$
BEGIN
RETURN QUERY
(
  SELECT NK.keyword_name, COUNT(*) AS total_traffic, SUM(NK.total_result) AS total_result,
    EXISTS (SELECT 1 FROM kd_cate_keyword CK WHERE CK.keyword_name = NK.keyword_name
      UNION
      SELECT 1 FROM kd_keyword UK WHERE UK.keyword_name = NK.keyword_name
      UNION
      SELECT 1 FROM kd_url U WHERE U.title = NK.keyword_name
    ) AS mapped,
    COUNT(*) OVER() AS total_record
  FROM kd_new_keyword NK
  WHERE NK.status = TRUE
  GROUP BY NK.keyword_name
  ORDER BY
    CASE WHEN order_by = 0 THEN sort_by END DESC,
    CASE WHEN order_by = 1 THEN sort_by END ASC
  LIMIT num_row OFFSET (num_row * (page - 1))
);

END;
$$;
