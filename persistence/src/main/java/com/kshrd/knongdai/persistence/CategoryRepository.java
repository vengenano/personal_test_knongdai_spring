package com.kshrd.knongdai.persistence;

import com.kshrd.knongdai.domain.*;
import com.kshrd.knongdai.domain.form.CategoryAddForm;
import com.kshrd.knongdai.domain.form.SubCategoryAddForm;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import java.util.List;

/**********************************************************************
 *
 * Author: Theara
 * Created Date: 27/July/2017
 * Development Group: PP_G3
 * Description: CategoryRepository interface is used as DAO
 *              for manipulating Category
 *
 **********************************************************************/

@Repository
public interface CategoryRepository {

    // Get all MainCategories and its Category
    @Select("SELECT id, cate_name, description, icon_name, status FROM kd_category " +
            "WHERE status = TRUE AND main_cate_id IS NULL ORDER BY id")
    @Results(value={
            @Result(property="id", column="id"),
            @Result(property="categoryName", column="cate_name"),
            @Result(property="iconName", column="icon_name"),
            @Result(property = "subCategories", column = "id", many = @Many(select = "getSubCategoriesByMainCateId")),
            @Result(property = "totalUrl", column = "id", many = @Many(select = "getTotalCountUrlByMainId"))
    })
    public List<MainCategory> getAllMainCategories();

//    get SubCategories by its MainCategory ID
    @Select("SELECT id, cate_name, description, status " +
            "FROM kd_category WHERE status = TRUE AND main_cate_id = #{main_cate_id}")
    @Results(value={
            @Result(property="id", column="id"),
            @Result(property="categoryName", column="cate_name"),
            @Result(property = "totalUrl", column = "id", one = @One(select = "getSubCateTotalUrl"))
    })
    public List<SubCategoryWithTotalUrl> getSubCategoriesByMainCateId(@Param("main_cate_id") int id);

    // Get all Sub Categories without its Main Category detail
    @Select("SELECT id, cate_name, description, status " +
            "FROM kd_category WHERE status = TRUE AND main_cate_id IS NOT NULL")
    @Results(value={
            @Result(property="categoryName", column="cate_name")
    })
    public List<BaseCategory> getAllSubCategories();


//    get category by Id
    @Select("SELECT id, main_cate_id, " +
            "cate_name, icon_name, description, status " +
            "FROM kd_category S WHERE status = TRUE AND id = #{id}")
    @Results(value = {
            @Result(property="id", column="id"),
            @Result(property = "mainCategory.id", column = "main_cate_id"),
            @Result(property = "mainCategory.categoryName", column = "main_cate_name"),
            @Result(property = "categoryName", column = "cate_name"),
            @Result(property = "iconName", column = "icon_name"),
            @Result(property = "mainCategory", column = "main_cate_id", one = @One(select = "getMainCategoryById")),
            @Result(property = "keywords", column = "id", many = @Many(select = "getCategoryKeywordsByCategoryId"))
    })
    public Category getCategoryById(@Param("id") int id);


//    get main category by ID
//    Use BaseCategory, so that it has no additional field, example sub_cate or main_cate
    @Select("SELECT id, cate_name, description, icon_name, status " +
            "FROM kd_category WHERE status = TRUE AND main_cate_id IS NULL AND id = #{id}")
    @Results(value={
            @Result(property="categoryName", column="cate_name"),
            @Result(property="iconName", column="icon_name")
    })
    public BaseCategory getMainCategoryById(@Param("id") int id);

//    Insert new Sub Category
    @Insert("INSERT INTO kd_category (main_cate_id, cate_name, icon_name, description) VALUES (" +
            "#{cate.mainCategoryId}, " +
            "#{cate.categoryName}, " +
            "#{cate.iconName}, " +
            "#{cate.description}" +
            ")")
    @Options(
            useGeneratedKeys = true,
            keyProperty = "cate.id", keyColumn = "id")
    public boolean insertSubCategory(@Param("cate") SubCategoryAddForm category);

//    Insert new Main Category
    @Insert("INSERT INTO kd_category (cate_name, icon_name, description) VALUES (" +
            "#{cate.categoryName}, " +
            "#{cate.iconName}, " +
            "#{cate.description}" +
            ")")
    @Options(
            useGeneratedKeys = true,
            keyProperty = "cate.id", keyColumn = "id")
    public boolean insertMainCategory(@Param("cate") CategoryAddForm category);

//    Update Sub Category
    @Update("UPDATE kd_category SET " +
            "main_cate_id = #{cate.mainCategoryId}, " +
            "cate_name = #{cate.categoryName}, " +
            "icon_name = #{cate.iconName}, " +
            "description = #{cate.description}, " +
            "status = #{cate.status} WHERE id = #{cate.id}")
    public boolean updateSubCategory(@Param("cate") SubCategoryAddForm category);

    //    Update Main Category
    @Update("UPDATE kd_category SET " +
            "cate_name = #{cate.categoryName}, " +
            "icon_name = #{cate.iconName}, " +
            "description = #{cate.description}, " +
            "status = #{cate.status} WHERE id = #{cate.id}")
    public boolean updateMainCategory(@Param("cate") CategoryAddForm category);

//    Delete Category by setting the status to false
    @Update("UPDATE kd_category SET status = FALSE WHERE id = #{id} AND status = TRUE")
    public boolean deleteCategoryById(@Param("id") int id);

    //Count URLs by Main Category
    @Select("SELECT COUNT(*) FROM kd_url U " +
                "JOIN kd_category S ON U.cate_id = S.id " +
            "WHERE S.id IN (SELECT M.id FROM kd_category M WHERE M.main_cate_id = #{main_cate_id})")
    public int getMainCateTotalUrl(@Param("main_cate_id") int mainCateId);

    @Select("SELECT COUNT(*) FROM kd_url U " +
                "JOIN kd_category C ON U.cate_id = C.id " +
            "WHERE C.id = #{id} AND main_cate_id IS NOT NULL AND U.status = TRUE AND U.is_approved = TRUE")
    public int getSubCateTotalUrl(@Param("id") int subCateId);

    @Select("SELECT COUNT(*) FROM kd_category WHERE status = TRUE AND main_cate_id IS NULL")
    int countTotalMainCate();

    @Select("SELECT COUNT(*) FROM kd_category WHERE status = TRUE AND main_cate_id IS NOT NULL")
    int countTotalSubCate();

    //venge implement on Count tatal url of subcategory by main id

    @Select("select count(kd_url.link) from kd_category left join kd_url on kd_category.id = kd_url.cate_id where main_cate_id= #{id} and kd_category.status=true and kd_url.is_approved=true;")
    int getTotalCountUrlByMainId(@Param("id") int id);




//****************    Starting Category Keywords ******************************

    // Get all Category Keywords by Category ID
    @Select("SELECT id, keyword_name, status " +
            "FROM kd_cate_keyword WHERE cate_id = #{cate_id} AND status = TRUE")
    @Results(value = {
            @Result(property = "keywordName", column = "keyword_name")
    })
    public List<Keyword> getCategoryKeywordsByCategoryId(@Param("cate_id") int categoryId);

    //    Insert Category keywords, it runs after insertSubCategory or insertMainCategory with safe @Transaction
    @Insert("<script>INSERT INTO kd_cate_keyword(cate_id, keyword_name)" +
            " VALUES" +
            " <foreach collection = 'keywords' item='keyword' separator=','>" +
            " (#{cate_id}, #{keyword})" +
            " </foreach>" +
            "</script>")
    public boolean insertCategoryKeywords(@Param("keywords") List<String> keywords, @Param("cate_id") int cateId);

    // Remove all keywords of Categories by cate_id
    @Delete("DELETE FROM kd_cate_keyword WHERE cate_id = #{cate_id}")
    public boolean removeAllKeywordsByCategoryId(@Param("cate_id") int cateId);
}
