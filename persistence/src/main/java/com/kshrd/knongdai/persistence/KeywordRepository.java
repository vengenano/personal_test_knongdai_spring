package com.kshrd.knongdai.persistence;

import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**********************************************************************
 *
 * Author: Chanpheng
 * Created Date: 26/July/2017
 * Development Group: PP_G3
 * Description: KeywordRepository interface is used as DAO
 *              for manipulating NewKeyword
 *
 **********************************************************************/
@Repository
public interface KeywordRepository {
    @Select("SELECT keyword_name " +
            "FROM kd_cate_keyword WHERE keyword_name ilike #{name}")
    public List<String> getkeyword(@Param("name")String name);
}
