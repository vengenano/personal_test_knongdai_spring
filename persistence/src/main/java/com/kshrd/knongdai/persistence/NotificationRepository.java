package com.kshrd.knongdai.persistence;


import com.kshrd.knongdai.domain.Notification;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Properties;


/**********************************************************************
 *
 * Author: Songden
 * Created Date: 15/January/2019
 * Development Group: PP_G2
 * Description: NotificationRepository interface is used as DAO
 *              for manipulating Notification
 *
 **********************************************************************/


@Repository
public interface NotificationRepository {

    //-------- get user and url info fro push notification ---------------------//
    @Select("SELECT *FROM get_user_for_notification(#{urlId})")
    @Results(value = {
            @Result(property = "userId", column = "r_user_id"),
            @Result(property = "userPlayerId", column = "r_user_player_id"),
            @Result(property = "urlId", column = "r_url_id"),
            @Result(property = "title", column = "r_title"),
            @Result(property = "link", column = "r_link"),
            @Result(property= "isApproval", column = "r_is_approved"),
            @Result(property = "requestDate",column = "r_created_date")})
    Notification getUserForNotification(@Param("urlId") int urlId);

    //----------------- for insert data after push notification to table notification ------------//

    @Insert("INSERT INTO kd_notification(request_date, url_id, user_id,message)" +
            "VALUES(#{requestDate},#{urlId},#{userId},#{message})")
    public boolean insertNotification(@Param("requestDate") Date requestDate, @Param("urlId") int urlId, @Param("userId") int userId,@Param("message") String message);

    //-------------------- get all information from notification -----------------------//

    @Select("SELECT *FROM get_notification(#{userId})")
    @Results(value = {
            @Result(property = "id", column = "r_notification_id"),
            @Result(property = "userPlayerId", column = "r_user_player_id"),
            @Result(property = "urlId", column = "r_url_id"),
            @Result(property = "title", column = "r_title"),
            @Result(property = "link", column = "r_link"),
            @Result(property = "requestedDate",column = "r_request_date"),
            @Result(property = "sendDate",column = "r_sent_date"),
            @Result(property = "message",column = "r_message")})
    public Notification getNotification(@Param("userId") int userId);

}
