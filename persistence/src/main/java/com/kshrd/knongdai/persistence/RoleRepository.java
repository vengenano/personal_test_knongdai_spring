package com.kshrd.knongdai.persistence;

import com.kshrd.knongdai.domain.Role;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

/**********************************************************************
 *
 * Author: MouyKea
 * Created Date: 27/July/2017
 * Development Group: PP_G3
 * Description: RoleRepository interface is used as DAO
 *              for manipulating user role
 *
 **********************************************************************/

import java.util.ArrayList;

@Repository
public interface RoleRepository {

	@Select("SELECT id, role_name FROM kd_role WHERE status = TRUE")
	@Results(value={
			@Result(property="roleName", column="role_name")
	})
	public ArrayList<Role> getAllRoles();

	@Update("UPDATE kd_role SET " +
			"role_name = #{role.roleName}, " +
			"status = #{role.status} " +
			"WHERE id = #{role.id}")
	public boolean updateRole(@Param("role") Role role);

	@Update("UPDATE kd_role SET " +
			"status = false " +
			"WHERE id = #{id} AND status = TRUE")
	public boolean deleteRole(@Param("id") int id);

	@Insert("INSERT INTO kd_role (role_name) VALUES (#{role.roleName})")
	@Options(
			useGeneratedKeys = true,
			keyProperty = "role.id", keyColumn = "id")
	public boolean insertRole(@Param("role") Role role);
	
}
