package com.kshrd.knongdai.persistence;


import com.kshrd.knongdai.domain.SearchHistory;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SearchHistoryRepository {

    @Select("SELECT * FROM func_add_search_history(#{searchHistory.userId},#{searchHistory.keyword})")
     boolean insertSearchHistory(@Param("searchHistory") SearchHistory searchHistory);

    @Update("UPDATE kd_search_history SET status=FALSE where user_id=#{searchHistory.userId} and keyword=#{searchHistory.keyword}")
    boolean deletedSearchHistoryStatus(@Param("searchHistory") SearchHistory searchHistory);

    @Select("SELECT * from kd_search_history where user_id=#{userId} and status=true")
    public List<SearchHistory> getKeywordByUserId(@Param("userId") int userId);
}
