package com.kshrd.knongdai.persistence;

import com.kshrd.knongdai.domain.ListUrl;
import com.kshrd.knongdai.domain.ResultUrlWithTotalRecord;
import com.kshrd.knongdai.domain.Url;
import com.kshrd.knongdai.domain.UrlGetPopular;
import com.kshrd.knongdai.persistence.sql.KnongdaiSQLBuilder;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.StatementType;
import org.springframework.stereotype.Repository;
import java.util.List;

/**********************************************************************
 *
 * Author: Theara
 * Created Date: 27/July/2017
 * Development Group: PP_G3
 * Description: UrlRepository interface is used as DAO
 *              for manipulating Url
 *
 **********************************************************************/

@Repository
public interface UrlRepository {

    @Insert("INSERT INTO kd_url(title, cate_id, url_type, user_id, link, address, phone, email, description, " +
            "pic_url, is_approved) VALUES (" +
            "#{url.title}, #{url.subCateId}, #{url.urlType}, #{url.userId}, #{url.link}, " +
            "#{url.address}, #{url.phone}, #{url.email},#{url.description}, #{url.picUrl}, #{url.approved})")
    @Options(
            useGeneratedKeys = true,
            keyProperty = "url.id", keyColumn = "id")
    public boolean insertUrl(@Param("url") Url url);

    @Select("SELECT U.id, U.title, U.cate_id AS sub_cate_id, " +
            "(SELECT main_cate_id FROM kd_category C WHERE C.id = U.cate_id) AS main_cate_id, U.url_type, " +
            "U.user_id, U.link, U.address, U.phone, U.email, " +
            "U.description, U.pic_url, U.status, U.is_approved, U.view " +
            "FROM kd_url U " +
            "WHERE U.id = #{url_id} AND U.status = TRUE")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "mainCateId", column = "main_cate_id"),
            @Result(property = "subCateId", column = "sub_cate_id"),
            @Result(property = "urlType", column = "url_type"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "picUrl", column = "pic_url"),
            @Result(property = "approved", column = "is_approved"),
            @Result(property = "keywords", column = "id", many = @Many(select = "getUrlKeywordsByUrlId"))
    })
    Url getUrlById(@Param("url_id") int id);

    @SelectProvider(type = KnongdaiSQLBuilder.class, method = "getUrlsByPage")
    @Results(value = {
            @Result(property = "urlType", column = "urL_type"),
            @Result(property = "picUrl", column = "pic_url"),
            @Result(property = "mainCategory", column = "main_cate"),
            @Result(property = "subCategory", column = "sub_cate"),
            @Result(property = "approved", column = "is_approved"),
            @Result(property = "totalRecord", column = "total_record")
    })
    List<ListUrl> getUrlsByPage(
            @Param("num_row") int numRow,
            @Param("page") int page,
            @Param("title") String title,
            @Param("main_cate_id") Integer mainCateId,
            @Param("sub_cate_id") Integer subCateId,
            @Param("approved") Boolean approved
    );

    @Update("UPDATE kd_url SET title = #{url.title}, cate_id = #{url.subCateId}, url_type = #{url.urlType}, " +
            "link = #{url.link}, address = #{url.address}, phone = #{url.phone}, email = #{url.email}, " +
            "description = #{url.description}, pic_url = #{url.picUrl}, status = #{url.status}, is_approved = #{url.approved} " +
            "WHERE id = #{url.id}")
    public boolean updateUrl(@Param("url") Url url);

    @Update("UPDATE kd_url SET view = view + 1 WHERE id = #{id} AND status = TRUE AND is_approved = TRUE")
    boolean incrementUrlView(@Param("id") int id);

    // Delete URl by setting URL status to false
    @Update("UPDATE kd_url SET status = FALSE WHERE id = #{id}")
    boolean deleteUrlById(@Param("id") int id);

    @Update("UPDATE kd_url SET is_approved = #{approved} WHERE id = #{id}")
    boolean updateApproved(@Param("id") int id, @Param("approved") boolean approved);

    @Select("SELECT * FROM kd_url;")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "mainCateId", column = "main_cate_id"),
            @Result(property = "subCateId", column = "sub_cate_id"),
            @Result(property = "urlType", column = "url_type"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "picUrl", column = "pic_url"),
            @Result(property = "approved", column = "is_approved"),
            @Result(property = "keywords", column = "id", many = @Many(select = "getUrlKeywordsByUrlId"))
    })
    List<Url> getAllUrls();

    @Select("SELECT status FROM kd_url WHERE id=#{id}")
    boolean getURLStatus(int id);


    // ************* Starting of URL keywords


    @Insert("<script>INSERT INTO kd_keyword(url_id, keyword_name, status)" +
            " VALUES" +
            " <foreach collection = 'keywords' item='keyword' separator=','>" +
            " (#{url_id}, #{keyword}, #{status})" +
            " </foreach>" +
            "</script>")
    boolean insertUrlKeywords(@Param("keywords")List<String> keywords, @Param("url_id") int urlId, @Param("status") boolean status);

    @Select("SELECT keyword_name FROM kd_keyword WHERE url_id = #{url_id}")
    List<String> getUrlKeywordsByUrlId(@Param("url_id") int urlId);

    //Delete keywords by UrlID
    @Delete("DELETE FROM kd_keyword WHERE url_id = #{url_id}")
    boolean removeAllKeywordsByUrlId(@Param("url_id") int urlId);

    //Get keyword suggestion by similar keyword
    @Select("SELECT DISTINCT keyword_name FROM " +
            "(" +
            "SELECT title AS keyword_name FROM kd_url WHERE title ilike #{keyword} AND status = TRUE AND is_approved = TRUE " +
            "UNION " +
            "SELECT keyword_name FROM kd_keyword WHERE keyword_name ilike #{keyword} AND status = TRUE " +
            "UNION " +
            "SELECT keyword_name FROM kd_cate_keyword WHERE keyword_name ilike #{keyword} AND status = TRUE" +
            ") AS keyword ORDER BY keyword_name")
    List<String> getSearchSuggestedKeywords(@Param("keyword")String keyword);

    //Get URL keyword suggestion
    @Select("SELECT DISTINCT keyword_name FROM kd_keyword " +
            "WHERE keyword_name ilike #{keyword} AND status = TRUE " +
            "ORDER BY keyword_name")
    List<String> getUrlSuggestedKeywords(@Param("keyword") String keyword);

    @Update("UPDATE kd_keyword SET status = #{status} WHERE url_id = #{url_id}")
    boolean updateKeywordsStatus(@Param("url_id") int urlId, @Param("status") boolean status);

    //**************************************************************
    //******************** Query URLs ******************************
    //**************************************************************
    @Select(value = "{ CALL search(#{keyword}, #{limit}, #{page}) }")
    @Options(statementType = StatementType.CALLABLE)
    @Results(value = {
            @Result(property = "id", column = "url_id"),
            @Result(property = "urlType", column = "url_type"),
            @Result(property = "picUrl", column = "pic_url"),
            @Result(property = "totalRecord", column = "total_record")
    })
    List<ResultUrlWithTotalRecord> queryUrlsByKeyword(@Param("keyword") String keyword, @Param("limit") int limit, @Param("page") int page);

    @Select("SELECT U.id, U.title, U.url_type, U.link, U.address, U.phone,\n" +
            "  U.email, U.description, U.pic_url, COUNT(*) OVER() AS total_record\n" +
            "FROM kd_url U\n" +
            "JOIN kd_category C ON U.cate_id = C.id\n" +
            "WHERE C.status = TRUE AND U.status = TRUE AND U.is_approved = TRUE AND C.id = #{cate_id}\n" +
            "ORDER BY U.view DESC , U.title\n" +
            "LIMIT #{limit} OFFSET (#{limit} * (#{page} - 1))")
    @Results(value = {
            @Result(property = "urlType", column = "url_type"),
            @Result(property = "picUrl", column = "pic_url"),
            @Result(property = "totalRecord", column = "total_record")
    })
    List<ResultUrlWithTotalRecord> queryUrlsBySubCateId(@Param("cate_id") int cateId, @Param("limit") int limit, @Param("page") int page);

    @Select("SELECT EXISTS (SELECT id FROM kd_url WHERE link = #{link}) AS exist")
    boolean isLinkExist(@Param("link") String link);

    @Select("SELECT COUNT(*) FROM kd_url WHERE status = TRUE AND is_approved = TRUE ")
    int countActiveUrl();

    @Select("SELECT *FROM kd_url \n" +
            "ORDER BY \"view\" DESC\n" +
            "LIMIT 10 OFFSET 0")
    @Results(value = {
            @Result(property = "id", column = "id"),
            @Result(property = "mainCateId", column = "main_cate_id"),
            @Result(property = "subCateId", column = "sub_cate_id"),
            @Result(property = "urlType", column = "url_type"),
            @Result(property = "userId", column = "user_id"),
            @Result(property = "picUrl", column = "pic_url"),
            @Result(property = "isApproved", column = "is_approved")
    })
    List<UrlGetPopular> selectTopTen();
}
