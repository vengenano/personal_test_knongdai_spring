package com.kshrd.knongdai.persistence;


import com.kshrd.knongdai.domain.Url;
import com.kshrd.knongdai.domain.User;
import com.kshrd.knongdai.domain.UserMobileRegister;
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.mapping.StatementType;
import org.springframework.stereotype.Repository;

/**********************************************************************
 *
 * Author: Songden
 * Created Date: 04/January/2019
 * Development Group: PP_G2
 * Description: UserMobileRegisterRepository interface is used as DAO
 *              for manipulating User Register
 *
 **********************************************************************/



@Repository
public interface UserMobileRegisterRepository {

//    @Select(value = "{ CALL insert_user_mobile_register(#{userMobileRegister.userName}, #{userMobileRegister.facebookId}, #{userMobileRegister.phoneNumber},#{userMobileRegister.image},#{userMobileRegister.gender}) }")
//    @Options(statementType = StatementType.CALLABLE)
//    @Select("SELECT *FROM insert_user_mobile_register(#{userMobileRegister.userName},#{userMobileRegister.facebookId},#{userMobileRegister.phoneNumber},#{userMobileRegister.image},#{userMobileRegister.gender})")

        @Insert("insert into kd_user( user_name, facebook_id, phone_number, image, gender,email, password,role_id,user_player_id)\n" +
            "    values (#{userMobileRegister.userName},#{userMobileRegister.facebookId},#{userMobileRegister.phoneNumber},#{userMobileRegister.image},#{userMobileRegister.gender},(uuid_in((md5(((random())::text || (now())::text)))::cstring)),'',0,#{userPlayerId});")
        public boolean insertUserMobileRegister(@Param("userMobileRegister") UserMobileRegister userMobileRegister);

        @Select("SELECT *FROM get_user_mobile_register(#{id})")
         @Results(value = {
                 @Result(property = "userName", column = "r_user_name"),
                 @Result(property = "facebookId", column = "r_facebook_id"),
                 @Result(property = "phoneNumber", column = "r_phone_number"),
                 @Result(property = "image", column = "r_image"),
                 @Result(property = "gender", column = "r_gender"),
                 @Result(property= "userPlayerId", column = "r_user_player_id")})
         UserMobileRegister getUserMobileRegister(@Param("id") int id);

//        @Select("SELECT *FROM update_user_mobile_register(#{userMobileRegister.id},#{userMobileRegister.userName},#{userMobileRegister.gender})")

        @Update("UPDATE kd_user" +
                "    SET user_name=#{userMobileRegister.userName}," +
                "        gender=#{userMobileRegister.gender}" +
                "    WHERE id=#{userMobileRegister.id};")
        public boolean updateUserMobileRegister(@Param("userMobileRegister") UserMobileRegister userMobileRegister);
}
