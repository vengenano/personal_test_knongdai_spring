package com.kshrd.knongdai.persistence;

import com.kshrd.knongdai.domain.form.UserAddForm;
import com.kshrd.knongdai.domain.form.UserUpdateForm;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;
import com.kshrd.knongdai.domain.User;
import java.util.List;

/**********************************************************************
 *
 * Author: Lyhout
 * Created Date: 24/July/2017
 * Development Group: PP_G3
 * Description: UserRepository interface is used as DAO
 *              for manipulating User
 *
 **********************************************************************/

@Repository
public interface UserRepository {

//	Find User By Email
	@Select("SELECT " +
				"U.id, " +
				"user_name, " +
				"role_id, " +
				"R.role_name, " +
				"email, " +
				"password, " +
				"U.status, " +
				"role_id, " +
				"R.status AS role_status " +
			"FROM kd_user U " +
			"JOIN kd_role R ON U.role_id = R.id " +
			"WHERE U.status = TRUE AND email = #{email}")
	@Results(value={
			@Result(property="userName", column="user_name"),
			@Result(property="role.id", column="role_id"),
			@Result(property="role.roleName", column="role_name"),
			@Result(property="role.status", column="role_status")
	})
	public User findUserByEmail(@Param("email") String email);

//	find User by Id
	@Select("SELECT " +
			"U.id, " +
			"user_name, " +
			"role_id, " +
			"R.role_name, " +
			"email, " +
			"password, " +
			"U.status, " +
			"role_id, " +
			"R.status AS role_status " +
			"FROM kd_user U " +
			"JOIN kd_role R ON U.role_id = R.id " +
			"WHERE U.status = TRUE AND U.id = #{id}")
	@Results(value={
			@Result(property="userName", column="user_name"),
			@Result(property="role.id", column="role_id"),
			@Result(property="role.roleName", column="role_name"),
			@Result(property="role.status", column="role_status")
	})
	public User findUserById(@Param("id") int id);

//	Get all active users
	@Select("SELECT " +
			"U.id, " +
			"user_name, " +
			"role_id, " +
			"R.role_name, " +
			"email, " +
			"U.status, " +
			"role_id " +
			"FROM kd_user U " +
			"JOIN kd_role R ON U.role_id = R.id " +
			"WHERE U.status = TRUE")
	@Results(value={
			@Result(property="userName", column="user_name"),
			@Result(property="role.id", column="role_id"),
			@Result(property="role.roleName", column="role_name")
	})
	public List<User> getAllUsers();

//	Update User
	@Update("UPDATE kd_user SET " +
			"user_name = #{user.userName}, " +
			"email = #{user.email}, " +
			"password = #{user.password}, " +
			"role_id = #{user.roleId}, " +
			"status = #{user.status} " +
			"WHERE id = #{user.id}")
	public boolean updateUser(@Param("user") UserUpdateForm user);

//	Delete User (disable)
	@Update("UPDATE kd_user SET status = false " +
			"WHERE id = #{user_id} AND status = TRUE")
	public boolean deleteUser(@Param("user_id") int userId);

//	Insert new User
	@Insert("INSERT INTO kd_user(user_name, email, password, role_id) VALUES (" +
			"#{user.userName}, #{user.email}, #{user.firstPassword}, #{user.roleId});")
	@Options(
		useGeneratedKeys = true,
		keyProperty = "user.id", keyColumn = "id")
	public boolean createUser(@Param("user") UserAddForm user);
}
