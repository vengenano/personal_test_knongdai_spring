package com.kshrd.knongdai.persistence.sql;

import org.apache.ibatis.annotations.Param;

/**********************************************************************
 *
 * Author: Theara
 * Created Date: 8/Aug/2017
 * Development Group: PP_G3
 * Description: KnongdaiSQLBuilder class is used to create dynamic sql query
 *
 **********************************************************************/

public class KnongdaiSQLBuilder {

    public static String getUrlsByPage(@Param("num_row") int numRow,
                                       @Param("page") int page,
                                       @Param("title") String title,
                                       @Param("main_cate_id") Integer mainCateId,
                                       @Param("sub_cate_id") Integer subCateId,
                                       @Param("approved") Boolean approved
    ){

        StringBuilder sql = new StringBuilder();

        sql.append(
                "SELECT U.id, U.title, U.urL_type, U.link, U.address, U.phone, U.email, " +
                        "U.description, U.pic_url, M.cate_name AS main_cate, " +
                        "S.cate_name AS sub_cate, U.is_approved, COUNT(*) OVER() AS total_record " +
                "FROM kd_url U\n" +
                "  JOIN kd_category S ON U.cate_id = S.id\n" +
                "  JOIN kd_category M ON S.main_cate_id = M.id\n" +
                "WHERE S.main_cate_id IS NOT NULL AND U.status = TRUE" +
                " AND M.status = TRUE AND S.status = TRUE\n"
        );

        if(title != null){
            sql.append(" AND (U.title ILIKE #{title} OR U.link ILIKE #{title}) ");
        }
        if(mainCateId != null){
            sql.append(" AND M.id = #{main_cate_id} ");
        }
        if(subCateId != null){
            sql.append(" AND U.cate_id = #{sub_cate_id} ");
        }
        if(approved != null){
            sql.append(" AND U.is_approved = " + (approved ? "TRUE " : "FALSE "));
        }
        if(title == null){
            sql.append("ORDER BY U.id DESC \n");
        }else{
            sql.append("ORDER BY U.title\n");
        }
        sql.append("LIMIT #{num_row} OFFSET (#{num_row} * (#{page} - 1))");

        return sql.toString();

    }

}
