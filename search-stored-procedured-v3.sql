CREATE OR REPLACE FUNCTION search (keyword VARCHAR(40), num_row INT, page INT)
  RETURNS TABLE(
    url_id INT,
    title VARCHAR(255),
    url_type CHAR,
    link VARCHAR(100),
    address VARCHAR(255),
    phone VARCHAR(100),
    email VARCHAR(100),
    description text,
    pic_url VARCHAR(255),
    total_record BIGINT
  )
AS $$

BEGIN

RETURN QUERY(
WITH table1 AS

  (SELECT U.id, U.title, U.url_type, U.link, U.address, U.phone, U.email, U.description, U.pic_url
    FROM kd_url U
    WHERE U.title ILIKE keyword
    ORDER BY U.title, U.view),

  table2 AS
    (
      SELECT * FROM table1
        UNION ALL
      (SELECT U.id, U.title, U.url_type, U.link, U.address, U.phone, U.email, U.description, U.pic_url
        FROM kd_url U
          JOIN kd_keyword K ON U.id = K.url_id
        WHERE K.keyword_name ILIKE keyword AND U.id NOT IN (SELECT id FROM table1)
        ORDER BY K.keyword_name, U.view)
    ),

  table3 AS
    (
      SELECT * FROM table2
        UNION ALL
      (SELECT U.id, U.title, U.url_type, U.link, U.address, U.phone, U.email, U.description, U.pic_url
        FROM kd_cate_keyword K
          JOIN kd_category C ON K.cate_id = C.id
          JOIN kd_url U ON C.id = U.cate_id
        WHERE keyword_name ILIKE keyword AND U.id NOT IN (SELECT id FROM table2)
        ORDER BY K.keyword_name, U.view DESC, U.title)
    ),

  table4 AS
    (
      SELECT * FROM table3
        UNION ALL
      (SELECT U.id, U.title, U.url_type, U.link, U.address, U.phone, U.email, U.description, U.pic_url
        FROM kd_cate_keyword K
          JOIN kd_category MC ON K.cate_id = MC.id
          JOIN kd_category SC ON MC.id = SC.main_cate_id
          JOIN kd_url U ON SC.id = U.cate_id
        WHERE keyword_name ILIKE keyword AND U.id NOT IN (SELECT id FROM table3)
        ORDER BY K.keyword_name, U.view, U.title)
    )
SELECT R.id, R.title, R.url_type, R.link, R.address, R.phone, R.email, R.description, R.pic_url, COUNT(*) OVER() AS total_record
FROM table4 R LIMIT num_row OFFSET (num_row * (page - 1)) );

END;
$$ LANGUAGE plpgsql;

-- SELECT * FROM search('%new%', 2, 1)