CREATE OR REPLACE function search(keyword character varying, num_row integer, page integer) returns TABLE(url_id integer, title character varying, url_type character, link character varying, address character varying, phone character varying, email character varying, description text, pic_url character varying, total_record bigint)
LANGUAGE plpgsql
AS $$
BEGIN

RETURN QUERY(
WITH table1 AS

  (SELECT DISTINCT * FROM
    (SELECT U.id, U.title, U.url_type, U.link, U.address, U.phone, U.email, U.description, U.pic_url
      FROM kd_url U
      WHERE U.title ILIKE keyword AND U.status = TRUE AND is_approved = TRUE
      ORDER BY U.title, U.view) AS tbl1),

  table2 AS
    (
      SELECT * FROM table1
        UNION ALL
      (SELECT DISTINCT * FROM
        (SELECT U.id, U.title, U.url_type, U.link, U.address, U.phone, U.email, U.description, U.pic_url
          FROM kd_url U
            JOIN kd_keyword K ON U.id = K.url_id
          WHERE K.keyword_name ILIKE keyword AND U.id NOT IN (SELECT id FROM table1) AND U.status = TRUE AND is_approved = TRUE
          ORDER BY K.keyword_name, U.view) AS tbl2
      )
    ),

  table3 AS
    (
      SELECT * FROM table2
        UNION ALL
      (SELECT DISTINCT * FROM
        (SELECT U.id, U.title, U.url_type, U.link, U.address, U.phone, U.email, U.description, U.pic_url
          FROM kd_cate_keyword K
            JOIN kd_category C ON K.cate_id = C.id
            JOIN kd_url U ON C.id = U.cate_id
          WHERE keyword_name ILIKE keyword AND U.id NOT IN (SELECT id FROM table2) AND U.status = TRUE AND is_approved = TRUE
          ORDER BY K.keyword_name, U.view DESC, U.title) AS tbl3
      )
    ),

  table4 AS
    (
      SELECT * FROM table3
        UNION ALL
      (SELECT DISTINCT * FROM
        (SELECT U.id, U.title, U.url_type, U.link, U.address, U.phone, U.email, U.description, U.pic_url
          FROM kd_cate_keyword K
            JOIN kd_category MC ON K.cate_id = MC.id
            JOIN kd_category SC ON MC.id = SC.main_cate_id
            JOIN kd_url U ON SC.id = U.cate_id
          WHERE keyword_name ILIKE keyword AND U.id NOT IN (SELECT id FROM table3) AND U.status = TRUE AND is_approved = TRUE
          ORDER BY K.keyword_name, U.view, U.title) AS tbl4
      )
    )
SELECT R.id, R.title, R.url_type, R.link, R.address, R.phone, R.email, R.description, R.pic_url, COUNT(*) OVER() AS total_record
FROM table4 R LIMIT num_row OFFSET (num_row * (page - 1)) );

END;
$$;
