package com.kshrd.knongdai.configuration;

import org.springframework.stereotype.Component;

/**
 * Created by Theara on 16-Aug-17.
 */

@Component
public class ApplicationResource {

    public String getUploadPath(){
        return "/opt/Data/Testing/KD_SEARCH/";
    }
    public String getResourcePath(){
        return "/resources/upload/";
    }

}
