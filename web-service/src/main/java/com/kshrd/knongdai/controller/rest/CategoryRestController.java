package com.kshrd.knongdai.controller.rest;

import com.kshrd.knongdai.domain.BaseCategory;
import com.kshrd.knongdai.domain.Category;
import com.kshrd.knongdai.domain.MainCategory;
import com.kshrd.knongdai.domain.form.CategoryAddForm;
import com.kshrd.knongdai.domain.form.SubCategoryAddForm;
import com.kshrd.knongdai.response.Response;
import com.kshrd.knongdai.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**********************************************************************
 *
 * Author: Theara
 * Created Date: 25/July/2017
 * Development Group: PP_G3
 * Description: CategoryRestController is controller for handling RESTFul API
 *              for manipulating Category
 *
 **********************************************************************/

@RestController
@RequestMapping("/api/v1/categories")
public class CategoryRestController extends KnongDaiRestController {

    private CategoryService categoryService;

    @Autowired
    public CategoryRestController(CategoryService categoryService){
        this.categoryService = categoryService;
    }

    @GetMapping
    public ResponseEntity<Response> getAllMainCategories(){
        try {
            List<MainCategory> categories = categoryService.getAllMainCategories();
//            List totalUrl=new ArrayList();
//            List all=new ArrayList();
//
//            for (MainCategory mainCategory:categories) {
//                totalUrl.add(categoryService.getTotalCountUrlByMainId(mainCategory.getId()));
//            }
//
//            all.add(categories);
////            all.add(totalUrl);

            if(categories != null){
                return successResponse(categories, "success");
            }
            return failResponse("failed to get categories");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Response> getCategoryById(@PathVariable("id") int id){
        try {
            Category category = categoryService.getCategoryById(id);
            if(category != null){
                return successResponse(category, "success");
            }
            return failResponse("failed to get the category");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @GetMapping("/sub")
    public ResponseEntity<Response> getAllSubCategories(){
        try {
            List<BaseCategory> categories = categoryService.getAllSubCategories();
            if(categories != null){
                return successResponse(categories, "successfully getting all sub categories");
            }
            return failResponse("failed to load all sub categories");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @PostMapping(value = "/create-sub", headers = "Accept=application/json")
    @Transactional
    public ResponseEntity<Response> createSubCategory(@RequestBody SubCategoryAddForm category){
        try {
            if(categoryService.insertSubCategory(category)){
                return successResponse(categoryService.getCategoryById(category.getId()), "successfully create sub category");
            }
            return failResponse("failed to created Sub Category");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @PostMapping(value = "/create-main", headers = "Accept=application/json")
    @Transactional
    public ResponseEntity<Response> createMainCategory(@RequestBody CategoryAddForm category){
        try {
            if(categoryService.insertMainCategory(category)){
                return successResponse(categoryService.getCategoryById(category.getId()), "successfully create main category");
            }
            return failResponse("failed to created main Category");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @PutMapping(value = "/update-sub", headers = "Accept=application/json")
    @Transactional
    public ResponseEntity<Response> updateSubCategory(@RequestBody SubCategoryAddForm category){
        try {
            if(categoryService.updateSubCategory(category)){
                return successResponse(categoryService.getCategoryById(category.getId()), "successfully update Sub Category");
            }
            return failResponse("failed to update Sub Category");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @PutMapping(value = "/update-main", headers = "Accept=application/json")
    @Transactional
    public ResponseEntity<Response> updateMainCategory(@RequestBody CategoryAddForm category){
        System.out.println(category.getId());
        System.out.println(category.getCategoryName());
        System.out.println(category.getIconName());
        System.out.println(category.getDescription());
        System.out.println(category.getStatus());
        try {
            if(categoryService.updateMainCategory(category)){
                return successResponse(categoryService.getCategoryById(category.getId()), "successfully update Main Category");
            }
            return failResponse("failed to update Main Category");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @DeleteMapping("{id}/delete")
    @Transactional
    ResponseEntity<Response> deleteRole(@PathVariable("id") int id){
        try {
            if(categoryService.deleteCategoryById(id)){
                return successResponse("successfully delete the Category");
            }
            return failResponse("failed to delete the Category");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @GetMapping("sub-by-main-id/{id}")
    public ResponseEntity<Response> getSubCategoriesByMainCateId(@PathVariable("id") int mainId){
        try {
            return successResponse(categoryService.getSubCategoriesByMainCateId(mainId), "successful");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @GetMapping("total-url-by-main-id/{id}")
    public ResponseEntity<Response> getTotalCountUrlByMainId(@PathVariable("id") int id){
        try {
            return successResponse(categoryService.getTotalCountUrlByMainId(id), "successful");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

}
