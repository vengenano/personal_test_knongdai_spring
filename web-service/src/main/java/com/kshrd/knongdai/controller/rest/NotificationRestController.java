package com.kshrd.knongdai.controller.rest;

import com.kshrd.knongdai.domain.Notification;
import com.kshrd.knongdai.response.Response;
import com.kshrd.knongdai.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/notification")
public class NotificationRestController extends KnongDaiRestController {
    private NotificationService notificationService;

    @Autowired
    public NotificationRestController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @GetMapping("{userId}")
    public ResponseEntity<Response> getNotification(@PathVariable("userId") int userId){
        try {
            Notification notification = notificationService.getNotification(userId);
            if(notification != null){
                return successResponse(notification, "get URL successfully");
            }
            return failResponse("failed to get URL");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }
}
