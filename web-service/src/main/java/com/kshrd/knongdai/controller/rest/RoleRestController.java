package com.kshrd.knongdai.controller.rest;

import com.kshrd.knongdai.domain.Role;
import com.kshrd.knongdai.response.Response;
import com.kshrd.knongdai.service.RoleService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**********************************************************************
 *
 * Author: Chanpheng
 * Created Date: 22/July/2017
 * Development Group: PP_G3
 * Description: RoleRestController is controller for handling RESTFul API
 *              for manipulating User Role
 *
 **********************************************************************/

@RestController
@RequestMapping("/api/v1/roles")
public class RoleRestController extends KnongDaiRestController<Role> {

    private RoleService roleService;

    public RoleRestController(RoleService roleService) {
        this.roleService = roleService;
    }

    @GetMapping(headers = "Accept=application/json")
    public ResponseEntity<Response> getAllRoles(){
        List<Role> roles = roleService.getAllRoles();
        if(roles == null){
            return failResponse("fail");
        }
        return successResponse(roles, "success");
    }

    @PutMapping(value = "/update", headers = "Accept=application/json")
    public ResponseEntity<Response> updateRole(@RequestBody Role role){
        try {
            if(roleService.updateRole(role)){
                return successResponse("success");
            }
            return failResponse("fail");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<Response> deleteRole(@PathVariable("id") int id){
        try {
            if(roleService.deleteRole(id)){
                return successResponse("success");
            }
            return failResponse("fail");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Response> deleteRole(@RequestParam("role_name") String roleName){
        System.out.println(roleName);
        Role role = new Role();
        role.setRoleName(roleName);
        try {
            if(roleService.insertRole(role)){
                return successResponse("success");
            }
            return failResponse("fail");
        }catch (Exception e){
            e.printStackTrace();
            return failResponse("fail to insert new role");
        }
    }

}