package com.kshrd.knongdai.controller.rest;

import com.kshrd.knongdai.domain.SearchHistory;
import com.kshrd.knongdai.response.Response;
import com.kshrd.knongdai.service.SearchHistoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/search-history")
public class SearchHistoryRestController extends KnongDaiRestController<SearchHistory> {
    private SearchHistoryService searchHistoryService;
    @Autowired
    public SearchHistoryRestController(SearchHistoryService searchHistoryService) {
        this.searchHistoryService = searchHistoryService;
    }

    @PostMapping(value = "/create", headers = "Accept=application/json")
    public ResponseEntity<Response> insertSearchHistory(@RequestBody SearchHistory searchHistory){
        try {
            if(searchHistoryService.insertSearchHistory(searchHistory)){
                return successResponse("success");
            }
            return failResponse("fail");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }
    @DeleteMapping(value = "/deleteKeywordByUpdateStatus", headers = "Accept=application/json")
    public  ResponseEntity<Response> deletedSearchHistoryStatus(@RequestBody SearchHistory searchHistory){
        try {
            if(searchHistoryService.deletedSearchHistoryStatus(searchHistory)){
                return successResponse("success");
            }
            return failResponse("fail");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }
    @GetMapping(value = "/{userId}", headers = "Accept=application/json")
    public  ResponseEntity<Response> getKeywordByUserId(@PathVariable("userId") int userId){
        try {
            List<SearchHistory> searchHistory = searchHistoryService.getKeywordByUserId(userId);
            if(searchHistory != null){
                return successResponse(searchHistory, "success");
            }
            return failResponse("failed to get the category");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

}
