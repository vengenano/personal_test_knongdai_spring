package com.kshrd.knongdai.controller.rest;

import com.kshrd.knongdai.domain.ResultUrl;
import com.kshrd.knongdai.domain.ResultUrlWithTotalRecord;
import com.kshrd.knongdai.response.Pagination;
import com.kshrd.knongdai.response.PaginationResponseList;
import com.kshrd.knongdai.service.CategoryService;
import com.kshrd.knongdai.service.UrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriUtils;

import java.net.URLEncoder;
import java.util.List;

/**********************************************************************
 *
 * Author: Theara
 * Created Date: 24/July/2017
 * Development Group: PP_G3
 * Description: SearchRestController is controller for handling RESTFul API
 *              for query URL result
 *
 **********************************************************************/

//@RestController
@Controller
public class SearchRestController extends KnongDaiRestController {

    private CategoryService categoryService;
    private UrlService urlService;

    @Autowired
    public SearchRestController(UrlService urlService,CategoryService categoryService){
        this.urlService = urlService;
        this.categoryService=categoryService;
    }

    @ResponseBody
    @GetMapping("/api/v1/search/keyword/{keyword}")
    public List<String> getSuggestKeyword(@PathVariable("keyword") String keyword){
        return urlService.getSearchSuggestedKeywords(keyword);
    }

    @GetMapping("/search")
    public String searchPage(
            @RequestParam(value = "q", required = false, defaultValue = "") String keyword,
            Model model){
        model.addAttribute("MAIN_CATES", categoryService.getAllMainCategories());
        model.addAttribute("KW_NAME",keyword);
         return "/front/resultpage";
    }

    @ResponseBody
    @GetMapping("/api/v1/search")
    public ResponseEntity<PaginationResponseList<ResultUrl>> queryUrlsByKeyword(
            @RequestParam(value = "q", required = false) String keyword,
            @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
            @RequestParam(value = "cate_id", required = false) Integer cateId
    ){
        try {
            if(cateId == null && keyword == null){
                return failResponse("'q' parameter is required");
            }
            int limit = 20;

            List<ResultUrlWithTotalRecord> urls =
                    (cateId == null) ? urlService.queryUrlsByKeyword(keyword, limit, page)
                            : urlService.queryUrlsBySubCateId(cateId, limit, page);

            if(urls != null){
                Pagination pagination = new Pagination();
                pagination.setTotalRecord( (urls.size() == 0) ? 0 : urls.get(0).getTotalRecord() );
                pagination.setCurrentPage(page);
                pagination.setLimit(limit);
                PaginationResponseList paginationResponseList = new PaginationResponseList("200", true, "success retreive URls", urls);
                paginationResponseList.setPagination(pagination);
                return new ResponseEntity<PaginationResponseList<ResultUrl>>(paginationResponseList, HttpStatus.OK);
            }else {
                return failResponse("failed to query result");
            }
        }catch(Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @GetMapping("/redirect/{id}")
    public ModelAndView toString(@PathVariable("id") int id, ModelAndView model) {
        try {
            if(urlService.incrementUrlView(id)){
                return new ModelAndView("redirect:"
                    + UriUtils.encodePath(urlService.getUrlById(id).getLink(), "UTF-8") );
            }else{
                model.setViewName("/errors/404");
                return model;
            }
        }catch (Exception e){
            e.printStackTrace();
            model.setViewName("/errors/500");
            return model;
        }
    }
}
