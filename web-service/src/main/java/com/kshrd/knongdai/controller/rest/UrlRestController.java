package com.kshrd.knongdai.controller.rest;

import com.kshrd.knongdai.domain.*;
import com.kshrd.knongdai.response.Pagination;
import com.kshrd.knongdai.response.PaginationResponseList;
import com.kshrd.knongdai.response.Response;
import com.kshrd.knongdai.service.NotificationService;
import com.kshrd.knongdai.service.UrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import com.kshrd.knongdai.configuration.ApplicationResource;

import java.io.OutputStream;
import java.net.URL;
import java.net.HttpURLConnection;

import java.util.List;
import java.util.Scanner;

/**********************************************************************
 *
 * Author: Sokheang
 * Created Date: 25/July/2017
 * Development Group: PP_G3
 * Description: UrlRestController is controller for handling RESTFul API
 *              for manipulating Url
 *
 **********************************************************************/

@RestController
@RequestMapping("/api/v1/urls")
public class UrlRestController extends KnongDaiRestController {

    private UrlService urlService;
    private FileUploader fileUploader;
    private ApplicationResource applicationResource;
    private NotificationService notificationService;

    //----------- for insert to table notification ------------//
    String title="ចំណងជើងគេហទំព័រៈ ";
    String link="គេហទំព័រៈ ";
    String message="ការស្នើរសុំគេហទំព័ររបស់លោកអ្នកត្រូវបានដាក់អោយប្រើប្រាស់";

    //------------------------------------------------------//

    @Autowired
    public UrlRestController(UrlService urlService, FileUploader fileUploader, ApplicationResource applicationResource,NotificationService notificationService){
        this.urlService = urlService;
        this.fileUploader = fileUploader;
        this.applicationResource = applicationResource;
        this.notificationService=notificationService;
    }

    @PostMapping(value = "/create", headers = "Accept=application/json")
    @Transactional
    public ResponseEntity<Response> createUrl(@RequestBody Url url){
        try {
            if(urlService.insertUrl(url)){

                // Check if status is true then call sendNotification()
                // temchannat@gmail.com
//                if (urlService.getURLlStatus(url.getId()))


//                }



                return successResponse(urlService.getUrlById(url.getId()), "URL has been created successfully");
            }
            return failResponse("failed to create URL");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

//    String userPlayerId

//    public List<Notification> getUserForNotification(){
//        return this.notificationService.getUserForNotification();
//    }


    // notification
    public boolean sendNotification(String userPlayerId,String urlTitle,String urlLink,String message) {
//        boolean b = false;
        // OneSignalCode Here

            try {
                String jsonResponse;

                URL url = new URL("https://onesignal.com/api/v1/notifications");
                HttpURLConnection con = (HttpURLConnection)url.openConnection();
                con.setUseCaches(false);
                con.setDoOutput(true);
                con.setDoInput(true);

                con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                con.setRequestMethod("POST");

                String strJsonBody = "{"
                        +   "\"app_id\": \"b545bdf6-3322-4452-8af2-c60dbb3006a1\","
                        +   "\"include_player_ids\": [\""+userPlayerId+"\"],"
                        +   "\"data\": {\"foo\": \"bar\"},"
                        +   "\"contents\": {\"en\": \""+title+ ""+urlTitle+"\n"+link+ ""+urlLink+"\n"+message+"\"}"
                        + "}";


                System.out.println("strJsonBody:\n" + strJsonBody);

                byte[] sendBytes = strJsonBody.getBytes("UTF-8");
                con.setFixedLengthStreamingMode(sendBytes.length);

                OutputStream outputStream = con.getOutputStream();
                outputStream.write(sendBytes);

                int httpResponse = con.getResponseCode();
                System.out.println("httpResponse: " + httpResponse);

                if (  httpResponse >= HttpURLConnection.HTTP_OK
                        && httpResponse < HttpURLConnection.HTTP_BAD_REQUEST) {
                    Scanner scanner = new Scanner(con.getInputStream(), "UTF-8");
                    jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
                    scanner.close();
                }
                else {
                    Scanner scanner = new Scanner(con.getErrorStream(), "UTF-8");
                    jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
                    scanner.close();
                }
                System.out.println("jsonResponse:\n" + jsonResponse);

            } catch(Throwable t) {
                t.printStackTrace();
            }
        return true;
    }

    @GetMapping("{id}")
    public ResponseEntity<Response> getUrlById(@PathVariable("id") int id){
        try {
            Url url = urlService.getUrlById(id);
            if(url != null){
                return successResponse(url, "get URL successfully");
            }
            return failResponse("failed to get URL");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @GetMapping()
    public ResponseEntity<PaginationResponseList<ListUrl>> getUrlsByPage(
            @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
            @RequestParam(value = "title", required = false) String title,
            @RequestParam(value = "main_cate_id", required = false) Integer mainCateId,
            @RequestParam(value = "sub_cate_id", required = false) Integer subCateId,
            @RequestParam(value = "approved", required = false) Boolean approved
    ){
        try {
            int numRow = 30;

            List<ListUrl> urls = urlService.getUrlsByPage(numRow, page, title, mainCateId, subCateId, approved);

            Pagination pagination = new Pagination();
            pagination.setTotalRecord( (urls.size() == 0) ? 0 : urls.get(0).getTotalRecord() );
            pagination.setCurrentPage(page);
            pagination.setLimit(numRow);

            PaginationResponseList paginationResponseList = new PaginationResponseList("400", true, "success retreive URls", urls);

            paginationResponseList.setPagination(pagination);

            return new ResponseEntity<PaginationResponseList<ListUrl>>(paginationResponseList, HttpStatus.OK);
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @PutMapping(value = "/update", headers = "Accept=application/json")
    @Transactional
    public ResponseEntity<Response> updateSubCategory(@RequestBody Url url){
        try {
            if(urlService.updateUrl(url)){
                return successResponse(urlService.getUrlById(url.getId()), "successfully update URL");
            }
            return failResponse("failed to update URL");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @DeleteMapping("{id}/delete")
    public ResponseEntity<Response> deleteUrlById(@PathVariable("id") int id){
        try {
            if(urlService.deleteUrlById(id)){
                return successResponse("sucessfully delete the URL");
            }
            return failResponse("failed to delete the URL");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @PutMapping("{id}/approved/{approved}")
    public ResponseEntity<Response> updateApproved(@PathVariable("id") int id, @PathVariable("approved") boolean approved){
        try {
            if(urlService.updateApproved(id, approved)){
                if(approved){
                    try {
                        Notification notifications = this.notificationService.getUserForNotification(id);
                        if(notifications.getUserPlayerId().equals("")){
                            return null;
                        }else
                        {
                            sendNotification(notifications.getUserPlayerId(),notifications.getTitle(),notifications.getLink(),message);

                                this.notificationService.insertNotification(notifications.getRequestDate(), notifications.getUrlId(), notifications.getUserId(), message);

                        }
                    }catch (Exception ex){
                        System.out.println(ex.toString());
                    }

                }
                return successResponse(((approved) ? "approved" : "disapproved"));
            }
            return failResponse("failed to update the URL's approval");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @GetMapping("keyword-suggestion/{keyword}")
    public List<String> getUrlSuggestedKeywords(@PathVariable("keyword") String keyword){
        return urlService.getUrlSuggestedKeywords(keyword);
    }

    @PostMapping("/upload/web-icon")
    public ResponseEntity<Response> uploadImage(@RequestParam("file") MultipartFile file){
        String uploadPath = applicationResource.getUploadPath();
        String resourcePath = applicationResource.getResourcePath();

        try {

            fileUploader.setResourcePath(uploadPath);
            fileUploader.setResourcesHandler(resourcePath);
            fileUploader.setMultipartFile(file);

            if(fileUploader.upload(null)){
                return successResponse(fileUploader.getAbsolutePath(), "successfully uploaded image");
            }

            return failResponse("failed to upload image");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @GetMapping("/popular")
    public List<UrlGetPopular> popularUrl(){
        try{
            return urlService.selectTopTen();
        }catch (Exception ex){
            System.out.println(ex.toString());
            return null;
        }
    }
}
