package com.kshrd.knongdai.controller.rest;

import com.kshrd.knongdai.domain.Url;
import com.kshrd.knongdai.response.Response;
import com.kshrd.knongdai.scrape.*;
import com.kshrd.knongdai.service.ScrapeService;
import com.kshrd.knongdai.service.UrlService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**********************************************************************
 *
 * Author: Phen
 * Created Date: 7/Aug/2017
 * Development Group: PP_G3
 * Description: UrlScrapingRestController is controller for handling RESTFul API
 *              for manipulating Scraping Data
 *
 **********************************************************************/

@RestController
@RequestMapping("/api/v1/scrape")
public class
UrlScrapingRestController extends KnongDaiRestController {

    private UrlService urlService;

    @Autowired
    public UrlScrapingRestController(UrlService urlService){
        this.urlService = urlService;
    }

    @GetMapping(headers = "Accept=application/json")
    public ResponseEntity<Response> scrape(@RequestParam("site") String site, @RequestParam("keyword") String keyword){

        //Config URL pages
        String searchKeyword = keyword;
        String mainUrl = site;
//        String mainUrl = "http://atozcambodia.com/";
        System.out.println("site is : " + mainUrl);
        System.out.println("keyword is : " + searchKeyword);

        int start_page = 1;
        int until_page = 1;

        //Request All Data from Database
        List<Url> getUrlFromDatabase = null ;



        List<Url> getAllPageUrl = new ArrayList<>();
        List<Url> filterUrl = new ArrayList<>();
//        Set<Url> afterFindDuplicateLink = new HashSet<>();
//        Set<String> test = new HashSet<>();


        try{
            if(mainUrl.equals("http://atozcambodia.com/")){
                System.out.println("Now scrape "+ mainUrl);
                for(int i = start_page ; i<= until_page; i++) {
                    System.out.println("start scrape page : "+i);
//                    getAllPageUrl.addAll(ScrapeAtoZpage.extractData(mainUrl,"http://atozcambodia.com/maintence.php?cat=9&id="+i));
                    getAllPageUrl.addAll(ScrapeAtoZpage.extractData(mainUrl,"http://atozcambodia.com/searchres.php?local=&&key="+searchKeyword));
                }
            }else if(mainUrl.equals("http://yp.com.kh/")){
                searchKeyword = ScrapingFunction.convertSpaceToSlush(keyword);
                System.out.println("Now scrape "+ mainUrl);
                for(int i = start_page ; i<= until_page; i++) {
                    System.out.println("start scrape page : "+i);
//                    getAllPageUrl.addAll(ScrapeYellowPage.extractData(mainUrl,"https://www.yp.com.kh/search_results?page="+i+"&q=shop&location_value="));
                    getAllPageUrl.addAll(ScrapeYellowPage.extractData(mainUrl,"https://www.yp.com.kh/search_results?q="+searchKeyword+"&location_value="));
                }
            }else if(mainUrl.equals("https://www.cccdirectory.org")){
                System.out.println("Now scrape "+ mainUrl);
                searchKeyword = ScrapingFunction.convertSpaceToSlush(keyword);
                for(int i = start_page ; i<= until_page; i++) {
                    System.out.println("start scrape page : "+i);
                    getAllPageUrl.addAll(ScrapeCccdirectory.extractData(mainUrl,"https://www.cccdirectory.org/search?q="+searchKeyword));
//                    getAllPageUrl.addAll(ScrapeCccdirectory.extractData(mainUrl,"https://www.cccdirectory.org/categories/id28/listings&page="+i));
//                   getAllPageUrl.addAll(ScrapeGoogleDemo.extractDataFromGoogle(mainUrl,"https://www.google.com.kh/search?num=100&q="+searchKeyword));
//
// getAllPageUrl.addAll(ScrapeCccdirectory.extractData(mainUrl,"https://www.cccdirectory.org/categories/id186/listings"));
                }
            }else if(mainUrl.equals("https://cambodia-pages.com")){
                System.out.println("Now scrape "+ mainUrl);
                searchKeyword = ScrapingFunction.convertSpaceToSlush(keyword);
                for(int i = start_page; i<= until_page; i++){
                    System.out.println("start scrape page : "+i);
//                    getAllPageUrl.addAll(ScrapeCambodiaPage.extractData(mainUrl,"https://cambodia-pages.com/categories/khmer184-association/listings?page="+i));
                    getAllPageUrl.addAll(ScrapeCambodiaPage.extractData(mainUrl,"https://cambodia-pages.com/search?q="+searchKeyword));
                }
            }else if(mainUrl.equals("https://www.google.com.kh")) {
                searchKeyword = ScrapingFunction.convertSpaceToSlush(keyword);
                System.out.println("Now scrape " + mainUrl);
                for (int i = start_page; i <= until_page; i++) {
                    System.out.println("start scrape page : " + i);
                    getAllPageUrl.addAll(ScrapeGoogleDemo.extractDataFromGoogle(mainUrl, "https://www.google.com.kh/search?num=100&source=hp&q=" + searchKeyword));
                }
            }else{
                System.out.println("No Url to scan");
            }

            //TEST SHOW ALL RECORD FROM WEB PAGES
            System.out.println("TEST SHOW ALL RECORD FROM WEB PAGES");
            for(Url show: getAllPageUrl ){
                System.out.println(show.getLink());
            }
        }catch (Exception e){
            System.err.println("Adding data was error.");
        }

        //find doublicate in list from webpage by it self
        List<Url> afterFindDuplicateLink = ScrapingFunction.deleteDuplicationUrl(getAllPageUrl);

        if(!afterFindDuplicateLink.isEmpty()){
            System.out.println("Start fetch data from Database...");
            getUrlFromDatabase = this.urlService.getAllUrls();
            System.out.println("fetch data finish...");
            // Validation Url  From Web site and Url in our Database
            filterUrl = ScrapingFunction.validateWithDatabase(afterFindDuplicateLink,getUrlFromDatabase);
        }
        
        System.out.println("Scrape " + mainUrl + " has been finished.");

        try {
            return successResponse(filterUrl, "retrieved scraping URLs");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

}
