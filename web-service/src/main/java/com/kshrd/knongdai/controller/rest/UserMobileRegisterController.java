package com.kshrd.knongdai.controller.rest;

import com.kshrd.knongdai.configuration.ApplicationResource;
import com.kshrd.knongdai.domain.FileUploader;
import com.kshrd.knongdai.domain.Url;
import com.kshrd.knongdai.domain.UserMobileRegister;
import com.kshrd.knongdai.domain.form.UserAddForm;
import com.kshrd.knongdai.response.Response;
import com.kshrd.knongdai.service.UserMobileRegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/v1/users/register")
public class UserMobileRegisterController extends KnongDaiRestController<UserMobileRegister> {
    private UserMobileRegisterService userMobileRegisterService;
//    private FileUploader fileUploader;
//    private ApplicationResource applicationResource;

//    public UserMobileRegisterController(UserMobileRegisterService userMobileRegisterService, FileUploader fileUploader, ApplicationResource applicationResource) {
//        this.userMobileRegisterService = userMobileRegisterService;
//        this.fileUploader = fileUploader;
//        this.applicationResource = applicationResource;
//    }

    @Autowired
    public UserMobileRegisterController(UserMobileRegisterService userMobileRegisterService) {
        this.userMobileRegisterService = userMobileRegisterService;
    }




    @PostMapping(value = "/create", headers = "Accept=application/json")
    public ResponseEntity<Response> createUserMobileRegister(@RequestBody UserMobileRegister userMobileRegister){
        System.out.println("====>"+userMobileRegister);
        try{
            if(userMobileRegisterService.insertUserMobileRegister(userMobileRegister)){
                return successResponse("success");
            }else {
                return failResponse("fail");
            }
        }catch (Exception ex){
            ex.printStackTrace();
            return failResponse("fail to insert user register");
        }
    }

//    @PostMapping("/upload/web-icon")
//    public ResponseEntity<Response> uploadImage(@RequestParam("file") MultipartFile file){
//        String uploadPath = applicationResource.getUploadPath();
//        String resourcePath = applicationResource.getResourcePath();
//
//        try {
//
//            fileUploader.setResourcePath(uploadPath);
//            fileUploader.setResourcesHandler(resourcePath);
//            fileUploader.setMultipartFile(file);
//
//            if(fileUploader.upload(null)){
//                return successResponse(fileUploader.getAbsolutePath(), "successfully uploaded image");
//            }
//
//            return failResponse("failed to upload image");
//        }catch (Exception e){
//            e.printStackTrace();
//            return internalErrorResponse();
//        }
//    }

    @GetMapping("{id}")
    public ResponseEntity<Response> getUrlById(@PathVariable("id") int id){
        try {
            UserMobileRegister userMobileRegister = userMobileRegisterService.getUserMobileRegister(id);
            if(userMobileRegister != null){
                return successResponse(userMobileRegister, "get user mobile register successfully");
            }
            return failResponse("failed to get URL");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @PutMapping(value = "/update", headers = "Accept=application/json")
    public ResponseEntity<Response> updateUserMobileRegister(@RequestBody UserMobileRegister userMobileRegister){
        System.out.println("====>"+userMobileRegister);
        try{
            if((userMobileRegisterService.updateUserMobileRegister(userMobileRegister))==true) {
                return successResponse("success");
            }
                return failResponse("fail");

        }catch (Exception ex){
            ex.printStackTrace();
            return failResponse("fail to insert user register");
        }
    }
}
