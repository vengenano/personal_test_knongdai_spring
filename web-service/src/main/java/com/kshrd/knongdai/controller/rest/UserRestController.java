package com.kshrd.knongdai.controller.rest;

import com.kshrd.knongdai.domain.User;
import com.kshrd.knongdai.domain.form.UserAddForm;
import com.kshrd.knongdai.domain.form.UserUpdateForm;
import com.kshrd.knongdai.response.Response;
import com.kshrd.knongdai.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**********************************************************************
 *
 * Author: Lyhout
 * Created Date: 22/July/2017
 * Development Group: PP_G3
 * Description: UserRestController is controller for handling RESTFul API
 *              for manipulating User
 *
 **********************************************************************/

@RestController
@RequestMapping("/api/v1/users")
public class UserRestController extends KnongDaiRestController<User> {

    private UserService userService;

    @Autowired
    public UserRestController(UserService userService){
        this.userService = userService;
    }

    @GetMapping("/find-by-email")
    public ResponseEntity<Response> findUserByEmail(@RequestParam("email") String email){
        User user = userService.findUserByEmail(email);
        if(user != null){
            return successResponse(userService.findUserByEmail(email), "success");
        }
        return failResponse("fail");
    }

    @GetMapping
    public ResponseEntity<Response> getAllUsers(){
        List<User> users = userService.getAllUsers();
        if(users != null){
            return successResponse(users, "success");
        }
        return failResponse("fail");
    }

    @PutMapping(value = "/update", headers = "Accept=application/json")
    public ResponseEntity<Response> updateUser(@RequestBody UserUpdateForm user){
        try{
            if(userService.updateUser(user)){
                return successResponse("success");
            }
            return failResponse("fail");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @DeleteMapping("/{id}/delete")
    public ResponseEntity<Response> deleteUser(@PathVariable("id") int userId){
        try{
            if(userService.deleteUser(userId)){
                return successResponse("success");
            }
            return failResponse("fail");
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

    @PostMapping(value = "/create", headers = "Accept=application/json")
    public ResponseEntity<Response> createUser(@RequestBody UserAddForm user){
        try {
            if(!user.getFirstPassword().equals(user.getSecondPassword())){
                return failResponse("Password are not match");
            }else if(userService.createUser(user)){
                System.out.println(user.getId());
                System.out.println(user.getEmail());
                return successResponse(userService.findUserById(user.getId()), "success");
            }else{
                return failResponse("fail to create new user");
            }
        }catch (Exception e){
            e.printStackTrace();
            return internalErrorResponse();
        }
    }

}
