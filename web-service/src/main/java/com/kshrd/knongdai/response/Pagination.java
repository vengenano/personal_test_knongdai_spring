package com.kshrd.knongdai.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by User on 01-Aug-17.
 */
public class Pagination {

    @JsonProperty("current_page")
    private int currentPage;
    @JsonProperty("total_record")
    private int totalRecord;
    private int limit;

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getTotalRecord() {
        return totalRecord;
    }

    public void setTotalRecord(int totalRecord) {
        this.totalRecord = totalRecord;
    }

    @JsonProperty("total_page")
    public int getTotalPage() {
        return (int)Math.ceil((double)totalRecord / limit);
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }
}
