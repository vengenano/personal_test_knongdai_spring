package com.kshrd.knongdai.response;

import java.util.List;

/**
 * Created by User on 01-Aug-17.
 */
public class PaginationResponseList<T> extends ResponseList<T> {

    private Pagination pagination;

    public PaginationResponseList(String code, boolean status, String msg, List<T> data){
        super(code, status, msg, data);
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }
}
