package com.kshrd.knongdai.response;

import java.util.List;

public class ResponseList<T> extends Response<T> {

    private List<T> data;

    public ResponseList(String code, boolean status, String msg, List<T> data) {
        super(code, status, msg);
        this.data = data;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
