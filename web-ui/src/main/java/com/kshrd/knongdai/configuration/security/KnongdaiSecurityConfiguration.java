package com.kshrd.knongdai.configuration.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@Order(2)
public class KnongdaiSecurityConfiguration extends WebSecurityConfigurerAdapter{

	private BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
	private UserDetailsService userDetailsService;
	private LoginSuccessHandler loginSuccessHandler;

	@Autowired
	public KnongdaiSecurityConfiguration(
			UserDetailsService userDetailsService,
			@Qualifier("loginSuccessHandler") LoginSuccessHandler loginSuccessHandler) {
		this.userDetailsService = userDetailsService;
		this.loginSuccessHandler = loginSuccessHandler;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
				.antMatchers("/api/v1/**", "/index/**", "/", "/search", "/redirect/**").permitAll()
				.antMatchers("/admin/**", "/web-service").hasAnyRole("ADMIN")
//				.anyRequest().authenticated()
			.and()
			.formLogin().permitAll()
				.usernameParameter("email")
				.passwordParameter("password")
				.loginPage("/login")
				.failureUrl("/login?error")
				.successHandler(loginSuccessHandler)
				//.failureHandler(customFailureHandler)
			.and()
				.logout()
				.permitAll()
			.and()
			.exceptionHandling().accessDeniedPage("/error/403");
//			.exceptionHandling().accessDeniedHandler(accessDeniedHandler);

		http.csrf().disable();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {

		auth.userDetailsService(userDetailsService)
			.passwordEncoder(bCryptPasswordEncoder);

	}
	
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/resources/**");
		web.ignoring().antMatchers("/static/**");
	}
	
}
