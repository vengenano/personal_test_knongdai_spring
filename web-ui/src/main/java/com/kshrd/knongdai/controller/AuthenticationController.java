package com.kshrd.knongdai.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**********************************************************************
 *
 * Author: Theara
 * Created Date: 22/July/2017
 * Development Group: PP_G3
 * Description: AuthenticationController class is created for
 * 				manage the Authentication user request
 *
 **********************************************************************/

@Controller
public class AuthenticationController {

	@RequestMapping("/logout")
	public String logout(HttpServletRequest request, HttpServletResponse response ){
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth != null){
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/login?logout";
	}
	
	@RequestMapping("/login")
	public String loginPage(ModelMap model){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(!auth.getPrincipal().equals("anonymousUser")){
			
			// Get User from session
//			User usr = (User)auth.getPrincipal();
//			model.addAttribute("logged",true);
			return "redirect:/admin/dashboard";
		}
		return "login";
	}
	
	@RequestMapping("/login-swagger")
	public String loginWaggerPage(ModelMap model){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(!auth.getPrincipal().equals("anonymousUser")){
			System.out.println(auth.getPrincipal());
			model.addAttribute("logged",true);
			return "redirect:/web-service";
		}
		return "login-swagger";
	}

}
