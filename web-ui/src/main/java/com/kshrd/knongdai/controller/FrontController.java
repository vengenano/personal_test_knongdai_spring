package com.kshrd.knongdai.controller;

import com.kshrd.knongdai.service.CategoryService;
import com.kshrd.knongdai.service.KeywordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**********************************************************************
 *
 * Author: Chanpheng
 * Created Date: 24/July/2017
 * Development Group: PP_G3
 * Description: FrontController is a controller for handling front-end pages
 *
 **********************************************************************/

@Controller
public class FrontController {

    private CategoryService categoryService;
    private KeywordService keywordService;

    @Autowired
    public FrontController(CategoryService categoryService,KeywordService keywordService){
        this.categoryService  = categoryService;
        this.keywordService=keywordService;
    }

    @RequestMapping({"/index", "/"})
    public String front(Model model){
        model.addAttribute("MAIN_CATES", categoryService.getAllMainCategories());
        return "/front/index";
    }

}
