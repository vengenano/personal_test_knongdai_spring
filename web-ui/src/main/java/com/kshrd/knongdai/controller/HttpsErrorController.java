package com.kshrd.knongdai.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/error")
public class HttpsErrorController {

	@RequestMapping("/403")
	public String accessDenied(){
		return "errors/403";
	}
	
	@RequestMapping("/401")
	public String notAuthorized(){
		return "errors/401";
	}
	
}
