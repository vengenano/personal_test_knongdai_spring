notify={};
$(document).ready(function () {
    notify.getAllNotification= function () {
        var notification=$("#total-notification");
        $.ajax({
            url : kd.host + "/api/v1/urls?page=1&approved=false",
            type:"GET",
            beforeSend : function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                notification.html('<i class="fa fa-spin fa-spinner"></i>');
            },
            success:function(data){
                notification.text(data.pagination.total_record);
                var notifyItem="";
                for(var index in data.data){
                    var row = data.data[index];
                    notifyItem+='<a href="'+row.link+'" class="single-mail">' +
                                    '<strong>' +
                                    '<img src="https://www.google.com/s2/favicons?domain='+data.link+'"> ' +
                                        ''+' '+row.title+'' +
                                    '</strong>' +
                                    '<p><small>'+row.link+'</small></p>' +
                                    '<span class="un-read tooltips" data-original-title="Mark as Read" data-toggle="tooltip" data-placement="left">' +
                                        '<i class="fa fa-circle"></i>' +
                                    '</span>' +
                                 '</a>';
                }
                    notifyItem+='<a href="/admin/urls">' +
                                    '<button class="btn-danger form-control btn-view-all">' +
                                        'Show more' +
                                    '</button>' +
                                '</a>';
                $("#div-get-add-notify").html(notifyItem)
            },
            error:function(data,status,er) {
                console.log(data);

            }
        });
    }
    notify.getAllNotification();
});