addManual={};
$(document).ready(function () {
    $('#tags_1').tagsInput({width:'100%'});
    $("#FormAddURL").submit(function (e) {
        e.preventDefault();
        addManual.addURL();
    });
    $(document).on("click", "#div-add-manual", function () {
        $('#addUrlModal').modal('show');
        $("#btn-trigger-file-open").removeClass("disabled");
        $('#upload-progress').hide();
        $("#FormAddURL #txt-Url-Title-Add").val("");
        $("#FormAddURL #txt-Addr-Add").val("");
        $("#FormAddURL #txt-Desc-Add").val("");
        $("#FormAddURL #txt-Email-Add").val("");
        $("#FormAddURL #txt-UrlLink-Add").val("");
        $("#FormAddURL #txt-Tel-Add").val("");
        $("#FormAddURL #txt-Img-Add").val("");
        $('#tags_1').val("");
        addManual.getAllMainCategorieOption("#select-get-add-MainCate");
        addManual.getAllSubCategorieOption(1,"#select-get-add-SubCate-ByMain");
        $('#select-get-add-MainCate').dropdown('refresh');
    });
    $('.ui.radio.checkbox').checkbox();
    $(document).on("change","#select-get-add-MainCate",function () {
        $("#div-subCate>div>.text").text("ជ្រើសរើសប្រភេទតូចៗ...");
        addManual.getAllSubCategorieOption($(this).val(),"#select-get-add-SubCate-ByMain")
    });
    $("#btn-trigger-file-open").click(function () {
        $("#txt-Img-File").trigger("click");
    });
    $(document).on("change","#txt-Img-File",function () {
        var formData = new FormData();
        formData.append('file', $('#txt-Img-File').prop('files')[0]);
        // console.log("go");
        $.ajax({
            url : kd.host+"/api/v1/urls/upload/web-icon",
            type:"POST",
            data:formData,
            contentType:false,
            processData:false,
            beforeSend : function (xhr) {
                $('#upload-progress').fadeIn(200);
                for(var i = 1;i<80;i++){
                    $('#upload-progress').progress({
                        percent: i
                    });
                }
            },
            success:function(data){
                $('#upload-progress').progress({
                    percent: 100
                });
                console.log(data);
                $("#txt-Img-Add").val(data.data);
                $("#btn-trigger-file-open").addClass("disabled");
                // $("#txt-Img-Add").addClass("ui disabled input");
            },
            error:function(data,status,er) {
                $("#btn-trigger-file-open").removeClass("disabled");
            }
        });
    });
    addManual.getAllMainCategorieOption = function (Selector) {
        $.ajax({
            url : kd.host+"/api/v1/categories",
            type:"GET",
            beforeSend : function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                $(Selector).empty();
            },
            success:function(data){
                var select_MainCate='<option value="">ជ្រេីសរេីសប្រភេទនៃគេហទំព័រ</option>';
                for(var index in data.data){
                    var row = data.data[index];
                    if(row.id==1){
                        var sel="SELECTED";
                    }else{
                        var sel="";
                    }
                    select_MainCate += "<option value='"+row.id+"' "+sel+">"+row.cate_name+"</option>";
                }
                $(Selector).html(select_MainCate);
            },
            error:function(data,status,er) {
                console.log(data);
                swal(data.msg);
            }
        });
    };
    addManual.getAllSubCategorieOption = function (ID,Selector) {
        $.ajax({
            url : kd.host+"/api/v1/categories/sub-by-main-id/"+ID,
            type:"GET",
            beforeSend : function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                $(Selector).empty();
            },
            success:function(data){
                var select_SubCate= '';
                for(var index in data.data){
                    var row = data.data[index];
                    select_SubCate += "<option value='"+row.id+"'>"+row.cate_name+"</option>";
                }
                $(Selector).html(select_SubCate);
            },
            error:function(data,status,er) {
                console.log(data);
                swal(data.msg);
            }
        });
    };
    addManual.addURL = function () {
        var sms= $("#sms");
        var keyword = $('#FormAddURL #tags_1').val();
        var keywords = keyword.split(',');
        var jsonKeyword=[];

        for(var i = 0; i < keywords.length; i++){
            jsonKeyword.push(keywords[i]);
        }
        console.log(jsonKeyword);

        var txtTitle_Add=$("#FormAddURL #txt-Url-Title-Add").val();
        URLDataAdd = {
            "address":  $("#FormAddURL #txt-Addr-Add").val(),
            "des":      $("#FormAddURL #txt-Desc-Add").val(),
            "email":    $("#FormAddURL #txt-Email-Add").val(),
            "keywords": jsonKeyword,
            "link":     $("#FormAddURL #txt-UrlLink-Add").val(),
            "main_cate_id": $("#FormAddURL #select-get-add-MainCate").val(),
            "phone":    $("#FormAddURL #txt-Tel-Add").val(),
            "pic_url":  $("#FormAddURL #txt-Img-Add").val(),
            "sub_cate_id": $("#FormAddURL #select-get-add-SubCate-ByMain").val(),
            "title":    txtTitle_Add,
            "type":     $("input[name='urltype']:checked").val()
        };
        $.ajax({
            url : kd.host+"/api/v1/urls/create",
            type : "POST",
            data : JSON.stringify(URLDataAdd),
            beforeSend : function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                sms.text("សូមមេត្តារង់ចាំ...")
                    .css({
                        "background-color":"yellow",
                        "color":"black"
                    })
                    .slideDown(200);
            },
            success : function(data) {
                console.log(data.msg)
                sms.text("ការស្នើរបានជោគជ័យ...")
                    .css({
                        "background-color":"#009900",
                        "color":"white"
                    })
                    .slideDown(200);
                setTimeout(function () {
                    sms.slideUp(200);
                },3000);
                $("#addUrlModal").modal('hide');
            },
            error : function(data, err, status) {
                console.log(data);
                sms.text("ការស្នើរបានបរាជ័យ...")
                    .css({
                        "background-color":"red",
                        "color":"white"
                    })
                    .slideDown(200);
                setTimeout(function () {
                    sms.slideUp(200);
                },3000);
            }
        });
    }
    $("#btn-Cancel-add").click(function (e) {
        e.preventDefault();
        $("#addUrlModal").modal('hide');
    });
});