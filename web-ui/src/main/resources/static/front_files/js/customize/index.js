$(document).ready(function () {

    $(document).keypress(function(e) {
        if ($("#txtSearch").is(":focus")) {
            if(e.which == 13) {
                $("#div_btnSearch").trigger("click");
            }
        }
    });
    $("#div_btnSearch").click(function (e) {
        var value = $("#txtSearch").val().trim().replace(/\\/g, '');
        if(value.length==0){

        }else{
            window.location.href = kd.host+"/search?q="+ value+"&page=1";
        }

    });
    var divtxtSearch=$("#div-txt-Search");


    var options = {
        url: function(phrase) {
            return kd.host+'/api/v1/search/keyword/' + phrase;//.split(' ').pop();
        },
        getValue: function(element) {
            return element;
        },
        ajaxSettings: {
            dataType: "json",
            method: "GET",
            success: function(element) {
                // console.log(element)
                var ArrayContent = element.length;
                if(ArrayContent==""){
                    //console.log("empty")
                }else {
                    $("#eac-container-txtSearch").slideDown().css('width', divtxtSearch.width() + 'px');
                }
            }
        },
        list:{
            showAnimation: {
                type: "slide", //normal|slide|fade
                time: 100,
                callback: function() {}
            },

            hideAnimation: {
                type: "slide", //normal|slide|fade
                time: 100,
                callback: function() {}
            },
            onClickEvent:function(result) {
                // var value= $("#txtSearch").val($("#txtSearch").val() + E.getSelectedItemData());
                var value = $("#txtSearch").val();
                window.location.href = kd.host+"/search?q="+ value+"&page=1";
            },
            onKeyEnterEvent:function(result) {
                // var value= $("#txtSearch").val($("#txtSearch").val() + E.getSelectedItemData());
                var value = $("#txtSearch").val();
                window.location.href = kd.host+"/search?q="+ value+"&page=1";
            }
        }
        ,
        requestDelay: 0
    };
    $("#txtSearch").easyAutocomplete(options);
});
