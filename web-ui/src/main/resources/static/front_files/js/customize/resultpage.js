var result = {};
var port = 'http://localhost:9090';
var check=true;

$(document).ready(function () {

    var page = 1;

    var toggleCateAccordion = function () {
        var mainCateAcc = $(".focusing").parent().parent().parent().parent().parent().prev();
        if(!mainCateAcc.hasClass('active')){
            mainCateAcc.trigger('click');
        }
    };

    setTimeout(function () {
        if($(".loading").is(":visible")){
            $("#div_btnSearch").trigger("click");
        }
    },2000);
    setTimeout(function () {
        if($(".loading").is(":visible")){
            $("#div_btnSearch").trigger("click");
        }else{
        }
    },5000);
    var divtxtSearch=$("#div-txt-Search");

    var options = {
        url: function(phrase) {
            return kd.host+'/api/v1/search/keyword/' + phrase;
        },
        getValue: function(element) {
            return element;
        },
        ajaxSettings: {
            dataType: "json",
            method: "GET",
            success: function(element) {
                // console.log(element)
                var ArrayContent = element.length;
                if(ArrayContent==""){
                    //console.log("empty")
                }
            }
        },
        list:{
            showAnimation: {
                type: "slide", //normal|slide|fade
                time: 0,
                callback: function() {}
            },

            hideAnimation: {
                type: "slide", //normal|slide|fade
                time: 0,
                callback: function() {}
            },
            onClickEvent:function(result) {
                var query = $("#txtSearch").val();
                getData(query);
                check = true;
                $('body').css('overflow-y','auto');
            },
            onKeyEnterEvent:function(result) {
                var query = $("#txtSearch").val();
                getData(query);
                check = true;
                $('body').css('overflow-y','auto');
            }
        }
        ,
        requestDelay: 0
    };
    $("#txtSearch").easyAutocomplete(options);
    $("#txtSearch").focus(function () {
        $("#eac-container-txtSearch").css({
            "display":"block"
        });
    });
    $(document).keypress(function(e) {
        if ($("#txtSearch").is(":focus")) {
            if(e.which == 13) {
                check = true;
                $("#div_btnSearch").trigger("click");
            }
        }
    });
    $("#div_btnSearch").click(function (e) {
        var value = $("#txtSearch").val().trim().replace(/\\/g, '');
        $('body').css('overflow-y','auto');
        if(value.length==0){

        }else{
            getData(value);
        }
        $("#txtSearch").trigger("blur");
    });
    $("#overlay-div").focusout(function(){
        $(this).css("background-color", "#FFFFFF");
    });
    /************************************/
    /*****end of on enter for search*****/
    /************************************/

    var width = $(window).width();
    if (width > 750) {
        result.setPagination = function(totalPage, currentPage){
            $('#pagination').bootpag({
                total: totalPage,
                page: currentPage,
                maxVisible: 10,
                leaps: true,
                firstLastUse: true,
                first: 'ដំបូង',
                last: 'ចុងក្រោយ',
                wrapClass: 'pagination',
                activeClass: 'active',
                disabledClass: 'disabled',
                nextClass: 'next',
                prevClass: 'prev',
                lastClass: 'last',
                firstClass: 'first'
            })
        };
    } else if (width <= 750)  {
        result.setPagination = function(totalPage, currentPage){
            $('#pagination').bootpag({
                total: totalPage,
                page: currentPage,
                maxVisible: 6,
                leaps: true,
                firstLastUse: true,
                first: '|<',
                last: '>|',
                wrapClass: 'pagination',
                activeClass: 'active',
                disabledClass: 'disabled',
                nextClass: 'next',
                prevClass: 'prev',
                lastClass: 'last',
                firstClass: 'first'
            })
        };

    }

    $('#pagination').on("page", function(event, currentPage){
        check = false;
        getData(kd.search.q, currentPage, kd.search.cateId);
    });

    var removeFocusingClassFromAllSubCates = function(){
        $(".mystyle>.content>.header>span").removeClass("focusing").removeClass('active');
    };

    $(".mystyle").click(function () {
        removeFocusingClassFromAllSubCates();
        $(this).find("span").addClass("focusing");
    });

    if($("#ul-Urls>li").length<=0){
        $("#h2showRes").fadeIn(2000);
        $("#spanResult>#count-li-row").text(0);
    }

    $("#txtSearch").keyup(function () {
        $("#txtSearchFixed").val($(this).val());
    });
    /*****************************/
    /* Starting Searching Script */
    /*****************************/
    kd.search = {};

    var setResult = function (data) {
        $("#ul-Urls").empty();
        $("#sites-template-tmpl").tmpl({SITES: data.data}).appendTo("#ul-Urls").each(function () {
            $("#eac-container-txtSearch").hide();
            $("#Search-Result-url").removeClass("loading");
        });
        $("#count-li-row").text(data.pagination.total_record);
        $("#span_keyword_name").text($("#txtSearch").val());
        if(check){
            if(data.data.length != 0){
                result.setPagination(data.pagination.total_page, (kd.search.page) ? kd.search.page : 1);
                check=false;
            }else {
                $('#pagination').html("");
                $("#Search-Result-url").removeClass("loading");
            }
        }

        removeFocusingClassFromAllSubCates();

        if(data.search.cateId){
            var $subCate = $('a[data-id="' + data.search.cateId + '"]');
            $subCate.find("span").addClass("focusing active");
            setSearchKeyword($subCate.find("span")[0].innerText);
            toggleCateAccordion();
        }
        if(data.search.query){
            $("#txtSearch").val(data.search.query);
        }

    };

    var buildQueryUrl = function (query, page, cateId) {
        var url = '?';
        var andMark = false;
        if(query){
            url += 'q=' + query;
            andMark = true;
        }
        if(page){
            url += ((andMark) ? '&page=' : 'page=') + page;
            andMark = true;
        }
        if(cateId){
            url += ((andMark) ? '&cate_id=' : 'cate_id=') + cateId;
        }
        return url;
    };

    var addAdditionalData = function(data) {
        var params = new URL(document.URL).searchParams;
        var query = params.get('q');
        var cateId = params.get('cate_id');
        data.search = {
            cateId : kd.search.cateId,
            query : kd.search.q
        };
        return data;
    };

    var firstLoad = function (data) {
        console.log('replace state');
        history.replaceState(addAdditionalData(data), undefined, buildQueryUrl(kd.search.q, kd.search.page, kd.search.cateId));
        setResult(data);
    };

    var successGetUrlHandler = function (data) {
        history.pushState(addAdditionalData(data), null, buildQueryUrl(kd.search.q, kd.search.page, kd.search.cateId));
        setResult(data);
    };

    var queryData = function (query, page, cateId, successHandler) {
        kd.getAjax({
            path: '/search' + buildQueryUrl(query, page, cateId),
            success: successHandler,
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                $("#Search-Result-url").addClass("loading");
            }
        });
    };

    var loadState = function () {
        var params = new URL(document.URL).searchParams;
        kd.search.q = params.get('q');
        kd.search.page = params.get('page');
        kd.search.cateId = params.get('cate_id');
        queryData(kd.search.q, kd.search.page, kd.search.cateId, firstLoad);
    };

    var getData = function (q, page, cateId) {
        kd.search.q = q;
        kd.search.page = page;
        kd.search.cateId = cateId;
        queryData(q, page, cateId, successGetUrlHandler);
        $("#txtSearch").trigger("blur");
        $("#overlay-div").fadeOut(200);
    };

    $(loadState);

    $(window).bind('popstate', function(event) {
        var data = event.originalEvent.state;
        setResult(data);
        var params = new URL(document.URL).searchParams;
        var page = params.get('page');
        result.setPagination(data.pagination.total_page, (page) ? page : 1);
    });

    var setSearchKeyword = function(value){
        $("#txtSearch").val(value);
        $('#txtSearchFixed').val(value);
        $("#span_keyword_name").text(value);
    };

    $('.sub-cate-container .item').click(function (e) {
        e.preventDefault();
        check = true;
        getData(undefined, undefined, $(this).data('id'));
        var value = $(this).find(".span-SubCate-name").text();
        setSearchKeyword(value);
    });

    $(document).on('click', '.url-list .redirect', function (e) {
        e.preventDefault();
        window.open(kd.host + '/redirect/' + $(this).closest('.url-list').data('id'), '_blank');
    });

    setInterval(function () {
        if (navigator.onLine) {
            $("#no-connectivity").slideUp(200);
            $("#whiteScreen").fadeOut(1000);
            if ($(window).scrollTop() > 100){
                $("#textbox-fixed").slideDown(100);
            }
        }else{
            $("#no-connectivity").slideDown(200);
            $("#whiteScreen").fadeIn(1000);
            if($("#textbox-fixed").is(":visible")){
                $("#textbox-fixed").slideUp();
            }
        }
    },1000);
});
