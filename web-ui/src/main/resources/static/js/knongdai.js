var kd = {};
kd.host = '';
kd.ajax = function (config) {

    $.ajax({
        url : kd.host + '/api/v1' + config.path,
        type: config.method,
        data: config.data,
        beforeSend : config.beforeSend,
        success: config.success,
        error: (config.error) ? config.error : function (data, status, error) {
            console.error('Knongdai : ' + "There are some errors!");
            console.error(data);
        }
    });

};

kd.getAjax = function (config) {

    kd.ajax({
        path : config.path,
        method : 'GET',
        data : config.data,
        beforeSend : config.beforeSend,
        success : config.success,
        error : config.error
    });

};

kd.postAjax = function (config) {

    kd.ajax({
        path : config.path,
        method : 'POST',
        data : config.data,
        beforeSend : config.beforeSend,
        success : config.success,
        error : config.error
    });

};

