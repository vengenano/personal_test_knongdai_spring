/**
 * Created by Theara on 03-Aug-17.
 */
var kwObj = {};
var test;
var check = true;
var myCurrentPage = 1;
$(document).ready(function (){
    var loading = $("div#loading");
    kwObj.getAllKeyword = function(page,Loading,sortBy,sortOrdering){
        $.ajax({
            url:  kd.host+"/api/v1/new-keywords?sort_by="+sortBy+"&order="+sortOrdering+"&page="+page,
            type: "GET",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                Loading.show();
            },
            success: function(data) {

                console.log(data);
                $("#kw-data").empty();
                $("#kw-template-tmpl").tmpl({kws: data.data,pages:data.pagination}).appendTo("#kw-data");
                if(check){
                    kwObj.setPagination(data.pagination.total_page,page);
                    check=false;
                }
                Loading.hide();
            },
            error:function(data,status,er) {
                console.log(data);
            }
        });

    };
    kwObj.setPagination = function(totalPage, currentPage){
        $('#pagination').bootpag({
            total: totalPage,
            page: currentPage,
            maxVisible: 8,
            leaps: true,
            firstLastUse: true,
            first: 'First',
            last: 'Last',
            wrapClass: 'pagination',
            activeClass: 'active',
            disabledClass: 'disabled',
            nextClass: 'next',
            prevClass: 'prev',
            lastClass: 'last',
            firstClass: 'first'
        }).on("page", function(event, currentPage){
            check = false;
            myCurrentPage = currentPage;
            if($("#txt-check-pagination").val()==1){
                var filter = $("input[name='opt-map']:checked").val();
                kwObj.getKeywordFilter(myCurrentPage,loading,filter);
            }else {
                kwObj.getAllKeyword(myCurrentPage,loading,"total_traffic","DESC");
            }

        });
    };

    $(document).on("click","#btn-map-kw",function (e) {
        e.preventDefault();
        kwObj.getAllMainCategorieOption(1,"#divMainCateMap,#divMainCateMap-to-sub","select-mainCate-Map");
        kwObj.getAllSubCategorieOption(1,1,"#divSubCateMap","select-subCate-Map");
        $("#txt-keyword-passing").val($(this).data("kwname"));
        $("#KWMapModal").modal("show");
    });
    $(document).on("change","#select-mainCate-Map",function () {
        kwObj.getAllSubCategorieOption($(this).val(),1,"#divSubCateMap","select-subCate-Map");
    });
    $(document).on("click","#btn-map-MainCate",function (e) {
        e.preventDefault();
        var keyword = $("#txt-keyword-passing").val();
        kwObj.mapMainCate(keyword);
    });
    $(document).on("click","#btn-map-subCate",function (e) {
        e.preventDefault();
        var keyword = $("#txt-keyword-passing").val();
        kwObj.mapSubCate(keyword);
    });
    $(document).on("click","#btn-get-all-kw",function (e) {
        e.preventDefault();
        $("#txt-check-pagination").val("");
        kwObj.getAllKeyword(myCurrentPage,loading,"total_traffic","DESC");
    });
    $(document).on("click","#btn-filter-map",function (e) {
        e.preventDefault();
        var filter = $("input[name='opt-map']:checked").val();
        myCurrentPage=1;
        check=true;
        $("#txt-check-pagination").val(1);
        kwObj.getKeywordFilter(myCurrentPage,loading,filter);
    });
    $(document).on("click","#btn-map-URL",function (e) {
        e.preventDefault();
        var keyword = $("#txt-keyword-passing").val();
        var url_id= $("#txt-input-url-id").val().trim();
        if(url_id.length==0){
            $("#txt-input-url-id").css({
                "border":"1px solid red"
            });
        }else{
            kwObj.mapKWToUrl(keyword,url_id);
        }
    });
    kwObj.getKeywordFilter = function (page,Loading,type) {
        $.ajax({
            url : kd.host+"/api/v1/new-keywords?page="+page+"&mapped="+type,
            type : "GET",
            beforeSend : function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                Loading.show();
            },
            success: function(data) {
                Loading.hide();
                console.log(data);
                $("#kw-data").empty();
                $("#kw-template-tmpl").tmpl({kws: data.data,pages:data.pagination}).appendTo("#kw-data");
                if(check){
                    kwObj.setPagination(data.pagination.total_page,page);
                    check=false;
                }

            },
            error:function(data,status,er) {
                console.log(data);
            }
        });
    }
    kwObj.mapMainCate = function (kwName) {
        var mainCateName=$("#divMainCateMap option:selected").text();
        KWDataMap ={
            "cate_id": $("#select-mainCate-Map").val(),
            "keyword": kwName
        }

        console.log(KWDataMap);
        swal({
                title : " Mapping Keyword",
                text : "Are you sure you want to map \" "+kwName+" \" to Main Category "+mainCateName+"?",
                type : "info",
                showCancelButton : true,
                closeOnConfirm : true,
                showLoaderOnConfirm : true
            },
            function() {

                $.ajax({
                    url : kd.host+"/api/v1/new-keywords/map-category",
                    type : "POST",
                    data : JSON.stringify(KWDataMap),
                    beforeSend : function(xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success : function(data) {
                        console.log(data.msg)
                        swal(data.msg);
                        if($("#txt-check-pagination").val()==1){
                            var filter = $("input[name='opt-map']:checked").val();
                            kwObj.getKeywordFilter(myCurrentPage,loading,filter);
                        }else {
                            kwObj.getAllKeyword(myCurrentPage,loading,"total_traffic","DESC");
                        }
                        $('#KWMapModal').modal('hide');
                    },
                    error : function(data, err, status) {
                        console.log(data);
                        swal(data.msg);
                    }
                });

            });
    }
    kwObj.mapKWToUrl = function (kwName,url_ID) {
        KWDataMap =
        {
            "keyword": kwName,
            "url_id": url_ID
        }

        console.log(KWDataMap);
        swal({
                title : " Mapping Keyword",
                text : "Are you sure you want to map \" "+kwName+" \" to URL's ID "+url_ID+"?",
                type : "info",
                showCancelButton : true,
                closeOnConfirm : true,
                showLoaderOnConfirm : true
            },
            function() {

                $.ajax({
                    url : kd.host+"/api/v1/new-keywords/map-url",
                    type : "POST",
                    data : JSON.stringify(KWDataMap),
                    beforeSend : function(xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success : function(data) {
                        console.log(data.msg)
                        swal(data.msg);
                        if($("#txt-check-pagination").val()==1){
                            var filter = $("input[name='opt-map']:checked").val();
                            kwObj.getKeywordFilter(myCurrentPage,loading,filter);
                        }else {
                            kwObj.getAllKeyword(myCurrentPage,loading,"total_traffic","DESC");
                        }
                        $('#KWMapModal').modal('hide');
                    },
                    error : function(data, err, status) {
                        console.log(data);
                        swal(data.msg);
                    }
                });

            });
    }
    kwObj.mapSubCate = function (kwName) {
        var mainCateName=$("#divMainCateMap option:selected").text();
        KWDataMap ={
            "cate_id": $("#select-subCate-Map").val(),
            "keyword": kwName
        }

        console.log(KWDataMap);
        swal({
                title : " Mapping Keyword",
                text : "Are you sure you want to map \" "+kwName+" \" to Main Category "+mainCateName+"?",
                type : "info",
                showCancelButton : true,
                closeOnConfirm : true,
                showLoaderOnConfirm : true
            },
            function() {

                $.ajax({
                    url : kd.host+"/api/v1/new-keywords/map-category",
                    type : "POST",
                    data : JSON.stringify(KWDataMap),
                    beforeSend : function(xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success : function(data) {
                        console.log(data.msg)
                        swal(data.msg);
                        if($("#txt-check-pagination").val()==1){
                            var filter = $("input[name='opt-map']:checked").val();
                            kwObj.getKeywordFilter(myCurrentPage,loading,filter);
                        }else {
                            kwObj.getAllKeyword(myCurrentPage,loading,"total_traffic","DESC");
                        }
                        $('#KWMapModal').modal('hide');
                    },
                    error : function(data, err, status) {
                        console.log(data);
                        swal(data.msg);
                    }
                });

            });
    }
    kwObj.getAllMainCategorieOption = function (ID,Selector,SetselectOptionID) {
        $.ajax({
            url : kd.host+"/api/v1/categories",
            type:"GET",
            beforeSend : function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                $(Selector).html('<i class="fa fa-spin fa-spinner"></i> &nbsp;&nbsp; Loading...');
            },
            success:function(data){
                var select_MainCate= '<select class="form-control" id='+SetselectOptionID+'>';
                for(var index in data.data){
                    var row = data.data[index];
                    if(row.id==ID){
                        var Selected="SELECTED";
                    }else {
                        var Selected="";
                    }
                    select_MainCate += "<option value='"+row.id+"' "+Selected+">"+row.cate_name+"</option>";
                }
                select_MainCate+= '</select>';
                $(Selector).html(select_MainCate);
            },
            error:function(data,status,er) {
                console.log(data);
                swal(data.msg);
            }
        });
    };
    kwObj.getAllSubCategorieOption = function (ID,SubID,Selector,SetSelectOptionID) {
        $.ajax({
            url : kd.host+"/api/v1/categories/sub-by-main-id/"+ID,
            type:"GET",
            beforeSend : function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                $(Selector).html('<i class="fa fa-spin fa-spinner"></i> &nbsp;&nbsp; Loading...');
            },
            success:function(data){
                var select_SubCate= '<select class="form-control" id='+SetSelectOptionID+'>';
                for(var index in data.data){
                    var row = data.data[index];
                    if(row.id==SubID){
                        var Selected="SELECTED";
                    }else {
                        var Selected="";
                    }
                    select_SubCate += "<option value='"+row.id+"' "+Selected+">"+row.cate_name+"</option>";
                }
                select_SubCate+= '</select>';
                $(Selector).html(select_SubCate);
            },
            error:function(data,status,er) {
                console.log(data);
                swal(data.msg);
            }
        });
    };
    kwObj.getAllKeyword(myCurrentPage,loading,"total_traffic","DESC");




});