/**
 * Created by Theara on 03-Aug-17.
 */

$(document).ready(function (){

    var scrapeObj = {};

    var getScrapingData = function() {
    };

    scrapeObj.getScrapingData = function(query){
        $.ajax({
            url:  kd.host + '/api/v1/scrape?' + query,
            type: "GET",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                $("div#loading>p>span").text("Please wait while connecting to the site ....");
                setTimeout(function () {
                    $("div#loading>p>span").text("This may take some time...");
                },3000);
                setTimeout(function () {
                    $("div#loading>p>span").text("Check Duplicating...");
                },300000);
                setTimeout(function () {
                    $("div#loading>p>span").text("Validation website...");
                },600000);
                $("div#loading").fadeIn(1000);
            },
            success: function(data) {
                var $tableBody = $('#scraping-data');
                $tableBody.empty();
                $("#url-template-tmpl").tmpl({urls: data.data}).appendTo($tableBody);
                $('#scraping-table').DataTable();
                $("div#loading").fadeOut(1000);
            },
            error:function(data,status,er) {
                console.log(data);
                $("div#loading>p>span").text("Perhaps There is no data...");
                setTimeout(function () {
                    $("div#loading>p>span").text("Please Try another keyword...");
                },3000);
                setTimeout(function () {
                    $("div#loading").fadeOut(200);
                },4000);
            }
        });

    };

    $("#scraping-form").submit(function(e){
        e.preventDefault();
        scrapeObj.getScrapingData($(this).serialize());
    });

    $(document).on('click', '.btn-add-url', function () {
        var $row = $(this).closest('tr');
        $("#txtTitle_Add").val($row.find('.title').val().trim());
        $("#txtLink_Add").val($row.find('.link').val().trim());
        $("#txtAddr_Add").val($row.find('.address').val().trim());
        $("#txtTel_Add").val($row.find('.phone').val().trim());
        $("#txtEmail_Add").val($row.find('.email').val().trim());
        $("#txtimgURL_Add").val($row.find('.img').attr("src"));
        $("#txtDesc_Add").val($row.find('.des').val().trim());

        scrapeObj.getAllMainCategorieOption(1,"#divMainCateAdd","select-mainCate-Add");
        scrapeObj.getAllSubCategorieOption(1,1,"#divSubCateAdd","select-subCate-Add");
        $("#URLAddModal").modal('show');
    });
    $(document).on("click","#select-mainCate-Add",function () {
        scrapeObj.getAllSubCategorieOption($(this).val(),1,"#divSubCateAdd","select-subCate-Add");
    });
    $("#URLAddForm").submit(function(e) {
        e.preventDefault();
        scrapeObj.addURL();
    });
    scrapeObj.addURL = function () {

        var keyword = $('#URL_kw_Add').val();
        var keywords = keyword.split(',');
        var jsonKeyword=[];

        for(var i = 0; i < keywords.length; i++){
            jsonKeyword.push(keywords[i]);
        }
        console.log(jsonKeyword);

        var txtTitle_Add=$("#URLAddModal #txtTitle_Add").val();
        URLDataAdd = {
            "address":  $("#URLAddModal #txtAddr_Add").val(),
            "approved": true,
            "des":      $("#URLAddModal #txtDesc_Add").val(),
            "email":    $("#URLAddModal #txtEmail_Add").val(),
            "keywords": jsonKeyword,
            "link":     $("#URLAddModal #txtLink_Add").val(),
            "main_cate_id": $("#URLAddModal #select-mainCate-Add").val(),
            "phone":    $("#URLAddModal #txtTel_Add").val(),
            "pic_url":  $("#URLAddModal #txtimgURL_Add").val(),
            "status": true,
            "sub_cate_id": $("#URLAddModal #select-subCate-Add").val(),
            "title":    txtTitle_Add,
            "type":     $("input[name='optradio-type']:checked").val()
        };

        console.log(URLDataAdd);
        swal({
                title : " Add New URL",
                text : "Are you sure you want to add \" "+txtTitle_Add+" \" URL?",
                type : "info",
                showCancelButton : true,
                closeOnConfirm : true,
                showLoaderOnConfirm : true
            },
            function() {

                $.ajax({
                    url : kd.host+"/api/v1/urls/create",
                    type : "POST",
                    data : JSON.stringify(URLDataAdd),
                    beforeSend : function(xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success : function(data) {
                        console.log(data.msg)
                        swal(data.msg);
                        $('#URLAddModal').modal('hide');
                    },
                    error : function(data, err, status) {
                        console.log(data);
                        swal(data.msg);
                    }
                });

            });
    }
    scrapeObj.getAllMainCategorieOption = function (ID,Selector,SetselectOptionID) {
        $.ajax({
            url : kd.host+"/api/v1/categories",
            type:"GET",
            beforeSend : function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                $(Selector).html('<i class="fa fa-spin fa-spinner"></i> &nbsp;&nbsp; Loading...');
            },
            success:function(data){
                var select_MainCate= '<select class="form-control" id='+SetselectOptionID+'>';
                for(var index in data.data){
                    var row = data.data[index];
                    if(row.id==ID){
                        var Selected="SELECTED";
                    }else {
                        var Selected="";
                    }
                    select_MainCate += "<option value='"+row.id+"' "+Selected+">"+row.cate_name+"</option>";
                }
                select_MainCate+= '</select>';
                $(Selector).html(select_MainCate);
            },
            error:function(data,status,er) {
                console.log(data);
                swal(data.msg);
            }
        });
    };
    scrapeObj.getAllSubCategorieOption = function (ID,SubID,Selector,SetSelectOptionID) {
        $.ajax({
            url : kd.host+"/api/v1/categories/sub-by-main-id/"+ID,
            type:"GET",
            beforeSend : function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                $(Selector).html('<i class="fa fa-spin fa-spinner"></i> &nbsp;&nbsp; Loading...');
            },
            success:function(data){
                var select_SubCate= '<select class="form-control" id='+SetSelectOptionID+'>';
                for(var index in data.data){
                    var row = data.data[index];
                    if(row.id==SubID){
                        var Selected="SELECTED";
                    }else {
                        var Selected="";
                    }
                    select_SubCate += "<option value='"+row.id+"' "+Selected+">"+row.cate_name+"</option>";
                }
                select_SubCate+= '</select>';
                $(Selector).html(select_SubCate);
            },
            error:function(data,status,er) {
                console.log(data);
                swal(data.msg);
            }
        });
    };
});