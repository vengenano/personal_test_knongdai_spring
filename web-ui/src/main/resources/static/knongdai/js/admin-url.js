/**
 * Created by Theara on 03-Aug-17.
 */
var urlObj = {};
var test;
var check = true;
var myCurrentPage = 1;
$(document).ready(function (){
    var loading = $("div#loading");
    function search(val) {
        var idtxtSearch = $("#tbl_action-search #txt-id-search").val(val);
    }
    var idToSearch = $("#tbl_action-search #txt-id-search");
    urlObj.getAllUrl = function(page,Loading){
        $.ajax({
            url: kd.host + "/api/v1/urls?page=" + page,
            type: "GET",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                Loading.show();
            },
            success: function(data) {
                Loading.hide();
                console.log(data);
                $("#url-data").empty();
                $("#url-template-tmpl").tmpl({urls: data.data,pages:data.pagination}).appendTo("#url-data");
                if(check){
                    urlObj.setPagination(data.pagination.total_page,page);
                    check=false;
                }
            },
            error:function(data,status,er) {
                console.log(data);
            }
        });

    };
    urlObj.getUrlByTitle = function(page,Loading,key){
        $.ajax({
            url:  kd.host+"/api/v1/urls?page="+page+"&title="+key,
            type: "GET",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                Loading.show();
            },
            success: function(data) {
                Loading.hide();
                console.log(data);
                $("#url-data").empty();
                $("#url-template-tmpl").tmpl({urls: data.data,pages:data.pagination}).appendTo("#url-data");
                if(check){
                    urlObj.setPagination(data.pagination.total_page,page);
                    check=false;
                }
            },
            error:function(data,status,er) {
                console.log(data);
            }
        });

    };
    urlObj.getUrlByApproval = function(page,Loading,opt_value){
        $.ajax({
            url:  kd.host+"/api/v1/urls?page="+page+"&approved="+opt_value,
            type: "GET",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                Loading.show();
            },
            success: function(data) {
                Loading.hide();
                console.log(data);
                $("#url-data").empty();
                $("#url-template-tmpl").tmpl({urls: data.data,pages:data.pagination}).appendTo("#url-data");
                if(check){
                    urlObj.setPagination(data.pagination.total_page,page);
                    check=false;
                }
            },
            error:function(data,status,er) {
                console.log(data);
            }
        });

    };
    urlObj.getUrlByCategory = function(page,Loading,main,sub){
        $.ajax({
            url:  kd.host+"/api/v1/urls?page="+page+"&main_cate_id="+main+"&sub_cate_id="+sub,
            type: "GET",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                Loading.show();
            },
            success: function(data) {
                Loading.hide();
                console.log(data);
                $("#url-data").empty();
                $("#url-template-tmpl").tmpl({urls: data.data,pages:data.pagination}).appendTo("#url-data");
                if(check){
                    urlObj.setPagination(data.pagination.total_page,page);
                    check=false;
                }
            },
            error:function(data,status,er) {
                console.log(data);
            }
        });

    };
    urlObj.getUrlByAllCondition = function(page,Loading,key,main,sub,selValue){
        $.ajax({
            url:  kd.host+"/api/v1/urls?page="+page+"&title="+key+"&main_cate_id="+main+"&sub_cate_id="+sub+"&approved="+selValue,
            type: "GET",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                Loading.show();
            },
            success: function(data) {
                Loading.hide();
                console.log(data);
                $("#url-data").empty();
                $("#url-template-tmpl").tmpl({urls: data.data,pages:data.pagination}).appendTo("#url-data");
                if(check){
                    urlObj.setPagination(data.pagination.total_page,page);
                    check=false;
                }
            },
            error:function(data,status,er) {
                console.log(data);
            }
        });

    };
    urlObj.setPagination = function(totalPage, currentPage){
        $('#pagination').bootpag({
            total: totalPage,
            page: currentPage,
            maxVisible: 8,
            leaps: true,
            firstLastUse: true,
            first: 'First',
            last: 'Last',
            wrapClass: 'pagination',
            activeClass: 'active',
            disabledClass: 'disabled',
            nextClass: 'next',
            prevClass: 'prev',
            lastClass: 'last',
            firstClass: 'first'
        }).on("page", function(event, currentPage){
            check = false;
            myCurrentPage = currentPage;
            // option = {};
            if(idToSearch.val()==1){
                // alert(myCurrentPage);
                var key = $("#txt-title-search").val().trim();
                urlObj.getUrlByTitle(myCurrentPage,loading,key);
            }else if(idToSearch.val()==2) {
                var selValue = $('input[name=opt-Approval]:checked').val();
                urlObj.getUrlByApproval(myCurrentPage,loading,selValue);
            }else if(idToSearch.val()==3) {
                var main = $("#select-MainCate-Filter").val();
                var sub  = $("#select-SubCate-Filter").val();
                if(main==null){
                    swal("Please wait while Main Category is loading... ");
                }else if(sub==null){
                    swal("Please wait while Sub Category is loading... ");
                }else {
                    urlObj.getUrlByCategory(myCurrentPage,loading,main,sub);
                }
            }else if(idToSearch.val()==4) {
                var key = $("#txt-title-search").val().trim();
                var selValue = $('input[name=opt-Approval]:checked').val();
                var main = $("#select-MainCate-Filter").val();
                var sub  = $("#select-SubCate-Filter").val();
                if(main==null){
                    swal("Please wait while Main Category is loading... ");
                }else if(sub==null){
                    swal("Please wait while Sub Category is loading... ");
                }else {
                    urlObj.getUrlByAllCondition(myCurrentPage,loading,key,main,sub,selValue);
                }
            }else{
                urlObj.getAllUrl(currentPage,loading);
            }
        });
    };

    //Show MOdals
    $(document).on("click","#btndetail",function (e) {
        e.preventDefault();
        urlObj.getUrls($(this).data('id'),$(this).data('maincate'),$(this).data('subcate'));
        console.log($(this).data('id'),$(this).data('maincate'),$(this).data('subcate'));
    });
    $(document).on("click","#btndelete",function (e) {
        e.preventDefault();
        urlObj.deleteURL($(this).data("id"));
    });
    $(document).on("click","#btn-add-Url",function (e) {
        // alert();
        e.preventDefault();
        $("#URLAddModal").modal('show');
        urlObj.getAllMainCategorieOption(1,"#divMainCateAdd","Select-MainCateID-URL-ADD");
        urlObj.getAllSubCategorieOption(1,1,"#divSubCateAdd","Select-SubCateID-URL-ADD");
        $("#txtTitle_Add").focus();
    });
    $(document).on("change","#Select-MainCateID-URL-ADD",function () {
        console.log($(this).val());
        urlObj.getAllSubCategorieOption($(this).val(),1,"#divSubCateAdd","Select-SubCateID-URL-ADD");
    });
    $(document).on("click","#btnupdate",function (e) {
        // alert();
        e.preventDefault();
        $("#URLUpdateModal").modal('show');
        urlObj.getUrlsToUpDate($(this).data('id'));
        $("#URL-title-span").text($(this).data('title'));
    });
    $(document).on("click","#btn-get-allURL",function (e) {
        // alert();
        e.preventDefault();
        myCurrentPage=1;
        myCurrentPage=1;
        check=true;
        search("");
        urlObj.getAllUrl(myCurrentPage,loading);
        $("#txt-title-search").val("");
    });
    $(document).on("click","#btn-title-search",function (e) {
        // alert();
        e.preventDefault();
        var key = $("#txt-title-search").val().trim();
        myCurrentPage=1;
        // alert(myCurrentPage);
        check=true;

        if(key.length==0){
            $("#td-sms").text("Please Input Title Keyword")
                .css("color","red");
        }else {
            $("#td-sms").text("Filtering By Titile \""+key+"\"")
                .css("color","black");
            $("#tbl_action-search #txt-id-search").val(1);
            urlObj.getUrlByTitle(myCurrentPage,loading,key);
        }


    });
    $(document).on("click","#btn-approval-search",function (e) {
        // alert();
        e.preventDefault();
        var selValue = $('input[name=opt-Approval]:checked').val();
        search(2);
        if(selValue=="true"){
            $("#td-sms").text("Filtering By Approved")
                .css("color","black");
        }else{
            $("#td-sms").text("Filtering By Disapproved")
                .css("color","black");
        }
        myCurrentPage=1;
        check=true;
        urlObj.getUrlByApproval(myCurrentPage,loading,selValue);
    });
    $(document).on("click","#btn-category-search",function (e) {
        // alert();
        e.preventDefault();
        search(3);
        myCurrentPage=1;
        check=true;
        var main = $("#select-MainCate-Filter").val();
        var sub  = $("#select-SubCate-Filter").val();
        $("#td-sms").text("Filtering By Main Category: "+$("#select-MainCate-Filter option:selected").text() +" and Sub Category: "+$("#select-SubCate-Filter option:selected").text())
            .css("color","black");
        if(main==null){
            swal("Please wait while Main Category is loading... ");
        }else if(sub==null){
            swal("Please wait while Sub Category is loading... ");
        }else {
            urlObj.getUrlByCategory(myCurrentPage,loading,main,sub);
        }
    });
    $(document).on("click","#btn-search-addCondition",function (e) {
        // alert();
        e.preventDefault();
        search(4);
        myCurrentPage=1;
        check=true;
        var key = $("#txt-title-search").val().trim();
        var selValue = $('input[name=opt-Approval]:checked').val();
        var main = $("#select-MainCate-Filter").val();
        var sub  = $("#select-SubCate-Filter").val();
        if(key.length==0){
            $("#td-sms").text("Please Input Title Keyword")
                .css("color","red");
        }else{
            urlObj.getUrlByAllCondition(myCurrentPage,loading,key,main,sub,selValue);
        }
    });
    //End of Show Modals
    //Submit forms
    $("#URLUpdateForm").submit(function(e) {
        e.preventDefault();
        urlObj.updateURL();
        // alert($("input[name='optradio-type-up']:checked").val());
    });
    $("#URLAddForm").submit(function(e) {
        e.preventDefault();
        urlObj.addURL();
    });
    //End 0f Submitform
    $(document).on('click' , ".btnUpdateUrlStatus" , function(){
        // var this_Span = $(this).find('span').attr("class");
        //<i class="fa fa-spin fa-spinner"></i>
        var this_spin = $(this);
        urlObj.updateUrlStatus($(this).data('id'),$(this).data('status'),this_spin)
    });
    //change status
    urlObj.updateUrlStatus = function(id,status,this_span){
        $.ajax({
            url:  kd.host+"/api/v1/urls/"+id+"/approved/"+status,
            type: "PUT",
            beforeSend: function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                this_span.html('<i class="fa fa-spin fa-spinner"></i>')
            },
            success: function(data) {
                console.log(data);
                if(data.msg=="approved"){
                    this_span.parent().html('<button type="button" id="" data-id="'+id+'" data-status="0" class="btn btn-xs btn-success btnUpdateUrlStatus">' +
                                        '<span class="glyphicon glyphicon-ok"></span>' +
                                    '</button>');
                }else {
                    this_span.parent().html('<button type="button" id="" data-id="'+id+'" data-status="1" class="btn btn-xs btn-danger btnUpdateUrlStatus">' +
                                        '<span class="glyphicon glyphicon-remove"></span>' +
                                    '</button>');
                }
                // urlObj.getAllUrl(myCurrentPage,$("#NoIDs"));
            },
            error:function(data,status,er) {
                console.log(data);
            }
        });
    };
    //end of change status

    //add to database
    urlObj.addURL = function () {

        var keyword = $('#URL_kw_Add').val();
        var keywords = keyword.split(',');
        var jsonKeyword=[];

        for(var i = 0; i < keywords.length; i++){
            jsonKeyword.push(keywords[i]);
        }
        console.log(jsonKeyword);

        var txtTitle_Add=$("#URLAddModal #txtTitle_Add").val();
        URLDataAdd = {
            "address":  $("#URLAddModal #txtAddr_Add").val(),
            "approved": true,
            "des":      $("#URLAddModal #txtDesc_Add").val(),
            "email":    $("#URLAddModal #txtEmail_Add").val(),
            "keywords": jsonKeyword,
            "link":     $("#URLAddModal #txtLink_Add").val(),
            "main_cate_id": $("#URLAddModal #Select-MainCateID-URL-ADD").val(),
            "phone":    $("#URLAddModal #txtTel_Add").val(),
            "pic_url":  $("#URLAddModal #txtimgURL_Add").val(),
            "status": true,
            "sub_cate_id": $("#URLAddModal #Select-SubCateID-URL-ADD").val(),
            "title":    txtTitle_Add,
            "type":     $("input[name='optradio-type']:checked").val()
        };

        console.log(URLDataAdd);
        swal({
                title : " Add New URL",
                text : "Are you sure you want to add \" "+txtTitle_Add+" \" URL?",
                type : "info",
                showCancelButton : true,
                closeOnConfirm : true,
                showLoaderOnConfirm : true
            },
            function() {

                $.ajax({
                    url : kd.host+"/api/v1/urls/create",
                    type : "POST",
                    data : JSON.stringify(URLDataAdd),
                    beforeSend : function(xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success : function(data) {
                        console.log(data.msg)
                        swal(data.msg);
                        urlObj.getAllUrl(myCurrentPage,loading);
                        $('#URLAddModal').modal('hide');
                    },
                    error : function(data, err, status) {
                        console.log(data);
                        swal(data.msg);
                    }
                });

            });
    }
    //Update URL to database
    urlObj.updateURL = function(){
        var keyword = $('#URL_kw_up').val();
        var keywords = keyword.split(',');
        var jsonKeyword=[];

        for(var i = 0; i < keywords.length; i++){
            jsonKeyword.push(keywords[i]);
        }
        console.log(jsonKeyword);
        URLDataUp = {
            "id" :      $("#URLUpdateModal #txtID_Up").val(),
            "title" :   $("#URLUpdateModal #txtTitle_Up").val(),
            "main_cate_id" :   $("#URLUpdateModal #select_MainCate_Up").val(),
            "sub_cate_id" :   $("#URLUpdateModal #select_SubCate_Up").val(),
            "link" :   $("#URLUpdateModal #txtLink_Up").val(),
            "address" :   $("#URLUpdateModal #txtAddr_Up").val(),
            "phone" :   $("#URLUpdateModal #txtTel_Up").val(),
            "email" :   $("#URLUpdateModal #txtEmail_Up").val(),
            "pic_url" :   $("#URLUpdateModal #txtimgURL_Up").val(),
            "des" :   $("#URLUpdateModal #txtDesc_Up").val(),
            "keywords" :   jsonKeyword,
            "type": $("input[name='optradio-type-up']:checked").val(),
            "status": true,
            "approved": $("#URLUpdateModal #txtapproved").val()
        };
        console.log(URLDataUp);
        swal({
            title : "Updating URL",
            text : "Are you sure to update this URL?",
            type : "info",
            showCancelButton : true,
            closeOnConfirm : true,
            showLoaderOnConfirm : false,
        }, function() {
            $.ajax({
                url : kd.host+"/api/v1/urls/update",
                type : "PUT",
                data : JSON.stringify(URLDataUp),
                beforeSend : function(xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success : function(data) {
                    check=true;
                    console.log("id = "+idToSearch.val());
                    if(idToSearch.val()==1){
                        var key = $("#txt-title-search").val().trim();
                        urlObj.getUrlByTitle(myCurrentPage,loading,key);
                    }else if(idToSearch.val()==2) {
                        var selValue = $('input[name=opt-Approval]:checked').val();
                        urlObj.getUrlByApproval(myCurrentPage,loading,selValue);
                    }else if(idToSearch.val()==3) {
                        var main = $("#select-MainCate-Filter").val();
                        var sub  = $("#select-SubCate-Filter").val();
                        urlObj.getUrlByCategory(myCurrentPage,loading,main,sub);
                    }else if(idToSearch.val()==4) {
                        var key = $("#txt-title-search").val().trim();
                        var selValue = $('input[name=opt-Approval]:checked').val();
                        var main = $("#select-MainCate-Filter").val();
                        var sub  = $("#select-SubCate-Filter").val();
                        urlObj.getUrlByAllCondition(myCurrentPage,loading,key,main,sub,selValue);
                    }else{
                        urlObj.getAllUrl(myCurrentPage,loading);
                    }
                    console.log(data);
                    swal(data.msg);

                    //urlObj.getAllUrl(myCurrentPage,loading);
                    $('#URLUpdateModal').modal('hide');
                },
                error : function(data, status, err) {
                    var responseText = jQuery.parseJSON(data.responseText);
                    sweetAlert("Opps...Somethings went wrong !!!");
                    console.log(data);
                }
            });
        });
    }
    //Delete
    urlObj.deleteURL = function (data) {
        swal({
                title: "Deleting Url" ,
                text: "Are yo sure to delete this Url ?",
                type: "info",
                showCancelButton: true,
                closeOnConfirm: true,
                showLoaderOnConfirm: false
            },
            function(){
                $.ajax({
                    url:  kd.host+"/api/v1/urls/"+data+"/delete",
                    type: "DELETE",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function(data) {
                        swal(data.message);
                        urlObj.getAllUrl(myCurrentPage,loading);
                    },
                    error:function(data,status,er) {
                        console.log(data);
                        swal(data.msg);
                    }
                });


            });
    }
    //Detail
    urlObj.getUrls = function (data,MainCate,SubCate) {
        $.ajax({
            url: kd.host+"/api/v1/urls/" + data,
            type: "GET",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function (data) {
                $("#UrlDetailModal").modal('show');
                u_id = data.data.id;
                $("#UrlDetailModal #URLId_detail").text(data.data.id);
                $("#UrlDetailModal #title_detail").text(data.data.title);
                $("#UrlDetailModal #MainCate_detail").text(MainCate);
                $("#UrlDetailModal #SubCate_detail").text(SubCate);

                if(data.data.type=="w"){
                    $("#UrlDetailModal #link_url_type").text("Website");
                }else{
                    $("#UrlDetailModal #link_url_type").text("Facebook Page");
                }

                $("#UrlDetailModal #Link_detail").text(data.data.link);
                $("#UrlDetailModal #Addr_detail").text(data.data.address);
                $("#UrlDetailModal #Tel_detail").text(data.data.phone);
                $("#UrlDetailModal #Email_detail").text(data.data.email);
                $("#UrlDetailModal #Desc_detail").text(data.data.des);
                $("#UrlDetailModal #View_detail").text("~");
                $("#UrlDetailModal #Pic_detail").attr("src",data.data.pic_url);
            },
            error: function (data, status, er) {
                console.log(data);
            }
        });
    }
    urlObj.getUrlsToUpDate = function (data) {
        $.ajax({
            url: kd.host+"/api/v1/urls/" + data,
            type: "GET",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function (data) {
                console.log(data)
                // $("#UrlDetailModal").modal('show');
                u_id = data.data.id;
                $("#URLUpdateModal #txtID_Up").val(data.data.id);
                $("#URLUpdateModal #txtTitle_Up").val(data.data.title);
                urlObj.getAllMainCategorieOption(data.data.main_cate_id,"#divMainCateUp","select_MainCate_Up");
                urlObj.getAllSubCategorieOption(data.data.main_cate_id,data.data.sub_cate_id,"#divSubCateUp","select_SubCate_Up");
                $("#URLUpdateModal #txtLink_Up").val(data.data.link);
                $("#URLUpdateModal #txtAddr_Up").val(data.data.address);
                $("#URLUpdateModal #txtTel_Up").val(data.data.phone);
                $("#URLUpdateModal #txtEmail_Up").val(data.data.email);
                $("#URLUpdateModal #txtDesc_Up").val(data.data.des);
                // $("#URLUpdateModal #txtimgURL_Up").attr("src",data.data.pic_url);
                $("#URLUpdateModal #txtimgURL_Up").val(data.data.pic_url);
                $("#URLUpdateModal #txtapproved").val(data.data.approved);
                var mainCategorykeyword = '';
                for (var i = 0; i < data.data.keywords.length; i++) {
                    mainCategorykeyword += data.data.keywords[i]+',';

                }
                console.log(mainCategorykeyword)
                // $("#tagssss").html(mainCategorykeyword);
                $("#URLUpdateModal #URL_kw_up").importTags(mainCategorykeyword);
                    var radiobtn="";
                if(data.data.type=="w"){
                    radiobtn+='<label class="radio-inline"><input type="radio" checked name="optradio-type-up" value="w">Website</label>' +
                                '<label class="radio-inline"><input type="radio" name="optradio-type-up" value="f">facebook page</label>';
                }else{
                    radiobtn+='<label class="radio-inline"><input type="radio"  name="optradio-type-up" value="w">Website</label>' +
                        '<label class="radio-inline"><input type="radio" checked name="optradio-type-up" value="f">facebook page</label>';
                }

                $("#URLUpdateModal #url-radio-up").html(radiobtn);
            },
            error: function (data, status, er) {
                console.log(data);
            }
        });
    }
    urlObj.getAllMainCategorieOption = function (ID,Selector,SetselectOptionID) {
        $.ajax({
            url : kd.host+"/api/v1/categories",
            type:"GET",
            beforeSend : function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                $(Selector).html('<i class="fa fa-spin fa-spinner"></i> &nbsp;&nbsp; Loading...');
            },
            success:function(data){
                var select_MainCate= '<select class="form-control" id='+SetselectOptionID+'>';
                for(var index in data.data){
                    var row = data.data[index];
                    if(row.id==ID){
                        var Selected="SELECTED";
                    }else {
                        var Selected="";
                    }
                    select_MainCate += "<option value='"+row.id+"' "+Selected+">"+row.cate_name+"</option>";
                }
                select_MainCate+= '</select>';
                $(Selector).html(select_MainCate);
            },
            error:function(data,status,er) {
                console.log(data);
                swal(data.msg);
            }
        });
    };
    urlObj.getAllSubCategorieOption = function (ID,SubID,Selector,SetSelectOptionID) {
        $.ajax({
            url : kd.host+"/api/v1/categories/sub-by-main-id/"+ID,
            type:"GET",
            beforeSend : function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                $(Selector).html('<i class="fa fa-spin fa-spinner"></i> &nbsp;&nbsp; Loading...');
            },
            success:function(data){
                var select_SubCate= '<select class="form-control" id='+SetSelectOptionID+'>';
                for(var index in data.data){
                    var row = data.data[index];
                    if(row.id==SubID){
                        var Selected="SELECTED";
                    }else {
                        var Selected="";
                    }
                    select_SubCate += "<option value='"+row.id+"' "+Selected+">"+row.cate_name+"</option>";
                }
                select_SubCate+= '</select>';
                $(Selector).html(select_SubCate);
            },
            error:function(data,status,er) {
                console.log(data);
                swal(data.msg);
            }
        });
    };

    //checkbox for show the action search
    $("#chk-approval-search").on("change",function () {
        if($(this).is(':checked')){
            $("#tr-approval-search").show(200);
        }else{
            $("#tr-approval-search").hide(200);
        }
        $("#chk-allConditon-search").removeAttr("checked");
        $("#tr-search-allCondition").hide(200);
    });
    $("#chk-category-search").on("change",function () {
        if($(this).is(':checked')){
            $("#tr-Category-search").show(200);
        }else{
            $("#tr-Category-search").hide(200);
        }
        $("#chk-allConditon-search").removeAttr("checked");
        $("#tr-search-allCondition").hide(200);
    });
    $("#chk-allConditon-search").on("change",function () {
        if($(this).is(':checked')){
            $("#tr-search-allCondition").show(200);
            $("#tr-Category-search").show(200);
            $("#tr-approval-search").show(200);

            $("#chk-approval-search").attr("checked","checked");
            $("#chk-category-search").attr("checked","checked");
        }else{
            $("#tr-search-allCondition").hide(200);
            $("#chk-approval-search").removeAttr("checked");
            $("#chk-category-search").removeAttr("checked");
            $("#tr-Category-search").hide(200);
            $("#tr-approval-search").hide(200);
        }
    });

    //On change select Option
    $(document).on("change","#select_MainCate_Up",function () {
        urlObj.getAllSubCategorieOption($(this).val(),1,"#divSubCateUp","select_SubCate_Up");
    });
    //End change select option
    urlObj.getAllMainCategorieOption(1,"#td-MainCate-Filter","select-MainCate-Filter");
    urlObj.getAllSubCategorieOption(1,1,"#td-SubCate-Filter","select-SubCate-Filter");

    $(document).on("change","#select-MainCate-Filter",function () {
        console.log($(this).val());
        urlObj.getAllSubCategorieOption($(this).val(),1,"#td-SubCate-Filter","select-SubCate-Filter");
    });

    urlObj.getAllUrl(myCurrentPage,loading);




});