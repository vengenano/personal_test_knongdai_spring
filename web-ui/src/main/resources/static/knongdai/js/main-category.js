/**
 * Created by sokheangret on 7/26/17.
 */
var mainCategory = {};
var u_id;

var test;

$(document).ready(function(){

    //View detail
    $(document).on('click', "#btDetail", function (e) {
        e.preventDefault();
        e.stopPropagation();
        mainCategory.getMainCategoryDetail($(this).data('id'));
    });

    $(document).on('click',"#btDelete" , function(e){
        e.preventDefault();
        e.stopPropagation();
        mainCategory.deleteMainCategory($(this).data('id'),$(this).data('cate_name'),1);
    });
    $(document).on('click',"#btDelete_sub-cate" , function(e){
        e.preventDefault();
        e.stopPropagation();
        mainCategory.deleteMainCategory($(this).data('id'),$(this).data('sub_cate_name'),2);
    });
    $(document).on('click',"#btUpdate" , function(e){
        e.preventDefault();
        e.stopPropagation();
        mainCategory.getMainCategory($(this).data('id'),$(this).data('type'),$(this).data('main'));
        //$("#hidden-Main-Cate-ID").val($(this).data('mainCate'));
        // console.log();
    });
    $(document).on('click', "#btAdd", function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('#mainCategoryAddModal').modal('show');
    });
    $(document).on('click', "#btAddByMainCate", function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('#mainCategoryByMainCateAddModal').modal('show');
        $("#span_MainCate_name").html($(this).data('cate_name'));
        $("#hidden_MainCateID").val($(this).data('id'));
    });
    // Submit main category data to insert
    $("#mainCategoryAddForm").submit(function(e) {
        e.preventDefault();
        if($("#new-keywords").val().length==0){
            $("#new-keywords_tagsinput").css("border-color","red");
            $("#new-keywords_tag").focus();
        }else{
            mainCategory.addMainCategory();
        }
    });
    $("#mainCategoryByMainCateAddForm").submit(function(e) {
        e.preventDefault();
        if($("#new-keywordsByMainCate").val().length==0){
            $("#new-keywordsByMainCate_tagsinput").css("border-color","red");
            $("#new-keywordsByMainCate_tag").focus();
        }else {
            mainCategory.addSubCateByMainCate();
        }
    });
    // Submit main category data to update
    $("#mainCategoryUpdateForm").submit(function(e) {
        e.preventDefault();
        if($("#keywords").val().length==0){
            $("#keywords_tagsinput").css("border-color","red");
            $("#keywords_tag").focus();
        }else {
            mainCategory.updateMainCategory();
        }
    });
    $("#subCategoryUpdateForm").submit(function(e) {
        e.preventDefault();
        if($("#sub_keywords").val().length==0){
            $("#sub_keywords_tagsinput").css("border-color","red");
            $("#sub_keywords_tag").focus();
        }else{
            mainCategory.updateSubCategory();
        }
    });
    /**
     * Function return all data by AJAX
     */
    mainCategory.getAllMainCategories = function () {
        $.ajax({
            url : kd.host+"/api/v1/categories",
            type:"GET",
            beforeSend : function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success:function(data){
                if(data.status == true){
                    $("tbody#mainCategoryData").empty();
                    $("#mainCategory_tmpl").tmpl({mainCategories : data.data}).appendTo("tbody#mainCategoryData");
                }
            },
            error:function(data,status,er) {
                console.log(data);
                swal(data.msg);
            }
        });
    };

    /**
     * Function return one data by AJAX
     */

    mainCategory.getMainCategoryDetail = function(data){
        $.ajax({
            url : kd.host+"/api/v1/categories/"+data,
            type : "GET",
            beforeSend : function(xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                $("#tags_detail>ul").empty();
            },
            success : function(data) {
                $("#mainCategoryDetailModal #mainCategoryId_detail").text(data.data.id);
                $("#mainCategoryDetailModal #mainCategoryName_detail").text(data.data.cate_name);
                $("#mainCategoryDetailModal #description_detail").text(data.data.des);

                // var mainCategorykeyword = '';
                for (var i = 0; i < data.data.keywords.length; i++) {
                    // mainCategorykeyword += data.data.keywords[i].keyword_name+',';
                    $("#tags_detail>ul").append('<li><div class="label label-success btn-lg" style="margin-left: 10px;">'+data.data.keywords[i].keyword_name+' <i class="fa  fa-tag"></i></div></li>');
                }

                // $("#dKeywords").importTags(mainCategorykeyword);

                $("#mainCategoryDetailModal").modal('show');

            },
            error:function (data,status,xhr) {
                console.log(data);
                swal(data.msg);
            }
        });
    }

    /**
     * Fuction to change status of main category (delete)
     */


    mainCategory.deleteMainCategory = function (data,cate_name,check) {
        if(check==1){
            var sms="Are you sure you want to deleted \" "+cate_name+" \" main category?"
            var title= "Main category";
        }else {
            var sms="Are you sure you want to deleted \" "+cate_name+" \" sub category?"
            var title= "Sub category";
        }
        swal({
                title: title ,
                text: sms,
                type: "info",
                showCancelButton: true,
                closeOnConfirm: true,
                showLoaderOnConfirm: false
            },
            function(){
                $.ajax({
                    url:  kd.host+"/api/v1/categories/"+data+"/delete",
                    type: "DELETE",
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success: function(data) {
                        swal(data.message);
                        mainCategory.getAllMainCategories();
                    },
                    error:function(data,status,er) {
                        console.log(data);
                        swal(data.msg);
                    }
                });


            });
    }

    //Add not yet complete
    mainCategory.addMainCategory = function () {

        var keyword = $('#new-keywords').val();
        var keywords = keyword.split(',');
        var jsonKeyword=[];

        for(var i = 0; i < keywords.length; i++){
            jsonKeyword.push(keywords[i]);
        }

        var mainCategoryName_add=$("#mainCategoryName_add").val();
        mainCategoryData = {
            "cate_name" : $("#mainCategoryAddForm #mainCategoryName_add").val(),
            "icon_name" : $("#mainCategoryAddForm #iconName_add").val(),
            "des" : $("#mainCategoryAddForm #description_add").val(),
            "keywords" : jsonKeyword
        };

        swal({
            title : " Add Main Category",
            text : "Are you sure you want to add \" "+mainCategoryName_add+" \" main category?",
            type : "info",
            showCancelButton : true,
            closeOnConfirm : true,
            showLoaderOnConfirm : true
        },
            function() {

            $.ajax({
                url : kd.host+"/api/v1/categories/create-main",
                type : "POST",
                data : JSON.stringify(mainCategoryData),
                beforeSend : function(xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success : function(data) {

                    swal(data.message);
                    mainCategory.getAllMainCategories();
                    $('#mainCategoryAddModal').modal('hide');
                    $("html, body").animate({ scrollTop: $(document).height()-$(window).height() });
                    setTimeout(function () {
                        $('#mainCategoryData>tr').last().prev().css('background-color', '#FFFF99');
                        setTimeout(function () {
                            $('#mainCategoryData>tr').last().prev().css('background-color', 'transparent');
                        },2000);
                    },200);

                },
                error : function(data, err, status) {
                    console.log(data);
                    swal(data.msg);
                }
            });

        });
    }
    //Add sub Category By Main Category
    mainCategory.addSubCateByMainCate = function () {

        var keyword = $('#new-keywordsByMainCate').val();
        var keywords = keyword.split(',');
        var jsonKeyword=[];

        for(var i = 0; i < keywords.length; i++){
            jsonKeyword.push(keywords[i]);
        }
        console.log(jsonKeyword);

        var SubCategoryNameByMainCate=$("#mainCategoryByMainCateAddModal #SubCategoryNameByMainCate").val();
        SubCategoryDataAdd = {
            "cate_name" : $("#mainCategoryByMainCateAddModal #SubCategoryNameByMainCate").val(),
            "icon_name":"no.png",
            "des" : $("#mainCategoryByMainCateAddModal #descriptionByMainCate").val(),
            "keywords" : jsonKeyword,
            "main_cate_id": $("#mainCategoryByMainCateAddModal #hidden_MainCateID").val(),
            "status": true
        };

        console.log(SubCategoryDataAdd);
        swal({
                title : " Add Sub Category",
                text : "Are you sure you want to add \" "+SubCategoryNameByMainCate+" \" Sub category?",
                type : "info",
                showCancelButton : true,
                closeOnConfirm : true,
                showLoaderOnConfirm : true
            },
            function() {

                $.ajax({
                    url : kd.host+"/api/v1/categories/create-sub",
                    type : "POST",
                    data : JSON.stringify(SubCategoryDataAdd),
                    beforeSend : function(xhr) {
                        xhr.setRequestHeader("Accept", "application/json");
                        xhr.setRequestHeader("Content-Type", "application/json");
                    },
                    success : function(data) {
                        console.log(data.msg)
                        swal(data.msg);
                        mainCategory.getAllMainCategories();
                        $('#mainCategoryByMainCateAddModal').modal('hide');
                    },
                    error : function(data, err, status) {
                        console.log(data);
                        swal(data.msg);
                    }
                });

            });
    }
    //Get main category to update
    mainCategory.getMainCategory = function (data,type,main) {
        $.ajax({
            url: kd.host+"/api/v1/categories/" + data,
            type: "GET",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success: function (data) {
                if(type==1){
                    u_id  = data.data.id;
                    $("#mainCategoryUpdateModal #mainCategoryName_up").val(data.data.cate_name);
                    $("#mainCategoryUpdateModal #iconName_up").val(data.data.icon_name);
                    $("#mainCategoryUpdateModal #description_up").val(data.data.des);
                }else {
                    u_id  = data.data.id;
                    $("#subCategoryUpdateModal #subCategoryName_up").val(data.data.cate_name);
                    $("#subCategoryUpdateModal #sub_iconName_up").val(data.data.icon_name);
                    $("#subCategoryUpdateModal #sub_description_up").val(data.data.des);
                    $.ajax({
                        url: kd.host+"/api/v1/categories",
                        type: "GET",
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Accept", "application/json");
                            xhr.setRequestHeader("Content-Type", "application/json");
                            $("#div_select_opt_main_cate").empty();
                        },
                        success: function (data) {
                            var indexSelected = main;
                            var select_MainCate= '<select id="select_MainCate">';
                            for(var index in data.data){
                                var row = data.data[index];
                                if(row.id==indexSelected){
                                    var Selected="SELECTED";
                                }else {
                                    var Selected="";
                                }
                                select_MainCate += "<option value='"+row.id+"' "+Selected+">"+row.cate_name+"</option>";
                            }
                            select_MainCate+= '</select>';
                            $("#div_select_opt_main_cate").html(select_MainCate);
                        },
                        error: function (data, status, er) {
                            console.log(data);
                        }
                    });
                }


                var mainCategorykeyword = '';
                for (var i = 0; i < data.data.keywords.length; i++) {
                    mainCategorykeyword += data.data.keywords[i].keyword_name+',';

                }
                console.log(mainCategorykeyword)
                // $("#tagssss").html(mainCategorykeyword);
                if(type==1){
                    $("#mainCategoryUpdateModal #keywords").importTags(mainCategorykeyword);
                    $("#mainCategoryUpdateModal").modal('show');
                }else {
                    $("#subCategoryUpdateModal #sub_keywords").importTags(mainCategorykeyword);
                    $("#subCategoryUpdateModal").modal('show');
                }

            },
            error: function (data, status, er) {
                console.log(data);
            }
        });

    }
    //Update category
    mainCategory.updateMainCategory = function(){

        var keyword = $('#keywords').val();
        var keywords = keyword.split(',');
        var jsonKeyword=[];

        for(var i = 0; i < keywords.length; i++){
            jsonKeyword.push(keywords[i]);
        }
        console.log(jsonKeyword);

        mainCategoryData = {
            "id" : u_id,
            "cate_name" : $("#mainCategoryUpdateModal #mainCategoryName_up").val(),
            "icon_name" : $("#mainCategoryUpdateModal #iconName_up").val(),
            "des" : $("#mainCategoryUpdateModal #description_up").val(),
            "keywords" : jsonKeyword,
            "status" : true
        };
        console.log(mainCategoryData);
        swal({
            title : "Main Category",
            text : "Are you sure to update this main category?",
            type : "info",
            showCancelButton : true,
            closeOnConfirm : true,
            showLoaderOnConfirm : false,
        }, function() {
            $.ajax({
                url : kd.host+"/api/v1/categories/update-main",
                type : "PUT",
                data : JSON.stringify(mainCategoryData),
                beforeSend : function(xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success : function(data) {
                    console.log(data);
                    swal(data.msg);
                    mainCategory.getAllMainCategories();
                    $('#mainCategoryUpdateModal').modal('hide');
                },
                error : function(data, status, err) {
                    var responseText = jQuery.parseJSON(data.responseText);
                    sweetAlert("Opps...", responseText.message, "error");
                    console.log(data);
                }
            });
        });
    }
    mainCategory.updateSubCategory = function(){
        //alert(Main_cate_id);
        var keyword = $('#sub_keywords').val();
        var keywords = keyword.split(',');
        var jsonKeyword=[];

        for(var i = 0; i < keywords.length; i++){
            jsonKeyword.push(keywords[i]);
        }
        console.log(jsonKeyword);
        console.log("#select_MainCate = "+$("#select_MainCate").val())
        SubCategoryData = {
            "id" : u_id,
            "cate_name" : $("#subCategoryUpdateModal #subCategoryName_up").val(),
            "icon_name" : $("#subCategoryUpdateModal #sub_iconName_up").val(),
            "des" : $("#subCategoryUpdateModal #sub_description_up").val(),
            "keywords" : jsonKeyword,
            "main_cate_id": $("#select_MainCate").val(),
            "status" : true
        };
        console.log(mainCategoryData);
        swal({
            title : "Sub Category",
            text : "Are you sure to update this sub category?",
            type : "info",
            showCancelButton : true,
            closeOnConfirm : true,
            showLoaderOnConfirm : false,
        }, function() {
            $.ajax({
                url : kd.host+"/api/v1/categories/update-sub",
                type : "PUT",
                data : JSON.stringify(SubCategoryData),
                beforeSend : function(xhr) {
                    xhr.setRequestHeader("Accept", "application/json");
                    xhr.setRequestHeader("Content-Type", "application/json");
                },
                success : function(data) {
                    console.log(data);
                    swal(data.msg);
                    mainCategory.getAllMainCategories();
                    swal(data.msg);
                    $('#subCategoryUpdateModal').modal('hide');
                },
                error : function(data, status, err) {
                    var responseText = jQuery.parseJSON(data.responseText);
                    sweetAlert("Opps...", responseText.msg, "error");
                    console.log(data);
                }
            });
        });
    }
//sub category
    $(document).on("click",".tr_main_category",function () {
        $('.pg_sub_category').fadeOut(100);
        if($(this).closest('tr').next('.pg_sub_category').is(":visible")){
            $(this).closest('tr').next('.pg_sub_category').fadeOut(100);
        }else {
            $(this).closest('tr').next('.pg_sub_category').fadeToggle(200);
            $('.div-cover-sub-cate').niceScroll({
                autohidemode:"leave"
            });
        }

    });



    /*On load*/
    mainCategory.getAllMainCategories();

    // swal("Hello");
});