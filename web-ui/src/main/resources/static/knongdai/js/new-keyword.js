/**
 * Created by sokheangret on 8/4/17.
 */

var newKeyword = [];

$(document).ready(function(){
    newKeyword.getAllNewKeyword = function (data) {
        $.ajax({
            url : kd.host + "/api/v1/new-keywords?page="+data,
            type:"GET",
            beforeSend : function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
                $("#notification-list").empty();
            },
            success:function(data){
                if(data.status == true){
                    $("#new-keyword").text(data.data.keyword_name);
                    console.log(data);
                    for (var i = 0; i < data.data.keywords.length; i++) {
                        $("#notification-list").append('' +
                            '<div class="notification-list mail-list not-list"> ' +
                            '<a href="javascript:;" class="single-mail">' +
                            '<span class="icon bg-primary">' +
                            '    <i class="fa fa-tag"></i>' +
                            '</span> ' +
                            '<strong id="new-keyword">'+data.data.keywords[i].keyword_name+'</strong> ' +
                            '<p>' +
                            ' <small>Just Now</small> ' +
                            '</p> ' +
                            '</a> ' +
                            '</div>');
                    }
                }
            },
            error:function(data,status,er) {
                console.log(data);
                swal(data.msg);
            }
        });
    }

    newKeyword.getAllNewKeyword(3);
});