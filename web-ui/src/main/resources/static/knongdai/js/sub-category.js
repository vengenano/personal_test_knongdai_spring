/**
 * Created by sokheangret on 7/26/17.
 */
var subCategory = {};

$(document).ready(function(){

    /**
     * Function return all data by AJAX
     */
    mainCategory.getAllMainCategories = function () {
        $.ajax({
            url : "http://localhost:9090/api/v1/categories",
            type:"GET",
            beforeSend : function (xhr) {
                xhr.setRequestHeader("Accept", "application/json");
                xhr.setRequestHeader("Content-Type", "application/json");
            },
            success:function(data){
                if(data.status == true){
                    $("tbody#mainCategoryData").empty();
                    $("#url-template-tmpl").tmpl({mainCategories : data.data}).appendTo("tbody#mainCategoryData");
                }
            },
            error:function(data,status,er) {
                console.log(data);
            }
        });
    };

    /*On load*/
    mainCategory.getAllMainCategories();
});